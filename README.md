Rene Haberland, Dresden September-December 2003, 2020

This is a online adventure client-server application written in Java. The user stories in the games are designed as XML-files, including story book, actions, and scene configuration. The network communication is done with JavaRMI (remote method invocation). The game is designed as 3-tier architecture, and there is also a MYSQL database involved for storing and loading multiple game instances.

It was done as an (successful) attempt to software engineering complex software systems.


This work is licensed under the Creative Commons Attribution-Noncommercial-Share Alike version 3 (CC-SA-NC 3). To view a copy of this license, visit http://creativecommons.org/licenses/ or send a letter to Creative Commons, 171 Second Street, Suite 300, San Francisco, California, 94105, USA. A license has been added to this repository.
