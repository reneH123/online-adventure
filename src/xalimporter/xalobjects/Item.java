package adventure.xalimporter.xalobjects;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Rene Haberland
 *
 */
public class Item {
	private List actTakeList;
	private List actLookList;
	private List actUseList;
	private List actTalkList;

	private String ID;
	private String imagePath;
	private String posX;
	private String posY;
	private String level;
	private String visible;
	private String description;

	public Item() {
		actTakeList = new ArrayList();
		actLookList = new ArrayList();
		actUseList = new ArrayList();
		actTalkList = new ArrayList();

	}

	/**
	 * @return
	 */
	public List getActLookList() {
		return actLookList;
	}

	/**
	 * @return
	 */
	public List getActTakeList() {
		return actTakeList;
	}

	/**
	 * @return
	 */
	public List getActTalkList() {
		return actTalkList;
	}

	/**
	 * @return
	 */
	public List getActUseList() {
		return actUseList;
	}

	/**
	 * @param list
	 */
	public void setActLookList(List list) {
		actLookList = list;
	}

	/**
	 * @param list
	 */
	public void setActTakeList(List list) {
		actTakeList = list;
	}

	/**
	 * @param list
	 */
	public void setActTalkList(List list) {
		actTalkList = list;
	}

	/**
	 * @param list
	 */
	public void setActUseList(List list) {
		actUseList = list;
	}

	/**
	 * @return
	 */
	public String getImagePath() {
		return imagePath;
	}

	/**
	 * @return
	 */
	public String getLevel() {
		return level;
	}

	/**
	 * @return
	 */
	public String getPosX() {
		return posX;
	}

	/**
	 * @return
	 */
	public String getPosY() {
		return posY;
	}

	/**
	 * @return
	 */
	public String getVisible() {
		return visible;
	}

	/**
	 * @param string
	 */
	public void setImagePath(String string) {
		imagePath = string;
	}

	/**
	 * @param string
	 */
	public void setLevel(String string) {
		level = string;
	}

	/**
	 * @param string
	 */
	public void setPosX(String string) {
		posX = string;
	}

	/**
	 * @param string
	 */
	public void setPosY(String string) {
		posY = string;
	}

	/**
	 * @param string
	 */
	public void setVisible(String string) {
		visible = string;
	}

	/**
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return
	 */
	public String getID() {
		return ID;
	}

	/**
	 * @param string
	 */
	public void setDescription(String string) {
		description = string;
	}

	/**
	 * @param string
	 */
	public void setID(String string) {
		ID = string;
	} 
}

