package adventure.xalimporter.xalobjects;

/**
 * @author Rene Haberland
 *
 */
public class DialogOption {
	private String ID;
	private String text;
	private String actionScript;
	
	public DialogOption() {
	}

	/**
	 * @return
	 */
	public String getActionScript() {
		return actionScript;
	}

	/**
	 * @return
	 */
	public String getID() {
		return ID;
	}

	/**
	 * @return
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param string
	 */
	public void setActionScript(String string) {
		actionScript = string;
	}

	/**
	 * @param string
	 */
	public void setID(String string) {
		ID = string;
	}

	/**
	 * @param string
	 */
	public void setText(String string) {
		text = string;
	}
}

