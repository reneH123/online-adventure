package adventure.xalimporter.xalobjects;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Rene Haberland
 *
 */
public class Dialog {

	private List dialogOptionList;
	private String ID;

	/*
	 * constructor
	 */
	public Dialog(String ID) {
		this.ID = ID;
		this.dialogOptionList = new ArrayList();
	}

	/**
	 * @return
	 */
	public List getDialogOptionList() {
		return dialogOptionList;
	}

	/**
	 * @param list
	 */
	public void setDialogOptionList(List list) {
		dialogOptionList = list;
	}

	/**
	 * @return
	 */
	public String getID() {
		return ID;
	}

	/**
	 * @param string
	 */
	public void setID(String string) {
		ID = string;
	 }
}

