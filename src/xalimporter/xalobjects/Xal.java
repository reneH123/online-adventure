package adventure.xalimporter.xalobjects;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Rene Haberland
 *
 */
public class Xal {
	
	private String roleSetName;
	private List dialogList;
	private List sceneList;

	public Xal(String roleSetName) {
		this.roleSetName=roleSetName;
		this.dialogList=new ArrayList();
		this.sceneList=new ArrayList();
	}

	/**
	 * @return
	 */
	public List getDialogList() {
		return dialogList;
	}

	/**
	 * @return
	 */
	public List getSceneList() {
		return sceneList;
	}

	/**
	 * @param list
	 */
	public void setDialogList(List list) {
		dialogList = list;
	}

	/**
	 * @param list
	 */
	public void setSceneList(List list) {
		sceneList = list;
	}

	/**
	 * @return
	 */
	public String getRoleSetName() {
		return roleSetName;
	}

	/**
	 * @param string
	 */
	public void setRoleSetName(String string) {
		roleSetName = string;
	}
}

