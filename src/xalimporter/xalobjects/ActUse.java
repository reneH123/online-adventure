package adventure.xalimporter.xalobjects;

/**
 * @author Rene Haberland
 *
 */
public class ActUse {
	private String targetItem;
	private String sourceItem;
	private String actionScript;

	/**
	 * @return
	 */
	public String getSourceItem() {
		return sourceItem;
	}

	/**
	 * @return
	 */
	public String getTargetItem() {
		return targetItem;
	}

	/**
	 * @param string
	 */
	public void setSourceItem(String string) {
		sourceItem = string;
	}

	/**
	 * @param string
	 */
	public void setTargetItem(String string) {
		targetItem = string;
	}

	/**
	 * @return
	 */
	public String getActionScript() {
		return actionScript;
	}

	/**
	 * @param string
	 */
	public void setActionScript(String string) {
		actionScript = string;
	}
}

