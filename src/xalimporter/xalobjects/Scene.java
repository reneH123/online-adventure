package adventure.xalimporter.xalobjects;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Rene Haberland
 *
 */
public class Scene {
	private List itemList;
	private String ID;
	
	/*
	 * Konstruktor
	 */
	public Scene() {
		itemList = new ArrayList();
	}

	/**
	 * @return
	 */
	public List getItemList() {
		return itemList;
	}

	/**
	 * @param list
	 */
	public void setItemList(List list) {
		itemList = list;
	}

	/**
	 * @return
	 */
	public String getID() {
		return ID;
	}

	/**
	 * @param string
	 */
	public void setID(String string) {
		ID = string;
	}
}

