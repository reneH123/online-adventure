package adventure.xalimporter.xalobjects;

/**
 * @author Rene Haberland
 *
 */
public class ActTake {
	
	private String actionScript;

	/**
	 * @return
	 */
	public String getActionScript() {
		return actionScript;
	}

	/**
	 * @param string
	 */
	public void setActionScript(String string) {
		actionScript = string;
	}
}

