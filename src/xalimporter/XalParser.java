package adventure.xalimporter;

import java.util.Iterator;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import adventure.persistence.XALImportHelper;
import adventure.xalimporter.xalobjects.ActLook;
import adventure.xalimporter.xalobjects.ActTake;
import adventure.xalimporter.xalobjects.ActTalk;
import adventure.xalimporter.xalobjects.ActUse;
import adventure.xalimporter.xalobjects.Dialog;
import adventure.xalimporter.xalobjects.DialogOption;
import adventure.xalimporter.xalobjects.Item;
import adventure.xalimporter.xalobjects.Scene;
import adventure.xalimporter.xalobjects.Xal;

/**
 * parses a XML-File (XAL) and saves the values into a db
 * 
 */

//  !!this code here is extremely poor and was originally only intented as a quick hack; totally BUGGY and broken code, do not even try to understand!!

public class XalParser {
	//static final String XALEXAMPLE = "./doc/Implementation/XAL/XALBsp.xml";
	static final String XALEXAMPLE = "./doc/Implementation/XAL/Szene1.xml";
	static final String XALNAME = "XAL-Beispiel";

	private Xal xal;

	/**
	 * parses a XAL and calls a filter-method
	 */
	public XalParser(String xmlURI, String xalName) {
		xal = new Xal(xalName);
		XalImporter imp = new XalImporter(xmlURI);
		Document xalDoc = imp.getXalDocument();
		this.filter(xalDoc);
		this.validate(xalDoc);
		XALImportHelper xalImporterHelper = new XALImportHelper(xal);
		System.out.println("Fertig");
	}

	/**
	 * @param xalDoc
	 */
	private void validate(Document xalDoc) {
		// TODO Auto-generated method stub

	}

	public static void main(String[] args) {
		XalParser test = new XalParser(XALEXAMPLE, XALNAME);

	}

	/**
	 * filters tags and attributes from a XAL and saves the values corresponding to a db
	 */
	private void filter(Document xalDoc) {

		/*
		 * filter Dialogs-tags
		 */

		// list of all nodes with tag 'dialogs'
		NodeList dialogsList = xalDoc.getElementsByTagName("dialogs");
		int dialogsLength = dialogsList.getLength();
		System.out.println("Amount of 'dialogs'-tags:" + dialogsLength);
		int i = 0;
		for (i = 0; i < dialogsLength; i++) {
			System.out.println(i);
			Node dialogs = dialogsList.item(i);
			System.out.println(dialogs.getNodeName() + "\n");

			/*
			 * filter dialog-tags
			 */

			NodeList dialogsChildList = dialogs.getChildNodes();
			int dialogLength = dialogsChildList.getLength();
			int j = 0;
			System.out.println("Dialogs-Kinder:");
			for (j = 0; j < dialogLength; j++) {
				Node dialogsChild = dialogsChildList.item(j);
				if (dialogsChild.getNodeName().equals("dialog")) {
					System.out.println(dialogsChild.getNodeName());

					/*
					 * filter attributes for a dialog-tag
					 */

					NamedNodeMap dialogAttributesList =
						dialogsChild.getAttributes();
					Node dialogAttribut = dialogAttributesList.item(0);

					/*
					 * create Dialog-object with matching ID
					 */
					Dialog dl = new Dialog(dialogAttribut.getNodeValue());
					//					List XalDialogList = this.xal.getDialogList();
					/*
										XalDialogList.add(dl);
										System.out.println(
											"Beginning of Dialog-instance: "
												+ ((Dialog) xal.getDialogList().get(0)).getID()
												+ " End of Dialog-instance");
					*/
					/* ID Required - has exactly 1 attribute with ID*/
					/* Ausgabe von der ID*/
					System.out.println(
						dialogAttribut.getNodeName()
							+ ": "
							+ dialogAttribut.getNodeValue()
							+ "\n");

					/*
					 * filter dialogOption-Tags
					 */

					NodeList dialogChildList = dialogsChild.getChildNodes();
					int dialogChildLength = dialogChildList.getLength();
					int k;
					/* traverse list of all dialogOption-tags */
					for (k = 0; k < dialogChildLength; k++) {
						Node dialogChild = dialogChildList.item(k);
						if (dialogChild.getNodeName().equals("dialogOption")) {

							DialogOption XalDialogOption = new DialogOption();

							System.out.println(dialogChild.getNodeName());
							NamedNodeMap dialogOptionAttributList =
								dialogChild.getAttributes();
							int dialogOptionAttributLength =
								dialogOptionAttributList.getLength();
							int l;
							System.out.println(
								"Output of dialogOption-attributes..");
							for (l = 0; l < dialogOptionAttributLength; l++) {
								Node dialogOptionAttribut =
									dialogOptionAttributList.item(l);
								String dialogOptionAttributName =
									dialogOptionAttribut.getNodeName();

								if (dialogOptionAttributName.equals("ID")) {

									XalDialogOption.setID(
										dialogOptionAttribut.getNodeValue());
									System.out.println(
										dialogOptionAttribut.getNodeName()
											+ ": "
											+ dialogOptionAttribut.getNodeValue()
											+ "\n");

								}
								if (dialogOptionAttributName.equals("text")) {
									XalDialogOption.setText(
										dialogOptionAttribut.getNodeValue());
									System.out.println(
										dialogOptionAttribut.getNodeName()
											+ ": "
											+ dialogOptionAttribut.getNodeValue()
											+ "\n");

								}

							}
							System.out.println(
								"  Output of Child-nodes..\n\n");

							/*
							 * filter dialogOption-tag
							 */

							NodeList responseChildList =
								dialogChild.getChildNodes();
							int responseChildLength =
								responseChildList.getLength();
							int m;
							for (m = 0; m < responseChildLength; m++) {
								Node responseChild = responseChildList.item(m);
								String responseChildName =
									responseChild.getNodeName();

								/*
								 * filter dialogOption
								 */
								if (responseChildName.equals("#text")) {
									// sete ActionScript of the DialogOption-instance
									XalDialogOption.setActionScript(
										responseChild.getNodeValue());

									System.out.println(
										responseChild.getNodeName()
											+ responseChild.getNodeValue());
									NodeList dialogOptionChildList =
										responseChild.getChildNodes();
									if (dialogOptionChildList.getLength()
										== 1) {
										Node dialogOption =
											dialogOptionChildList.item(0);
										System.out.println(
											dialogOption.getNodeName()
												+ ": "
												+ dialogOption.getNodeValue());
									}

								}
							}
							/*
							 * add DialogOption-instance of DialogOptionList (coming from Dialog)
							 */

							List XalDialogDialogOptionList =
								dl.getDialogOptionList();
							XalDialogDialogOptionList.add(XalDialogOption);
							Iterator it = XalDialogDialogOptionList.iterator();
							// debugging
							if (it.hasNext()) {
								DialogOption DialogOptionTest =
									(DialogOption) it.next();
								System.out.println(
									"DialogOption-instance(start):\n"
										+ "ID: "
										+ DialogOptionTest.getID()
										+ "\nText: "
										+ DialogOptionTest.getText()
										+ "\nActionScript: "
										+ DialogOptionTest.getActionScript()
										+ "\nDialogOption-instance(ende)");
							}
						}
					}
					List XalDialogList = this.xal.getDialogList();
					XalDialogList.add(dl);
					System.out.println(
						"start of Dialog-instance: "
							+ ((Dialog) xal.getDialogList().get(0)).getID()
							+ " end of Dialog-instance");
				}
			}
		}

		/*
		 * filter scene-Tags
		 */

		/* list of all nodes with tag 'scene' */
		NodeList sceneList = xalDoc.getElementsByTagName("scene");
		int sceneLength = sceneList.getLength();
		System.out.println(sceneLength);
		int p = 0;
		for (p = 0; p < sceneLength; p++) {
			Node scene = sceneList.item(p);
			System.out.println(scene.getNodeName());
			// create Scene-instance
			Scene XalScene = new Scene();
			List XalSceneList = xal.getSceneList();

			/*
			 * filter ID-Attribut for scene
			 */
			NamedNodeMap sceneAttributList = scene.getAttributes();
			int sceneAttributListLength = sceneAttributList.getLength();
			int pa = 0;
			for (pa = 0; pa < sceneAttributListLength; pa++) {
				Node sceneAttribut = sceneAttributList.item(pa);
				String sceneAttributName = sceneAttribut.getNodeName();
				if (sceneAttributName.equals("ID")) {
					// set ID in the Scene-instance
					XalScene.setID(sceneAttribut.getNodeValue());
					System.out.println(
						"Scene-ID:"
							+ sceneAttribut.getNodeName()
							+ ": "
							+ sceneAttribut.getNodeValue());
				}

			}

			/*
			 * filter item-tags
			 */
			NodeList sceneChildList = scene.getChildNodes();
			int sceneChildLength = sceneChildList.getLength();
			int q = 0;
			for (q = 0; q < sceneChildLength; q++) {
				Node sceneChild = sceneChildList.item(q);
				if (sceneChild.getNodeName().equals("item")) {
					System.out.println(sceneChild.getNodeName());
					Item XalItem = new Item();

					NamedNodeMap itemAttributList = sceneChild.getAttributes();
					int itemAttributListLength = itemAttributList.getLength();
					int r = 0;
					for (r = 0; r < itemAttributListLength; r++) {
						Node itemAttribut = itemAttributList.item(r);
						String itemAttributName = itemAttribut.getNodeName();
						if (itemAttributName.equals("ID")) {
							System.out.println(
								itemAttributName
									+ ": "
									+ itemAttribut.getNodeValue());
							XalItem.setID(itemAttribut.getNodeValue());
						}

						else if (itemAttributName.equals("img")) {
							System.out.println(
								itemAttributName
									+ ": "
									+ itemAttribut.getNodeValue());
							XalItem.setImagePath(itemAttribut.getNodeValue());
						}

						/*
						 * posX
						 */
						else if (itemAttributName.equals("posX")) {
							System.out.println(
								itemAttributName
									+ ": "
									+ itemAttribut.getNodeValue());
							XalItem.setPosX(itemAttribut.getNodeValue());

						}

						/*
						 * posY
						 */
						else if (itemAttributName.equals("posY")) {
							System.out.println(
								itemAttributName
									+ ": "
									+ itemAttribut.getNodeValue());
							XalItem.setPosY(itemAttribut.getNodeValue());
						}

						/*
						 * visible
						 */
						else if (itemAttributName.equals("visible")) {
							System.out.println(
								itemAttributName
									+ ": "
									+ itemAttribut.getNodeValue());
							XalItem.setVisible(itemAttribut.getNodeValue());
						}

						/*
						 * level filtern
						 */
						else if (itemAttributName.equals("level")) {
							System.out.println(
								itemAttributName
									+ ": "
									+ itemAttribut.getNodeValue());
							XalItem.setLevel(itemAttribut.getNodeValue());
						}

						/*
						 * filter description
						 */
						else if (itemAttributName.equals("description")) {
							System.out.println(
								itemAttributName
									+ ": "
									+ itemAttribut.getNodeValue());
							XalItem.setDescription(itemAttribut.getNodeValue());
						}
					}
					/*
					 * filter itemChild-tags
					 */
					NodeList itemChildList = sceneChild.getChildNodes();
					int itemChildLength = itemChildList.getLength();
					int s = 0;
					for (s = 0; s < itemChildLength; s++) {
						Node itemChild = itemChildList.item(s);
						String itemChildName = itemChild.getNodeName();

						/*
						 * filter actTake-tag
						 */
						if (itemChildName.equals("actTake")) {
							ActTake XalItemActTake = new ActTake();
							System.out.println(
								"    " + itemChild.getNodeName());
							NamedNodeMap actTakeAttributeList =
								itemChild.getAttributes();
							int actTakeAttributeLength =
								actTakeAttributeList.getLength();
							int u = 0;

							/*for (u = 0; u < actTakeAttributeLength; u++) {
								Node actTakeAttribute =
								actTakeAttributeList.item(u);
							*/
							/*
							 * text-Attribut filtern
							 */
							/*	if (actTakeAttribute
									.getNodeName()
									.equals("text")) {
									System.out.println(
										"    "
											+ actTakeAttribute.getNodeName()
											+ ": "
											+ actTakeAttribute.getNodeValue());
								}
							}
							*/
							/*
							 * #text f�r actTake filtern
							 */
							NodeList actTakeChildList =
								itemChild.getChildNodes();
							int actTakeChildLength =
								actTakeChildList.getLength();
							int sa = 0;
							for (sa = 0; sa < actTakeChildLength; sa++) {
								Node actTakeChild = actTakeChildList.item(sa);
								String actTakeChildName =
									actTakeChild.getNodeName();
								if (actTakeChildName.equals("#text")) {
									//#text-Tag in actTake-Klasse setzen
									XalItemActTake.setActionScript(
										actTakeChild.getNodeValue());
									System.out.println(
										actTakeChild.getNodeName()
											+ ": "
											+ actTakeChild.getNodeValue());
								}

							}
							// ActTake in ActTakeListe von Item aufnehmen
							List XalItemActTakeList = XalItem.getActTakeList();
							XalItemActTakeList.add(XalItemActTake);
							XalItem.setActTakeList(XalItemActTakeList);

						}
						/*
						 * actLook-Tag filtern
						 */
						if (itemChildName.equals("actLook")) {
							// ActLook-Klasse erstellen
							ActLook XalItemActLook = new ActLook();
							System.out.println(
								"    " + itemChild.getNodeName());
							NamedNodeMap actLookAttributeList =
								itemChild.getAttributes();
							int actLookAttributeLength =
								actLookAttributeList.getLength();
							int v = 0;
							for (v = 0; v < actLookAttributeLength; v++) {
								Node actLookAttribute =
									actLookAttributeList.item(v);
								/*
								 * text-Attribut filtern
								 */
								/*
								if (actLookAttribute
									.getNodeName()
									.equals("text")) {
									System.out.println(
										"    "
											+ actLookAttribute.getNodeName()
											+ ": "
											+ actLookAttribute.getNodeValue());
								}
								*/
							}
							/*
							 * #text f�r actLook filtern
							 */
							NodeList actLookChildList =
								itemChild.getChildNodes();
							int actLookChildLength =
								actLookChildList.getLength();
							int va = 0;
							for (va = 0; va < actLookChildLength; va++) {
								Node actLookChild = actLookChildList.item(va);
								String actLookChildName =
									actLookChild.getNodeName();
								if (actLookChildName.equals("#text")) {
									//#text-Tag in actTake-Klasse setzen
									XalItemActLook.setActionScript(
										actLookChild.getNodeValue());
									System.out.println(
										actLookChild.getNodeName()
											+ ": "
											+ actLookChild.getNodeValue());
								}
							}
							// ActTake in ActTakeListe von Item aufnehmen
							List XalItemActLookList = XalItem.getActLookList();
							XalItemActLookList.add(XalItemActLook);
							XalItem.setActLookList(XalItemActLookList);
						}
						/*
						 * actTalk-Tag filtern
						 */
						if (itemChildName.equals("actTalk")) {
							ActTalk XalItemActTalk = new ActTalk();
							System.out.println(
								"    " + itemChild.getNodeName());
							NamedNodeMap actTalkAttributeList =
								itemChild.getAttributes();
							int actTalkAttributeLength =
								actTalkAttributeList.getLength();
							int v = 0;
							for (v = 0; v < actTalkAttributeLength; v++) {
								Node actTalkAttribute =
									actTalkAttributeList.item(v);
								/*
								 * text-Attribut filtern
								 */
								if (actTalkAttribute
									.getNodeName()
									.equals("text")) {
									System.out.println(
										"    "
											+ actTalkAttribute.getNodeName()
											+ ": "
											+ actTalkAttribute.getNodeValue());
								}
								/*
								 * dialogID-Attribut filtern
								 */
								if (actTalkAttribute
									.getNodeName()
									.equals("dialogID")) {
									System.out.println(
										"    "
											+ actTalkAttribute.getNodeName()
											+ ": "
											+ actTalkAttribute.getNodeValue());
								}

							}
							/*
							 * #text f�r actTalk filtern
							 */
							NodeList actTalkChildList =
								itemChild.getChildNodes();
							int actTalkChildLength =
								actTalkChildList.getLength();
							int va = 0;
							for (va = 0; va < actTalkChildLength; va++) {
								Node actTalkChild = actTalkChildList.item(va);
								String actTalkChildName =
									actTalkChild.getNodeName();
								if (actTalkChildName.equals("#text")) {
									//#text-Tag in actTake-Klasse setzen
									XalItemActTalk.setActionScript(
										actTalkChild.getNodeValue());
									System.out.println(
										actTalkChild.getNodeName()
											+ ": "
											+ actTalkChild.getNodeValue());
								}
							}

							/*
							 * removeItem- und addItem-Tags filtern
							 */
							/*NodeList actTalkChildList =
								itemChild.getChildNodes();
							int actTalkChildLength =
								actTalkChildList.getLength();
							int w = 0;
							
							for (w = 0; w < actTalkChildLength; w++) {
								Node actTalkChild = actTalkChildList.item(w);
							*/
							/*
							 * removeItem-Tag filtern
							 */
							/*	if (actTalkChild
									.getNodeName()
									.equals("removeItem")) {
									System.out.println(
										"      " + actTalkChild.getNodeName());
									NamedNodeMap removeItemAttributeList =
										actTalkChild.getAttributes();
									int removeItemAttributeLength =
										removeItemAttributeList.getLength();
									int x = 0;
									for (x = 0;
										x < removeItemAttributeLength;
										x++) {
										Node removeItemAttribute =
											removeItemAttributeList.item(x);
							*/
							/*
							 * ID-Attribut filtern
							 */
							/*
										if (removeItemAttribute
											.getNodeName()
											.equals("ID")) {
											System.out.println(
												"      "
													+ removeItemAttribute
														.getNodeName()
													+ ": "
													+ removeItemAttribute
														.getNodeValue());
										}
							*/
							/*
							 * locID-Attribute filtern
							 */
							/*
										if (removeItemAttribute
											.getNodeName()
											.equals("locID")) {
											System.out.println(
												"      "
													+ removeItemAttribute
														.getNodeName()
													+ ": "
													+ removeItemAttribute
														.getNodeValue());
										}
									}
								}
								*/

							/* 
							 * addItem-Tag filtern 
							 */
							/*
							if (actTalkChild
								.getNodeName()
								.equals("addItem")) {
								System.out.println(
									"      " + actTalkChild.getNodeName());
								NamedNodeMap addItemAttributeList =
									actTalkChild.getAttributes();
								int addItemAttributeLength =
									addItemAttributeList.getLength();
								int y = 0;
								for (y = 0;
									y < addItemAttributeLength;
									y++) {
									Node addItemAttribute =
										addItemAttributeList.item(y);
							*/
							/*
							 * call-Attribut filtern
							 */
							/*
									if (addItemAttribute
										.getNodeName()
										.equals("call")) {
										System.out.println(
											"      "
												+ addItemAttribute
													.getNodeName()
												+ ": "
												+ addItemAttribute
													.getNodeValue());
									}
							*/

							/*
							 * ID-Attribut filtern
							 */
							/*
									if (addItemAttribute
										.getNodeName()
										.equals("ID")) {
										System.out.println(
											"      "
												+ addItemAttribute
													.getNodeName()
												+ ": "
												+ addItemAttribute
													.getNodeValue());
									}
							*/

							/*
							 * target-Attribut filtern
							 */
							/*
									if (addItemAttribute
										.getNodeName()
										.equals("target")) {
										System.out.println(
											"      "
												+ addItemAttribute
													.getNodeName()
												+ ": "
												+ addItemAttribute
													.getNodeValue());
									}
							
							
								}
							
							}
							
							}
							*/
							// ActTalk in ActTalkListe von Item aufnehmen
							List XalItemActTalkList = XalItem.getActTalkList();
							XalItemActTalkList.add(XalItemActTalk);
							XalItem.setActTalkList(XalItemActTalkList);

						}

						/*
						 * actUse-Tag filtern
						 */
						if (itemChildName.equals("actUse")) {
							// ActLook-Klasse erstellen
							ActUse XalItemActUse = new ActUse();
							System.out.println(
								"    " + itemChild.getNodeName());
							/*
							 * actUse-Attribute filtern
							 */
							NamedNodeMap actUseAttributeList =
								itemChild.getAttributes();
							int actUseAttributeLength =
								actUseAttributeList.getLength();
							int z = 0;
							for (z = 0; z < actUseAttributeLength; z++) {
								Node actUseAttribute =
									actUseAttributeList.item(z);
								/*
								 * targetItem-Attribut filtern
								 */
								if (actUseAttribute
									.getNodeName()
									.equals("targetItem")) {
									XalItemActUse.setTargetItem(
										actUseAttribute.getNodeValue());
									System.out.println(
										"      "
											+ actUseAttribute.getNodeName()
											+ ": "
											+ actUseAttribute.getNodeValue());
								}

								/*
								 * sourceItem-Attribut filtern
								 */
								if (actUseAttribute
									.getNodeName()
									.equals("sourceItem")) {
									XalItemActUse.setSourceItem(
										actUseAttribute.getNodeValue());
									System.out.println(
										"      "
											+ actUseAttribute.getNodeName()
											+ ": "
											+ actUseAttribute.getNodeValue());
								}

								/*
								 * ID-Attribut filtern
								 */
								/*
								if (actUseAttribute
									.getNodeName()
									.equals("ID")) {
									System.out.println(
										"      "
											+ actUseAttribute.getNodeName()
											+ ": "
											+ actUseAttribute.getNodeValue());
								}
								*/

								/*
								 * call-Attribut filtern
								 */
								/*
								if (actUseAttribute
									.getNodeName()
									.equals("call")) {
									System.out.println(
										"      "
											+ actUseAttribute.getNodeName()
											+ ": "
											+ actUseAttribute.getNodeValue());
								}
								*/

							}

							/*NodeList actUseChildList =
								itemChild.getChildNodes();
							int actUseChildLength = actUseChildList.getLength();
							int a = 0;
							for (a = 0; a < actUseChildLength; a++) {
								Node actUseChild = actUseChildList.item(a);
							*/
							/*
							 * showMessage-Tag filtern
							 */
							/*
								if (actUseChild
									.getNodeName()
									.equals("showMessage")) {
									System.out.println(
										"      " + actUseChild.getNodeName());
							*/
							/*
							 * show-Message-Attribute filtern
							 */
							/*
									NamedNodeMap showMessageAttributeList =
										actUseChild.getAttributes();
									int showMessageAttributeLength =
										showMessageAttributeList.getLength();
									int b = 0;
									for (b = 0;
										b < showMessageAttributeLength;
										b++) {
										Node showMessageAttribute =
											showMessageAttributeList.item(b);
							*/
							/*
							 * text-Attribut filtern
							 */
							/*
										if (showMessageAttribute
											.getNodeName()
											.equals("text")) {
											System.out.println(
												"      "
													+ showMessageAttribute
														.getNodeName()
													+ ": "
													+ showMessageAttribute
														.getNodeValue());
										}
							
									}
							
								}
							*/

							/*
							 * removeItem-Tag filtern
							 */
							/*	
								if (actUseChild
									.getNodeName()
									.equals("removeItem")) {
									System.out.println(
										"     " + actUseChild.getNodeName());
							*/
							/*
							 * removeItem-Attribute filtern
							 */
							/*
									NamedNodeMap removeItemAttributeList =
										actUseChild.getAttributes();
									int removeItemAttributeLength =
										removeItemAttributeList.getLength();
									int c = 0;
									for (c = 0;
										c < removeItemAttributeLength;
										c++) {
										Node removeItemAttribute =
											removeItemAttributeList.item(c);
							*/
							/*
							 * ID-Attribut filtern
							 */
							/*
										if (removeItemAttribute
											.getNodeName()
											.equals("ID")) {
											System.out.println(
												"    "
													+ removeItemAttribute
														.getNodeName()
													+ ": "
													+ removeItemAttribute
														.getNodeValue());
										}
							*/

							/*
							 * locID-Attribut filtern
							 */
							/*
										if (removeItemAttribute
											.getNodeName()
											.equals("locID")) {
											System.out.println(
												"    "
													+ removeItemAttribute
														.getNodeName()
													+ ": "
													+ removeItemAttribute
														.getNodeValue());
										}
							
									}
							
								}
							*/

							/*
							 * addItem-Tag filtern
							 */
							/*
								if (actUseChild
									.getNodeName()
									.equals("addItem")) {
									System.out.println(
										"       " + actUseChild.getNodeName());
							*/
							/*
							 * addItem-Attribute filtern
							 */
							/*
									NamedNodeMap addItemAttributeList =
										actUseChild.getAttributes();
									int addItemAttributeLength =
										addItemAttributeList.getLength();
									int d = 0;
									for (d = 0;
										d < addItemAttributeLength;
										d++) {
										Node addItemAttribute =
											addItemAttributeList.item(d);
							*/
							/*
							 * call-Attribut filtern
							 */
							/*
										if (addItemAttribute
											.getNodeName()
											.equals("call")) {
											System.out.println(
												"     "
													+ addItemAttribute
														.getNodeName()
													+ ":"
													+ addItemAttribute
														.getNodeValue());
										}
							*/

							/*
							 * ID-Attribut filtern
							 */
							/*
										if (addItemAttribute
											.getNodeName()
											.equals("ID")) {
											System.out.println(
												"     "
													+ addItemAttribute
														.getNodeName()
													+ ":"
													+ addItemAttribute
														.getNodeValue());
										}
							*/

							/*
							 * target-Attribut filtern
							 */
							/*
										if (addItemAttribute
											.getNodeName()
											.equals("target")) {
											System.out.println(
												"     "
													+ addItemAttribute
														.getNodeName()
													+ ":"
													+ addItemAttribute
														.getNodeValue());
										}
							
									}
							
								}
							*/
							/*
							 * changeLocation-Tag filtern
							 */
							/*
								if (actUseChild
									.getNodeName()
									.equals("changeLocation")) {
									System.out.println(
										"      " + actUseChild.getNodeName());
							*/
							/*
							 * changeLocation-Attribute filtern
							 */
							/*
									NamedNodeMap changeLocationAtrtibuteList =
										actUseChild.getAttributes();
									int changeLocationAttributeLength =
										changeLocationAtrtibuteList.getLength();
									int e = 0;
									for (e = 0;
										e < changeLocationAttributeLength;
										e++) {
										Node changeLocationAttribute =
											changeLocationAtrtibuteList.item(e);
							*/
							/*
							 * target-Attribut filtern
							 */
							/*
										if (changeLocationAttribute
											.getNodeName()
											.equals("target")) {
											System.out.println(
												"      "
													+ changeLocationAttribute
														.getNodeName()
													+ ": "
													+ changeLocationAttribute
														.getNodeValue());
										}
									}
							
								}
								
							}
							*/
							/*
							 * #text f�r actUse filtern
							 */
							NodeList actUseChildList =
								itemChild.getChildNodes();
							int actUseChildLength = actUseChildList.getLength();
							int va = 0;
							for (va = 0; va < actUseChildLength; va++) {
								Node actUseChild = actUseChildList.item(va);
								String actUseChildName =
									actUseChild.getNodeName();
								if (actUseChildName.equals("#text")) {
									//#text-Tag in actUse-Klasse setzen
									XalItemActUse.setActionScript(
										actUseChild.getNodeValue());
									System.out.println(
										actUseChild.getNodeName()
											+ ": "
											+ actUseChild.getNodeValue());
								}
								// ActUse in ActUseListe von Item aufnehmen
								List XalItemActUseList =
									XalItem.getActUseList();
								XalItemActUseList.add(XalItemActUse);
								XalItem.setActUseList(XalItemActUseList);
							}
						}
					}
					// Item einer SceneList zuordnen
					List XalSceneItemList = XalScene.getItemList();
					XalSceneItemList.add(XalItem);
				}
			}
			XalSceneList.add(XalScene);
		}
	}
}

