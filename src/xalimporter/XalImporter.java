package adventure.xalimporter;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * parses a XAL
 */
public class XalImporter
{
	/**
	 * DOM representing a given XAL-file
	 */
	private Document xalDocument = null;
	

	/**
	 * parses a XAL
	 * @param xalPath - Pfad zu einer XAL
	 */
	public XalImporter(String xalPath)
	{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setValidating(true);
		try
		{
			DocumentBuilder builder = factory.newDocumentBuilder();
			this.xalDocument = builder.parse(new File(xalPath));
		}
		catch (ParserConfigurationException e)
		{
			System.out.println("\nParserConfigurationException:");
			e.printStackTrace();
		}
		catch (SAXException e)
		{
			System.out.println("\nSAXException:");
			e.printStackTrace();
		}
		catch (IOException e)
		{
			System.out.println("\nIOException:");
			e.printStackTrace();
		}
	}

	/**
	 * returns the DOM
	 */
	public Document getXalDocument()
	{
		return this.xalDocument;
	}
}

