/*
* Created on Dec 15, 2003
*/
package adventure.server;

import java.rmi.RemoteException;
import java.util.*;

import adventure.gameobjects.*;

import adventure.persistence.Updater;
import adventure.persistence.BOFactory;
import adventure.persistence.SimpleQueryTool;
import adventure.persistence.ConnectionData;

public class ServerApplication {

    /**
     * database configuration: name, user, etc.
     */
    private static final String DATABASE_NAME = "adventure";
    private static final String DATABASE_USER = "hans";
    private static final String DATABASE_PASS = "test";
    private static final String DATABASE_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DATABASE_PORT = "3306";
    private static final long TIMEOUT = 1800000;

    // time elapse before games need to be rechecked: 10 min
    private static final int CHECK_INTERVAL = 600000;

    /**
     * Singleton instance of the server
     */
    private static ServerApplication instance;

    /**
     * port for the RMI registry service
     */
    private static final int PORT = 1099;

    /** 
     * all games managed by the serve at a time. Key is ID which is returned by createGame
     */
    private Hashtable games = new Hashtable();

    /**
     * communication interface with the server
     */
    private ServerCommunicator serverCommunicator;

    /**
     * thread controlling the game instances
     */
    private TimeoutThread timeoutThread;

    /**
     * db updater
     */
    private Updater updater;

    /**
     * db BOFactory
     */
    private BOFactory boFactory;

    /**
     * db SimpleQueryTool
     */
    private SimpleQueryTool simpleQueryTool;
    private static final int MAX_ONLINE_USERS = 100;
    private static final int MAX_REG_USERS = 1000;

    private List deletableRules = new ArrayList();

    /**
     * constructor for the server -- ServerCommunicator is instantiated and started
     */
    private ServerApplication() {
        try {
           /* ConnectionData conData = new ConnectionData();
            conData.setDriver(DATABASE_DRIVER);
            conData.setDriverPath(DATABASE_DRIVER);
            conData.setDatabaseName(DATABASE_NAME);
            conData.setPort(DATABASE_PORT);
            conData.setPassword(DATABASE_PASS);
            conData.setUser(DATABASE_USER);
              */
            serverCommunicator = new ServerCommunicator(PORT);
            serverCommunicator.startServer();
            /*
            updater = new Updater(conData);
            boFactory = new BOFactory(conData);
            simpleQueryTool = new SimpleQueryTool(conData);

        } catch(RemoteException re) {
            System.err.println("Unable to create ServerCommunicator:");
            System.err.println(re.getMessage());
            System.exit(0);*/
        } catch(Exception e) {
            System.err.println("Unable to start ServerCommunicator:");
            System.err.println(e.getMessage());
            System.exit(0);
        }
        timeoutThread = new TimeoutThread(CHECK_INTERVAL);
        timeoutThread.start();

        /*String[] t = new String[] {
        "setFlag(test2);",
        "if flag set(test1) {",
        "    addItem(2,3);",
        "    if flag not set(test2) {",
        "        addItem(3,4);",
        "    } else {",
        "        addItem(4,5);",
        "    }",
        "}"};
        Hashtable flags = new Hashtable();
        flags.put("test2", new Boolean(true));
        Scene sc = new Scene(new Location(-1, "test"));
        ScriptInterpreter inter = new ScriptInterpreter();
        try {
        inter.interprete(t, sc, flags);
        } catch(Exception ex) {
        System.out.println("Fehler bei ScriptIntepreter: " +ex.getMessage());
        }*/

    }

    /**
     * checks all running games for signals. A signal signs the need for interaction.
     * If a game times out (>TIMEOUT secs), the game is stored and quit.
     */
    public void checkGames() {
        Iterator iter = games.values().iterator();
        while(iter.hasNext()) {
            Game game = (Game)iter.next();
            if( (new Date()).getTime() - game.getLastInteractive().getTime() > TIMEOUT) {
                try {
                    game.save();
                } catch (Exception e) {
                    // couldn't save
                    return;
                }
                game.quit();
                games.remove(new Integer(game.getId()));
            }
        }
    }

    /**
     * returns the singleton server instance
     */
    public static ServerApplication getInstance() {
        if(instance == null)
            instance = new ServerApplication();
        return instance;
    }

    /**
     * creates a new game for the specified user and returns its unique ID
     */
    public int createGame(User user) {
        int id = user.getUserName().hashCode();
        user.setState(User.USER_ONLINE);
        int append = 0;
        while( games.get(new Integer(id)) != null ) {
            append ++;
            id += append;
        }
        Game game = new Game(id);
        game.setUser(user);
        games.put(new Integer(id), game);
        System.gc();
        return id;
    }

    /**
     * returns the game specified by ID
     */
    private Game getGame(int gameId) throws Exception {
        Game game = (Game)games.get(new Integer(gameId));
        if(game == null)
            throw new Exception("No such game: "+gameId);
        return game;
    }

    /**
     * stores the game specified
     */
    public void saveGame(int gameId, String saveGameName) throws Exception {
        Game game;
        game = getGame(gameId);
        game.save(saveGameName);
    }

    /**
     * logs a player in with username and password
     */
    public int login(String username, String password) throws Exception {
        // Zu Testzwecken
        if(username.equals("tester") && password.equals("test")) {
            User u = new User("Vorname test","test", "nickname test","test@test.com", "test", 0, User.USER_OFFLINE);
            return createGame(u);
        }
        return -1;

/*      ready for production version:
User user = simpleQueryTool.getUser(username, password);
return createGame(user);
*/
    }

    /**
     * loads some Savegame
     */
    public Scene loadGame(int gameId, String saveGameName) throws Exception {
        Game game = null;

        game = getGame(gameId);
        game.load(saveGameName);

        return game.getCurrentScene();
    }

    /**
     * quits the game with ID gameId
     */
    public void quitGame(int gameId) throws Exception {
        Game game = null;
        game = getGame(gameId);
        game.quit();
        games.remove(new Integer(gameId));
    }

    public Updater getUpdater() {
        return updater;
    }

    public BOFactory getBOFactory() {
        return boFactory;
    }

    public List getRules() throws Exception {
        return simpleQueryTool.getRules();
    }

    public void delRules(RuleSet ruleSet) throws Exception {
        boolean mayDelete = true;
        // If somebody still is playing, the rules only are locked
        Iterator iter = games.values().iterator();
        while(iter.hasNext()) {
            Game game = (Game) iter.next();
            if(game.getRuleSetName().equals(ruleSet.getRuleSetName())) {
                ruleSet.setState(false);
                simpleQueryTool.updateRuleState(ruleSet);
                mayDelete = false;
                deletableRules.add(ruleSet);
                break;
            }
        }
        if(mayDelete)
            simpleQueryTool.delRules(ruleSet);
    }

    public void updateRuleState(RuleSet ruleSet) throws Exception {
        simpleQueryTool.updateRuleState(ruleSet);
    }

    public  List getUserList() throws Exception {
        List res = new ArrayList();
        List l = simpleQueryTool.getUserList();
        Iterator iter = l.iterator();
        while(iter.hasNext()) {
            User u = (User) iter.next();
            Iterator gameIter = games.values().iterator();
            while(gameIter.hasNext()) {
                Game game = (Game) gameIter.next();
                if(game.getUser().getID() == u.getID())
                    u.setState(User.USER_ONLINE);
            }
            res.add(u);
        }
        return res;
    }

    public User getUserInfo(String userName) throws Exception {

        return simpleQueryTool.getUser(userName);
    }

    public void deleteUser(User user) throws Exception {
        // logout if user is online
        Iterator iter = games.values().iterator();
        while(iter.hasNext()) {
            Game game = (Game)iter.next();
            User u = game.getUser();
            if(u.equals(user)) {
                game.quit();
                games.remove(game);
            }
        }
        simpleQueryTool.deleteUser(user);
    }

    public int getMaxOnlineUsers() {
        return MAX_ONLINE_USERS;
    }

    public  void setMaxOnlineUsers(int maxOnlineUsers) {
        // TODO Implement me
    }

    public int getMaxRegUsers() {
        return MAX_REG_USERS;
    }

    public void setMaxRegUsers(int maxRegUsers) {
        // TODO Implement me
    }

    public void registerUser(User user) throws Exception {
        simpleQueryTool.registerUser(user);
    }

    public void updateUser(User user) throws Exception {
        simpleQueryTool.updateUser(user);
    }

    public void deleteSaveGame(int saveGameID) throws Exception {
        simpleQueryTool.deleteSaveGame(saveGameID);
    }

    public Scene sendAction(int gameID, UserAction userAction) throws Exception {
        Game game = (Game) games.get(new Integer(gameID));
        return game.executeAction(userAction);
    }

    public static void main(String[] args) {
        ServerApplication app = ServerApplication.getInstance();
    }

    public Scene getCurrentScene(int gameId) throws Exception {
        Game game = getGame(gameId);
        return game.getCurrentScene();
    }

    public void logout(int gameID) {
        Game game = null;
        try {
            game = getGame(gameID);
        } catch (Exception e) {
            // quite, even if game data could not be found
        }
        game.quit();
        games.remove(game);
    }

    protected void finalize() throws Throwable {

            super.finalize();

    }

    public List getUserList(RuleSet ruleSet) {
        List result = new ArrayList();
        Iterator iter = games.values().iterator();
        while(iter.hasNext()) {
            Game game = (Game) iter.next();
            if(game.getRuleSetName().equals(ruleSet.getRuleSetName())) {
                game.getUser().setState(User.USER_ONLINE);
                result.add(game.getUser());
            }
        }
        return result;
    }

    public void shutdown() {
        try {
            // delete removable rules
            Iterator iter = deletableRules.iterator();
            while(iter.hasNext()) {
                RuleSet rules = (RuleSet) iter.next();
                simpleQueryTool.delRules(rules);
            }
            serverCommunicator.shutdownCommunicator();
            timeoutThread.stopMe();
            boFactory = null;
            simpleQueryTool = null;
            updater = null;
            System.gc();
        } catch(Exception ex) {
            ex.printStackTrace();
        } finally {
            System.exit(0);
        }
    }
}

