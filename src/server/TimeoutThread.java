package adventure.server;

public class TimeoutThread extends Thread {
    /**
     * time between 2 epoches
     */
    private int timeout;

    private boolean running;

    public TimeoutThread(int timeout) {
        this.timeout = timeout;
    }

    public void run() {
        running = true;
        while(isAlive() && running) {
            try {
                sleep(timeout);
            } catch(Exception e) {}
            ServerApplication.getInstance().checkGames();
        }
    }

    public void stopMe() {
        running = false;
    }
}

