package adventure.server;

import java.util.Date;
import java.util.List;
import java.util.Hashtable;
import java.util.Iterator;

import adventure.gameobjects.*;

public class Game {

    /**
     * the ScriptInterpreter-instance associated with this game
     */
    private ScriptInterpreter interpreter;

    /**
     * the unique ID of this game (managed by the server, not by the db)
     */
    private int id;

    /**
     * the user associated with this game
     */
    private User user;

    /**
     * last interaction
     */
    private Date lastInteractive;

    /**
     * date when game starts
     */
    private Date startedAt;

    /**
     * current scene
     */
    private Scene currentScene;

    /**
     * scenes of the game
     */
    private List scenes;

    /**
     * game inventory
     */
    private List inventory;

    /**
     * dialog options of this game, hash table with ID as key and some DialogOption instance as value
     */
    private Hashtable dialogOptions;

    /**
     * name of the ruleset
     */
    private String ruleSetName;

    /**
     * hash table with flags
     * key = flagname, value = Boolean
     */
    private Hashtable flags;
    private Scene nextScene;

    /**
     * constructor. creates a new game with ID provided
     */
    public Game(int gameId) {
        this.id = gameId;
        this.lastInteractive = new Date();
        this.startedAt = new Date();
        this.interpreter = new ScriptInterpreter(this);

    }


    /**
     * quits the game and cleans up
     */
    public void quit() {
        this.ruleSetName = "";
        this.scenes = null;
        this.dialogOptions = null;
        this.flags = null;
        this.inventory = null;
        this.currentScene = null;
        this.nextScene = null;
        System.gc();
    }

    /**
     * stores this game under the name specified
     */
    public void save(String saveGameName) throws Exception {
        lastInteractive = new Date();
        ServerApplication.getInstance().getUpdater().saveGame(this, saveGameName);
    }

    /**
     * stores the game into a Quicksave ranking
     */
    public void save() throws Exception {
        lastInteractive = new Date();
        ServerApplication.getInstance().getUpdater().saveGame(this, "QUICKSAVE");
    }

    /**
     * loads the game with the name specified
     */
    public void load(String saveGameName) throws Exception {
        lastInteractive = new Date();
        ServerApplication.getInstance().getBOFactory().loadGame(this, saveGameName);
    }

    /**
     * returns the actual scene
     */
    public Scene getCurrentScene() {
        lastInteractive = new Date();
        return currentScene;
    }

    /**
     * returns the ID of this game
     */
    public int getId() {
        lastInteractive = new Date();
        return id;
    }

    /**
     * returns the date of the last interaction
     */
    public Date getLastInteractive() {
        lastInteractive = new Date();
        return lastInteractive;
    }

    /**
     * returns the user associated with this game
     */
    public User getUser() {
        lastInteractive = new Date();
        return user;
    }

    /**
     * assigns this game to a user
     */
    public void setUser(User user) {
        lastInteractive = new Date();
        this.user = user;
    }

    public Date getStartedAt() {
        lastInteractive = new Date();
        return startedAt;
    }

    public List getScenes() {
        lastInteractive = new Date();
        return scenes;
    }

    public List getInventory() {
        lastInteractive = new Date();
        return inventory;
    }

    public Hashtable getDialogOptions() {
        lastInteractive = new Date();
        return dialogOptions;
    }

    public String getRuleSetName() {
        lastInteractive = new Date();
        return ruleSetName;
    }

    public DialogOption getDialogOption(String s) {
        lastInteractive = new Date();
        return (DialogOption) dialogOptions.get(new Integer(s));
    }

    public Scene executeAction(UserAction userAction) throws Exception {
        Item item = currentScene.getLocation().getItem(userAction.getItem1());
        switch(userAction.getKindOfAktion()) {
            case UserAction.ACTION_USE:
                String key = userAction.getIem2();
                if(key == null) key = "SELF";
                if(item.isValidPartner(key)) {
                    String[] script = item.getActionUse(key);
                    interpreter.interprete(script, currentScene, flags);
                } else {
                    throw new Exception("Can't use it.");
                }
                break;
            case UserAction.ACTION_TALK:
                interpreter.interprete(item.getActionTalk(), currentScene, flags);
                break;
            case UserAction.ACTION_TAKE:
                interpreter.interprete(item.getActionTake(), currentScene, flags);
                break;
            case UserAction.ACTION_LOOK:
                interpreter.interprete(item.getActionLook(), currentScene, flags);
                break;
        }
        if(nextScene != null) {
            doChangeScene();
        }
        currentScene.setInventory(inventory);
        return currentScene;
    }

    public void doChangeScene() throws Exception {
        currentScene = nextScene;
        nextScene = null;
        // execute domefirst item
        Item domefirst = currentScene.getLocation().getItem("domefirst");
        interpreter.interprete(domefirst.getActionLook(), currentScene, flags);
        if(nextScene != null)
            doChangeScene();
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCurrentScene(Scene currentScene) {
        this.currentScene = currentScene;
    }

    public void setScenes(List scenes) {
        this.scenes = scenes;
    }

    public void setInventory(List inventory) {
        this.inventory = inventory;
    }

    public void setDialogOptions(Hashtable dialogOptions) {
        this.dialogOptions = dialogOptions;
    }

    public void setRuleSetName(String ruleSetName) {
        this.ruleSetName = ruleSetName;
    }

    public Hashtable getFlags() {
        return flags;
    }

    public void setFlags(Hashtable flags) {
        this.flags = flags;
    }

    public void changeLocation(String s) {
        Iterator iter = scenes.iterator();
        while(iter.hasNext()) {
            Scene scene = (Scene) iter.next();
            if(scene.getLocation().getLocationName().equals(s)) {
                nextScene = scene;
                return;
            }
            nextScene = null;
        }
    }
}

