/*
* Created on Dec 15, 2003
*/
package adventure.server;

import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;

import adventure.gameobjects.UserAction;
import adventure.gameobjects.Scene;
import adventure.gameobjects.User;
import adventure.gameobjects.RuleSet;

public class ServerCommunicator extends UnicastRemoteObject implements Server {
    private int port;
    private final String SERVICE_NAME = "RMI_ServerCommunicator";
    private Registry registry;

    /**
     * @throws RemoteException
     */
    protected ServerCommunicator(int port) throws RemoteException {
        super();
        this.port = port;
    }

    /**
     * Create a registry and bind the Service.
     * @author Rene Haberland
     * @throws java.lang.Exception
     */
    public void startServer() throws Exception
    {
        // create a new registry
        registry = LocateRegistry.createRegistry(port);

        // bind the service, in this case itself, and catch the wasty stuff
        try
        {
            registry.bind(SERVICE_NAME, this);
        }
        catch (AlreadyBoundException ex)
        {
            throw new Exception(SERVICE_NAME + " is already bound to socket " + port + ".\n");
        }
        catch (RemoteException ex)
        {
        }
    }

    /**
     * Unbind the registered service.
     * @author Rene Haberland
     * @throws java.lang.Exception
     */
    public void shutdownCommunicator()throws Exception
    {
        if(registry == null)
        {
            throw new Exception("Error: Regitry is null");
        }
        try
        {
            registry.unbind(SERVICE_NAME);
        }
        catch (NotBoundException ex)
        {
            throw new Exception("Service was not bound:" + SERVICE_NAME);
        }
        catch (RemoteException ex)
        {
        }
    }

    /* (non-Javadoc)
    * @see adventure.server.Server#getCurrentScene(int)
    */
    public Scene getCurrentScene(int gameID) throws RemoteException
    {
        try {
            return ServerApplication.getInstance().getCurrentScene(gameID);
        } catch (Exception e) {
            throw new RemoteException(e.getMessage());
        }
    }

    /* (non-Javadoc)
    * @see adventure.server.Server#sendAction(int, adventure.client.UserAction)
    */
    public Scene sendAction(int gameID, UserAction userAction) throws RemoteException
    {
        try {
            return ServerApplication.getInstance().sendAction(gameID, userAction);
        } catch (Exception e) {
            throw new RemoteException(e.getMessage());
        }

    }

    /* (non-Javadoc)
    * @see adventure.server.Server#loadGame(int, java.lang.String)
    */
    public Scene loadGame(int gameID, String saveGameName) throws RemoteException
    {
        try {
            return ServerApplication.getInstance().loadGame(gameID, saveGameName);
        } catch (Exception e) {
            throw new RemoteException(e.getMessage());
        }

    }

    /* (non-Javadoc)
    * @see adventure.server.Server#saveGame(int, java.lang.String)
    */
    public void saveGame(int gameID, String saveGameName) throws RemoteException
    {
        try {
            ServerApplication.getInstance().saveGame(gameID, saveGameName);
        } catch (Exception e) {
            throw new RemoteException(e.getMessage());
        }
    }

    /* (non-Javadoc)
    * @see adventure.server.Server#login(java.lang.String, java.lang.String)
    */
    public int login(String userName, String password) throws RemoteException
    {
        try {
            return ServerApplication.getInstance().login(userName, password);
        } catch (Exception e) {
            throw new RemoteException(e.getMessage());
        }
    }

    /* (non-Javadoc)
    * @see adventure.server.Server#quit(int)
    */
    public void quit(int gameID) throws RemoteException
    {
        try {
            ServerApplication.getInstance().quitGame(gameID);
        } catch (Exception e) {
            throw new RemoteException(e.getMessage());
        }

    }

    /* (non-Javadoc)
    * @see adventure.server.Server#logout(int)
    */
    public void logout(int gameID) throws RemoteException
    {
        try {
            ServerApplication.getInstance().logout(gameID);
        } catch (Exception e) {
            throw new RemoteException(e.getMessage());
        }

    }

    public List getRules() throws RemoteException {
        try {
            return ServerApplication.getInstance().getRules();
        } catch (Exception e) {
            throw new RemoteException(e.getMessage());
        }
    }

    public void deleteRules(RuleSet ruleSet) throws RemoteException {
        try {
            ServerApplication.getInstance().delRules(ruleSet);
        } catch (Exception e) {
            throw new RemoteException(e.getMessage());
        }
    }

    public void updateRuleState(RuleSet ruleSet) throws RemoteException {
        try {
            ServerApplication.getInstance().updateRuleState(ruleSet);
        } catch (Exception e) {
            throw new RemoteException(e.getMessage());
        }
    }

    public List getUsers() throws RemoteException {
        try {
            return ServerApplication.getInstance().getUserList();
        } catch (Exception e) {
            throw new RemoteException(e.getMessage());
        }
    }

    public List getUsers(RuleSet ruleSet) throws RemoteException {
        return ServerApplication.getInstance().getUserList(ruleSet);
    }

    public User getUserInfo(String userName) throws RemoteException {
        try {
            return ServerApplication.getInstance().getUserInfo(userName);
        } catch (Exception e) {
            throw new RemoteException(e.getMessage());
        }

    }

    public void deleteUser(User user) throws RemoteException {
        try {
            ServerApplication.getInstance().deleteUser(user);
        } catch (Exception e) {
            throw new RemoteException(e.getMessage());
        }
    }

    public int getMaxOnlineUsers() throws RemoteException {
        return ServerApplication.getInstance().getMaxOnlineUsers();
    }

    public  void setMaxOnlineUsers(int maxOnlineUsers) throws RemoteException {
        ServerApplication.getInstance().setMaxOnlineUsers(maxOnlineUsers);
    }

    public int getMaxRegUsers() throws RemoteException {
        return ServerApplication.getInstance().getMaxRegUsers();
    }

    public void setMaxRegUsers(int maxRegUsers) throws RemoteException {
        ServerApplication.getInstance().setMaxRegUsers(maxRegUsers);
    }

    public void registerUser(User user) throws RemoteException {
        try {
            ServerApplication.getInstance().registerUser(user);
        } catch (Exception e) {
            throw new RemoteException(e.getMessage());
        }
    }

    public void updateUser(User user) throws RemoteException {
        try {
            ServerApplication.getInstance().updateUser(user);
        } catch (Exception e) {
            throw new RemoteException(e.getMessage());
        }
    }

    public void deleteSaveGame(int saveGameID) throws RemoteException {
        try {
            ServerApplication.getInstance().deleteSaveGame(saveGameID);
        } catch (Exception e) {
            throw new RemoteException(e.getMessage());
        }
    }

    public void shutdownServer() throws RemoteException {
        ServerApplication.getInstance().shutdown();
    }
}

