/*
* Created on Dec 15, 2003
*/
package adventure.server;

import java.util.regex.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Hashtable;

import adventure.gameobjects.Scene;

public class ScriptInterpreter {
    private Pattern addItemPattern;
    private Pattern removeItemPattern;
    private Pattern showMessagePattern;
    private Pattern addDialogOptionPattern;
    private Pattern removeDialogOptionPattern;
    private Pattern setFlagPattern;
    private Pattern removeFlagPattern;
    private Pattern ifSetPattern;
    private Pattern ifNotSetPattern;
    private Pattern elsePattern;
    private Pattern endBlockPattern;
    private Pattern changeLocationPattern;
    private Scene scene = null;
    private Hashtable flags = null;
    private Game game;

    public ScriptInterpreter() {
        addItemPattern = Pattern.compile("addItem\\((.+),(.+)\\);");
        removeItemPattern = Pattern.compile("removeItem\\((.+)\\);");
        showMessagePattern = Pattern.compile("showMessage\\((.+)\\);");
        addDialogOptionPattern = Pattern.compile("addDialogOption\\((.+)\\);");
        removeDialogOptionPattern = Pattern.compile("removeDialogOption\\((.+)\\);");
        setFlagPattern = Pattern.compile("setFlag\\((.+)\\);");
        removeFlagPattern = Pattern.compile("removeFlag\\((.+)\\);");
        ifSetPattern = Pattern.compile("if flag set\\((.+)\\) \\{");
        ifNotSetPattern = Pattern.compile("if flag not set\\((.+)\\) \\{");
        elsePattern = Pattern.compile("\\} else \\{");
        endBlockPattern = Pattern.compile("\\}");
        changeLocationPattern = Pattern.compile("changeLocation\\((.+)\\);");
    }

    public ScriptInterpreter(Game game) {
        this();
        this.game = game;
    }


    public void interprete(String[] scripts, Scene scene, Hashtable flags) throws Exception {
        this.scene = scene;
        this.flags = flags;
        createScript(scripts);
    }

    private void createScript(String[] script) throws Exception {
        for(int i = 0; i < script.length; i++) {
            if(ifSetPattern.matcher(script[i].trim()).matches()
                    || ifNotSetPattern.matcher(script[i].trim()).matches()) {
                i = handleIfThenElse(script, i);
                continue;
            }

            if(isValid(script[i].trim())) {
                executeScript(script[i].trim());
                continue;
            }
        }
    }

    private void executeScript(String script) {
        if(addItemPattern.matcher(script).matches()) {
            handleAddItem(script, scene);
        }
        if(removeItemPattern.matcher(script).matches()) {
            handleRemoveItem(script, scene);
        }
        if(showMessagePattern.matcher(script).matches()) {
            handleShowMessage(script, scene);
        }
        if(addDialogOptionPattern.matcher(script).matches()) {
            handleAddDialogOption(script, scene);
        }
        if(removeDialogOptionPattern.matcher(script).matches()) {
            handleRemoveDialogOption(script, scene);
        }
        if(setFlagPattern.matcher(script).matches()) {
            handleSetFlag(script, flags);
        }
        if(removeFlagPattern.matcher(script).matches()) {
            handleRemoveFlag(script, flags);
        }
        if(changeLocationPattern.matcher(script).matches()) {
            handleChangeLocation(script);
        }
    }




    private boolean isValid(String script) {
        boolean result = false;
        result |= addItemPattern.matcher(script).matches();
        result |= removeItemPattern.matcher(script).matches();
        result |= addDialogOptionPattern.matcher(script).matches();
        result |= removeDialogOptionPattern.matcher(script).matches();
        result |= showMessagePattern.matcher(script).matches();
        result |= setFlagPattern.matcher(script).matches();
        result |= removeFlagPattern.matcher(script).matches();
        result |= changeLocationPattern.matcher(script).matches();
        return result;
    }

    private int handleElse(String[] script, int i) throws Exception {
        i++;
        for(; i < script.length; i++) {
            if(ifSetPattern.matcher(script[i].trim()).matches()
                    || ifNotSetPattern.matcher(script[i].trim()).matches()) {
                i = handleIfThenElse(script, i);
                continue;
            }

            if(endBlockPattern.matcher(script[i].trim()).matches()) {
                return i;
            }

            if(isValid(script[i].trim())) {
                executeScript(script[i].trim());
                continue;
            }
        }
        throw new Exception("Script ung�ltig: If-Block endet nicht");
    }

    private int handleIfThenElse(String[] script, int i) throws Exception {
        Matcher m = null;
        int ifs = 0;
        Matcher setMatcher = ifSetPattern.matcher(script[i].trim());
        Matcher notSetMatcher = ifNotSetPattern.matcher(script[i].trim());
        i++;
        if(setMatcher.find()) {
            m = setMatcher;
        }
        if(notSetMatcher.find()) {
            m = notSetMatcher;
        }
        if(m == null) throw new Exception("Script ist ung�ltig!");

        String flag = m.group(1);

        if( ((m == setMatcher) && flags.get(flag) != null) ||
                ((m == notSetMatcher && flags.get(flag) == null))) {
            for(; i < script.length; i++) {
                if(ifSetPattern.matcher(script[i].trim()).matches()
                        || ifNotSetPattern.matcher(script[i].trim()).matches()) {
                    i = handleIfThenElse(script, i) + 1;
                }

                if(endBlockPattern.matcher(script[i].trim()).matches()) {
                    break;
                }

                if(elsePattern.matcher(script[i].trim()).matches()) {
                    i = searchBlockEnd(script, i);
                    break;
                }

                if(isValid(script[i].trim())) {
                    executeScript(script[i].trim());
                }

            }
        } else {
            i = searchBlockEnd(script, i);
        }
        return i;
    }

    private int searchBlockEnd(String[] script, int i) throws Exception {
        int ifs = 0;
        i++;
        for(; i < script.length; i++) {
            if(ifSetPattern.matcher(script[i].trim()).matches()
                    || ifNotSetPattern.matcher(script[i].trim()).matches()) {
                ifs++;
                continue;
            }

            if(elsePattern.matcher(script[i].trim()).matches()) {
                if(ifs != 0) {
                    ifs--;
                    continue;
                } else {
                    i = handleElse(script, i);
                    break;
                }
            }

            if(endBlockPattern.matcher(script[i].trim()).matches()) {
                if(ifs != 0) {
                    ifs--;
                    continue;
                } else {
                    break;
                }
            }

        }
        return i;
    }

    public void handleAddItem(String script, Scene scene) {
        Matcher m = addItemPattern.matcher(script);
        if(m.find()) {
            System.out.println("************* addItem(" + m.group(1) + ", " + m.group(2) + ");");
            scene.getLocation().addItem(m.group(1), m.group(2));
        }
    }

    public void handleRemoveItem(String script, Scene scene) {
        Matcher m = removeItemPattern.matcher(script);
        if(m.find()) {
            System.out.println("************* removeItem(" + m.group(1) + ");");
            scene.getLocation().removeItem(m.group(1));
        }
    }

    public void handleShowMessage(String script, Scene scene) {
        Matcher m = showMessagePattern.matcher(script);
        if(m.find()) {
            System.out.println("************* showMessage(" + m.group(1) + ");");
            scene.showMessage(m.group(1));
        }
    }

    private void handleChangeLocation(String script) {
        Matcher m = changeLocationPattern.matcher(script);
        if(m.find()) {
            System.out.println("************* changeLocation(" + m.group(1) + ");");
            game.changeLocation(m.group(1));
        }
    }

    public void handleSetFlag(String script, Hashtable flags) {
        Matcher m = setFlagPattern.matcher(script);
        if(m.find()) {
            System.out.println("************* setFlag(" + m.group(1) + ");");
            try {
                flags.put(m.group(1), new Boolean(true));
            } catch (Exception e) {
                // ignored; flat was already set elsewhere
            }

        }
    }

    public void handleRemoveFlag(String script, Hashtable flags) {
        Matcher m = removeFlagPattern.matcher(script);
        if(m.find()) {
            System.out.println("************* removeFlag(" + m.group(1) + ");");
            flags.remove(m.group(1));
        }
    }

    public void handleAddDialogOption(String script, Scene scene) {
        Matcher m = addDialogOptionPattern.matcher(script);
        if(m.find()) {
            System.out.println("************* addDialogOption(" + m.group(1) + ");");
            scene.addDialogOption(game.getDialogOption(m.group(1)));
        }
    }

    public void handleRemoveDialogOption(String script, Scene scene) {
        Matcher m = removeDialogOptionPattern.matcher(script);
        if(m.find()) {
            System.out.println("************* removeDialogOption(" + m.group(1) + ");");
            scene.removeDialogOption(game.getDialogOption(m.group(1)));
        }
    }
}

