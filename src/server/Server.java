/*
* Created on Dec 15, 2003
*/
package adventure.server;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import adventure.gameobjects.UserAction;
import adventure.gameobjects.Scene;
import adventure.gameobjects.User;
import adventure.gameobjects.RuleSet;

public interface Server extends Remote {
    /**
     * returns the scene for a given game
     * @param gameID
     * @return
     * @throws RemoteException
     */
    public Scene getCurrentScene(int gameID) throws RemoteException;

    /**
     * sends an action of the user to the server. This is evaluated there and the clients receives an updated scene for display.
     * @param gameID
     * @param userAction
     * @return
     * @throws RemoteException
     */
    public Scene sendAction(int gameID, UserAction userAction) throws RemoteException;

    /**
     * loads the game specified by name. Returns the current scene for the loaded game.
     * @param gameID
     * @param saveGameName
     * @return
     * @throws RemoteException
     */
    public Scene loadGame(int gameID, String saveGameName) throws RemoteException;

    /**
     * stores the current game with name provided.
     * @param gameID
     * @param saveGameName
     * @return
     * @throws RemoteException
     */
    public void saveGame(int gameID, String saveGameName) throws RemoteException;

    /**
     * tries to log in user with a specified login credentials. Returns game ID.
     * @param userName
     * @param password
     * @return GameId
     * @throws RemoteException
     */
    public int login(String userName, String password) throws RemoteException;

    /**
     * quits specified game. The client does not mandatory have to quit.
     * A new game may be started or an existing game be loaded.
     * @param gameID
     * @throws RemoteException
     */
    public void quit(int gameID) throws RemoteException;

    /**
     * logs out a user. A new game may not be loaded afterwards. The client should logout himself after calling this routine.
     * @param gameID
     * @throws RemoteException
     */
    public void 	logout(int gameID) throws RemoteException;

    public java.util.List getRules() throws RemoteException;
    public void deleteRules(RuleSet ruleSet) throws RemoteException;
    public void updateRuleState(RuleSet ruleSet) throws RemoteException;
    public java.util.List getUsers() throws RemoteException;
    public List getUsers(RuleSet ruleSet) throws RemoteException;
    public User getUserInfo(String userName) throws RemoteException;
    public void deleteUser(User user) throws RemoteException;
    public int getMaxOnlineUsers() throws RemoteException;
    public  void setMaxOnlineUsers(int maxOnlineUsers) throws RemoteException;
    public int getMaxRegUsers() throws RemoteException;
    public void setMaxRegUsers(int maxRegUsers) throws RemoteException;
    public void registerUser(User user) throws RemoteException;
    public void updateUser(User user) throws RemoteException;
    public void shutdownServer() throws RemoteException;
}

