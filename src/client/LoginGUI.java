package adventure.client;

import java.awt.Container;

import java.awt.*;

import javax.swing.*;


/**
 * Folgendes ausw�hlen, um die Schablone f�r den erstellten Typenkommentar zu �ndern:
 * Fenster&gt;Benutzervorgaben&gt;Java&gt;Codegenerierung&gt;Code und Kommentare
 */
public class LoginGUI extends JFrame {

   
    
    private JPanel panel;
    
    private JLabel loginLabel,passwordLabel,imageLabel;
    
    private JTextField loginTextField;
    private JButton loginButton,registerButton;
    private JPasswordField passwordField;
    private Image bannerImage;
    
 public void init(Container cp)
 {
 	    cp.setLayout(null);
        cp.setBackground(ClientApplication.COMP_BACKGROUND_COLOR);
        
        panel = new JPanel();
 	    panel.setLayout(null);
        panel.setBackground(ClientApplication.BACKGROUND_COLOR);
        panel.setForeground(ClientApplication.FOREGROUND_COLOR);
 	    panel.setBounds(0,0,800,600);
       
        
        imageLabel = new JLabel(new ImageIcon("adventure/client/images/banner.gif"));
        imageLabel.setBounds(0,0,800,120);
        panel.add(imageLabel);
        
        loginLabel = new JLabel("Login:");
        loginLabel.setBounds(300, 250, 100, 20);
        loginLabel.setForeground(ClientApplication.FOREGROUND_COLOR);
        panel.add(loginLabel);
        
        loginTextField = new JTextField();
        loginTextField.setBackground(ClientApplication.COMP_BACKGROUND_COLOR);
        loginTextField.setForeground(ClientApplication.FOREGROUND_COLOR);
        loginTextField.setBounds(420, 250, 150, 20);
        panel.add(loginTextField);
        
        passwordLabel = new JLabel("Password:");
        passwordLabel.setBounds(300, 300, 100, 20);
        passwordLabel.setForeground(ClientApplication.FOREGROUND_COLOR);
        panel.add(passwordLabel);
 	
        passwordField = new JPasswordField();
        passwordField.setBounds(420, 300, 150, 20);
        passwordField.setForeground(ClientApplication.FOREGROUND_COLOR);
        passwordField.setBackground(ClientApplication.COMP_BACKGROUND_COLOR);
        passwordField.setEchoChar('*');
        panel.add(passwordField);
        
 	    loginButton = new JButton("Login");
 	    loginButton.setBounds(300,400,ClientApplication.BUTTON_WIDTH ,ClientApplication.BUTTON_HEIGHT);
        loginButton.setBorder(ClientApplication.BUTTON_BORDER);
        loginButton.setBackground(ClientApplication.BUTTON_COLOR);
        loginButton.setForeground(ClientApplication.FOREGROUND_COLOR);
        panel.add(loginButton);
        loginButton.addActionListener(new LoginActionListenerImpl(this,loginTextField,passwordField));
        
        registerButton = new JButton("Registieren");
 	    registerButton.setBounds(420,400,ClientApplication.BUTTON_WIDTH ,ClientApplication.BUTTON_HEIGHT);
	    registerButton.setBorder(ClientApplication.BUTTON_BORDER);
        registerButton.setBackground(ClientApplication.BUTTON_COLOR);
        registerButton.setForeground(ClientApplication.FOREGROUND_COLOR);
        panel.add(registerButton);
        registerButton.addActionListener(new LoginActionListenerImpl(this));
        
	 	
	    cp.add(panel);
	    bannerImage = Toolkit.getDefaultToolkit().createImage("banner.jpg");

 }
 public void paint(Graphics g)
 {
 	if(bannerImage != null)
 	{
 		g.drawImage(bannerImage,0,0,800,100,this);
 	}
 }

/* (Kein Javadoc)
 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
 */

}
