package adventure.client;


import java.awt.Container;
import javax.swing.*;

/**
 * Folgendes ausw�hlen, um die Schablone f�r den erstellten Typenkommentar zu �ndern:
 * Fenster&gt;Benutzervorgaben&gt;Java&gt;Codegenerierung&gt;Code und Kommentare
 */
public class RegisterGUI extends JFrame
{
       
        
        private JPanel panel;
        private JLabel vornameLabel,nachnameLabel,emailLabel,nicknameLabel,passwordLabel,newPasswordLabel;
        private JTextField  vornameField,nachnameField,emailField,nicknameField;
	    private JButton submitButton,cancelButton;
        private JPasswordField passwordField,newPasswordField;
        
        public void init(Container cp)
	    {
            cp.setLayout(null);
            cp.setBackground(ClientApplication.COMP_BACKGROUND_COLOR);
            
            panel = new JPanel();
 	        panel.setLayout(null);
            panel.setBackground(ClientApplication.BACKGROUND_COLOR);
            panel.setForeground(ClientApplication.FOREGROUND_COLOR);
 	        panel.setBounds(0,0,800,600);
            
            vornameLabel = new JLabel("Vorname:");
            vornameLabel.setForeground(ClientApplication.FOREGROUND_COLOR);
            vornameLabel.setBounds(250, 170, 100, 20);
            panel.add(vornameLabel);
        
            vornameField = new JTextField();
            vornameField.setBounds(400, 170, 150, 20);
            vornameField.setForeground(ClientApplication.FOREGROUND_COLOR);
            vornameField.setBackground(ClientApplication.COMP_BACKGROUND_COLOR);
            panel.add(vornameField);
        
            nachnameLabel = new JLabel("Nachname:");
            nachnameLabel.setForeground(ClientApplication.FOREGROUND_COLOR);
            nachnameLabel.setBounds(250, 210, 100, 20);
            panel.add(nachnameLabel);
        
            nachnameField = new JTextField();
            nachnameField.setBounds(400, 210, 150, 20);
            nachnameField.setForeground(ClientApplication.FOREGROUND_COLOR);
            nachnameField.setBackground(ClientApplication.COMP_BACKGROUND_COLOR);
            panel.add(nachnameField);
            
            emailLabel = new JLabel("E-mail:");
            emailLabel.setForeground(ClientApplication.FOREGROUND_COLOR);
            emailLabel.setBounds(250, 250, 100, 20);
            panel.add(emailLabel);
        
            emailField = new JTextField(20);
            emailField.setBounds(400, 250, 150, 20);
            emailField.setForeground(ClientApplication.FOREGROUND_COLOR);
            emailField.setBackground(ClientApplication.COMP_BACKGROUND_COLOR);
            panel.add(emailField);
            
            nicknameLabel = new JLabel("Nickname:");
            nicknameLabel.setForeground(ClientApplication.FOREGROUND_COLOR);
            nicknameLabel.setBounds(250, 290, 100, 20);
            panel.add(nicknameLabel);
        
            nicknameField = new JTextField();
            nicknameField.setBounds(400, 290, 150, 20);
            nicknameField.setForeground(ClientApplication.FOREGROUND_COLOR);
            nicknameField.setBackground(ClientApplication.COMP_BACKGROUND_COLOR);
            panel.add(nicknameField);
            
            passwordLabel = new JLabel("Password:");
            passwordLabel.setForeground(ClientApplication.FOREGROUND_COLOR);
            passwordLabel.setBounds(250, 330, 100, 20);
            panel.add(passwordLabel);
        
            passwordField = new JPasswordField();
            passwordField.setBounds(400, 330, 150, 20);
            passwordField.setForeground(ClientApplication.FOREGROUND_COLOR);
            passwordField.setBackground(ClientApplication.COMP_BACKGROUND_COLOR);
            passwordField.setEchoChar('*');
            panel.add(passwordField);
          
            newPasswordLabel = new JLabel("Bestaetigung:");
            newPasswordLabel.setForeground(ClientApplication.FOREGROUND_COLOR);
            newPasswordLabel.setBounds(250, 370, 100, 20);
            panel.add(newPasswordLabel);
        
            newPasswordField = new JPasswordField();
            newPasswordField.setBounds(400, 370, 150, 20);
            newPasswordField.setForeground(ClientApplication.FOREGROUND_COLOR);
            newPasswordField.setBackground(ClientApplication.COMP_BACKGROUND_COLOR);
            newPasswordField.setEchoChar('*');
            panel.add(newPasswordField);
            
            submitButton = new JButton("Senden");
 	        submitButton.setBounds(250,410,ClientApplication.BUTTON_WIDTH ,ClientApplication.BUTTON_HEIGHT); 
            submitButton.setBorder(ClientApplication.BUTTON_BORDER);
            submitButton.setBackground(ClientApplication.BUTTON_COLOR);
            submitButton.setForeground(ClientApplication.FOREGROUND_COLOR);
            panel.add(submitButton);
            submitButton.addActionListener(new RegisterActionListenerImpl(this,vornameField,nachnameField,emailField,nicknameField,passwordField,newPasswordField));
        
            /*cancelButton = new JButton("Abbrechen");
 	        cancelButton.setBounds(400,410,ClientApplication.BUTTON_WIDTH ,ClientApplication.BUTTON_HEIGHT);
            cancelButton.setBorder(ClientApplication.BUTTON_BORDER);
            cancelButton.setBackground(ClientApplication.BUTTON_COLOR);
            cancelButton.setForeground(ClientApplication.FOREGROUND_COLOR);
            panel.add(cancelButton);
            cancelButton.addActionListener(new RegisterActionListenerImpl(this));*/
		
            cp.add(panel);
           
	}
}

