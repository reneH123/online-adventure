package adventure.client;
import javax.swing.table.AbstractTableModel;


/**
 * Folgendes ausw�hlen, um die Schablone f�r den erstellten Typenkommentar zu �ndern:
 * Fenster&gt;Benutzervorgaben&gt;Java&gt;Codegenerierung&gt;Code und Kommentare
 */
public class AdminUsersTableModel extends AbstractTableModel 
{
	private int i = 0;
	private int length=0;
	
	private String[] columnNames = {"Nutzer",
									"Status"
									};
	
	private Object[][] data = {
			{"user1","online"},
			{"user2","offline"},
			{"user3","offline"},
			{"user4","online"},
			{"user5","offline"},
			{"user6","online"},
		    {"user7","online"},
		    {"user8","offline"},
		    {"user9","offline"},
		    {"user10","offline"},
		    {"user11","offline"},
		    {"user12","offline"},
		    {"user13","online"},
		    {"user14","offline"},
		    {"user15","offline"}
	};
	

	public int getColumnCount() {
		return columnNames.length;
	}

	public int getRowCount() 
	{
		return data.length;
	}
	public int getRowCount(String search)
	{
		while(i<data.length)
		{
				if(search.equals(this.getValueAt(i,1)))
					length ++;
			
			i++;
		}
		return length;
	}

	public String getColumnName(int col) {
		return columnNames[col];
	}

	public Object getValueAt(int row, int col) {
		return data[row][col];
	}

	/*
	 * JTable uses this method to determine the default renderer/
	 * editor for each cell.  If we didn't implement this method,
	 * then the last column would contain text ("true"/"false"),
	 * rather than a check box.
	 */
	public Class getColumnClass(int c) {
		return getValueAt(0, c).getClass();
	}
}
