/*
 * LoginActionListenerImp.java
 *
 * Created on 2004.01.03
 */

package adventure.client;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Component;


public class LoginActionListenerImpl extends JFrame implements ActionListener{
    
    private Component component;
    
    private JTextField loginTextField;
    private JPasswordField passwordField;
    private RegisterGUI register;
    
    /** Creates a new instance of LoginActionListenerImp */
    public LoginActionListenerImpl(Component component,JTextField loginTextField,JPasswordField passwordField) 
    {
        this.component = component;
        this.loginTextField = loginTextField;
        this.passwordField = passwordField;
		
        
    }
    public LoginActionListenerImpl(Component component)
    {
        this.component = component;
    }
    
    public void actionPerformed(ActionEvent e) 
    {
           String cmd = e.getActionCommand();
           if(cmd.equals("Login"))
           {
             //Aufrufen ClientCommunicator und schicken Server Datei
            
               //bei Erfolg,erzeug instanz der UserGUI
			  this.setSize(800,600);
			  this.setResizable(false);
			  this.setLocation(50,50);
			  this.setVisible(true); 
              UserGUI newUser = new UserGUI();
              newUser.init(getContentPane());
              
           }
           if(cmd.equals("Registieren"))
           {
            //erzeug instanz der RegisterGUI
			  this.setSize(800,600);
			  this.setResizable(false);
			  this.setLocation(50,50);
			  this.setVisible(true);
			  
			  
              register = new RegisterGUI();
			  register.init(getContentPane());
           }

    }
}

