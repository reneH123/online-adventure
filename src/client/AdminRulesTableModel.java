package adventure.client;
import javax.swing.table.AbstractTableModel;


/**
 * Folgendes ausw�hlen, um die Schablone f�r den erstellten Typenkommentar zu �ndern:
 * Fenster&gt;Benutzervorgaben&gt;Java&gt;Codegenerierung&gt;Code und Kommentare
 */
public class AdminRulesTableModel extends AbstractTableModel {
	
	private int i = 0;
	private int length = 0;
	
	private String[] columnNames = {"Regelwerk",
									"Status"
									};
	
	private Object[][] data = {
		{"Regelwerk 1","aktiv"},
		{"Regelwerk 2","gesperrt"},
		{"Regelwerk 3","aktiv"},
		
		}; 
	

	public int getColumnCount() {
		return columnNames.length;
	}

	public int getRowCount() 
	{
		return data.length;
	}
	/*
	 * Methode zum Z�hlen von Eintr&auml;gen mit bestimmten Kriterien
	 * @param String Suchkriterium
	 * @return int Anzahl der gefundenen Eintr&auml;ge
	 */
	public int getRowCount(String search)
	{
		while(i<data.length)
		{
				if(search.equals(this.getValueAt(i,1)))
					length ++;
			
			i++;
		}
		return length;
	}

	public String getColumnName(int col) {
		return columnNames[col];
	}

	public Object getValueAt(int row, int col) {
		return data[row][col];
	}

	/*
	 * JTable uses this method to determine the default renderer/
	 * editor for each cell.  If we didn't implement this method,
	 * then the last column would contain text ("true"/"false"),
	 * rather than a check box.
	 */
	public Class getColumnClass(int c) {
		return getValueAt(0, c).getClass();
	}

}
