package adventure.client;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.JTable;


/**
 * Folgendes ausw�hlen, um die Schablone f�r den erstellten Typenkommentar zu �ndern:
 * Fenster&gt;Benutzervorgaben&gt;Java&gt;Codegenerierung&gt;Code und Kommentare
 */
public class AdminRulesActionListenerImpl implements ActionListener 
{
	private Container container;
	private Component component;
	private JTable table;

	private int message = 0;
	
	/*
	 * Konstruktor
	 * @param Component Elter-Component
	 */
	public AdminRulesActionListenerImpl(Component component)
	{
		this.component = component;
	}
	
	/*
	 * Kontruktor
	 * @param Container ContentComponent
	 * @param Component Elter-Component
	 * @param JTable Tabelle mit Regelwerken
	 */

	public AdminRulesActionListenerImpl(Container cp, Component component, JTable table)
	{
		this.container = cp;
		this.component = component;
		this.table = table;
	}
	/* (Kein Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent event) 
	{
		String cmd = event.getActionCommand();
		System.out.println(cmd);
		if(!cmd.equals("neu"))
		{
			try {
				if(!cmd.equals("spielen"))
				{
					message = JOptionPane.showConfirmDialog(component,
															"Wollen Sie das "+table.getValueAt(table.getSelectedRow(),0)+" wirklich "+cmd+"?",
															"Sicherheitsdialog",
															JOptionPane.OK_CANCEL_OPTION,
															JOptionPane.WARNING_MESSAGE
															);
				} 
				else
				{
					System.out.println(table.getValueAt(table.getSelectedRow(),0)+" starten");
				}
			}
			catch (Exception e)
			{
				JOptionPane.showConfirmDialog(component,
												"Keine Daten ausgew�hlt!",
												"Fehler",
												JOptionPane.CLOSED_OPTION,
												JOptionPane.ERROR_MESSAGE);
			}
			
		} else{
			System.out.println("neues Regelwerk");	
		};
		
		if (message==JOptionPane.YES_OPTION && cmd.equals("l�schen"))
				deleteRule(table);
		else if(message==JOptionPane.YES_OPTION && cmd.equals("sperren"))
				lockRule(table);
			 else if(message==JOptionPane.YES_OPTION && cmd.equals("aktivieren"))
				 activateRule(table);
		else ;	
		
	}
	/*
	 * Methode zum l�schen von Regelwerken
	 * @param JTable Tabelle mit Regelwerken
	 * @return boolean Erfolg/Mi�erfolg
	 */
	private boolean deleteRule(JTable table)
	{
		try 
		{
			System.out.println(table.getValueAt(table.getSelectedRow(),0)+" l�schen...");
			return true;
		}
		catch (Exception e) 
		{
			return false;
		}
	}
	
	/*
	 * Methode zum sperren von Regelwerken
	 * @param JTable Tabelle mit Regelwerken
	 * @return boolean Erfolg/Mi�erfolg
	 */
	private boolean lockRule(JTable table)
	{
		try 
		{
			System.out.println(table.getValueAt(table.getSelectedRow(),0)+" sperren...");
			return true;
		}
		catch (Exception e)
		{
			return false;
		}
	}
	
	/*
	 * Methode zum aktivieren von Regelwerken
	 * @param JTable Tabell mit Regelwerken
	 * @return boolean Erfolg/Mi�erfolg
	 */
	private boolean activateRule(JTable table)
	{
		try 
		{
			System.out.println(table.getValueAt(table.getSelectedRow(),0)+" aktivieren...");
			
			return true;
		}
		catch (Exception e)
		{
			return false;
		}
	}
}
