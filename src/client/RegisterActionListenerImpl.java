/*
 * RegisterActionListenerImpl.java
 *
 * Created on 2004.01.03, 11:12
 */

package adventure.client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Component;
import javax.swing.JTextField;
import javax.swing.JPasswordField;

import javax.swing.JOptionPane;

public class RegisterActionListenerImpl implements ActionListener
{
    
    private Component component;
    private JTextField vornameField,nachnameField,emailField,nicknameField;
    private JPasswordField passwordField,newPasswordField;
    
    /** Creates a new instance of RegisterActionListenerImpl */
    public RegisterActionListenerImpl(Component component) 
    {
        this.component = component;
    }
    public RegisterActionListenerImpl(Component component,JTextField vornameField,JTextField nachnameField,JTextField emailField,JTextField nicknameField,JPasswordField passwordField,JPasswordField newPasswordField) 
    {
        this.component = component;
        this.vornameField = vornameField;
        this.nachnameField = nachnameField;
        this.emailField = emailField;
        this.nicknameField = nicknameField;
        this.passwordField = passwordField;
        this.newPasswordField = newPasswordField;
    }
    //check,if two passwords are same
    public boolean isPasswordNull()
    {
    	if(passwordField.getText().equals(""))
    	   return true;
    	else 
    	   return false;
    }
    public boolean isSamePassword()
    {
    	if(passwordField.getText().equals(newPasswordField.getText()))
    	   return true;
        else 
           return false;
    }
    //check,if the Email address is right
    public boolean isEmailNull()
    {
    	if(emailField.getText().equals(""))
    	   return true;
    	else
    	   return false;
    }
    public boolean  isRightEmail()
    {
		String regularExpression = "^[_a-zA-Z0-9-](.?[_a-zA-Z0-9-])*@([a-zA-Z0-9-]{2,}.)?[a-zA-Z0-9-]{3,}(.[a-zA-Z]{2,4}){1,2}$";
    	if(emailField.getText().matches(regularExpression))
    	    return true;
    	else
    	   return false;
		

    }
    //check,if all field is null
    public boolean isVornameNull()
    {
    	if(vornameField.getText().equals(""))
    	   return true;
    	else
    	   return false;   
    	
    }
    
    public boolean isNachnameNull()
    {
    	
    	if(nachnameField.getText().equals(""))
    	   return true;
    	else
    	   return false;  
    
    }
    public boolean isNicknameNull()
    {
    	if(nicknameField.getText().equals(""))
    	   return true;
    	else
    	   return false;
    }
    //check,if nickname exists already
    public boolean isExistNickname()
    {
    	return false;
    }
    
    public void actionPerformed(ActionEvent e) 
    {
        String cmd = e.getActionCommand();

        
        
            if(cmd.equals("Senden"))
            {
             //Aufrufen ClientCommunicator und schicken Server Datei
                
                if(!isVornameNull())
                {  
					if(!isNachnameNull())
					{
					  if(!isEmailNull())	
					   {	
					   	if(isRightEmail())
					    {
							if(!isNicknameNull())
							{
							   if(!isPasswordNull())
							    {
								   if(isSamePassword())
								   {
									
									
									//call ClientCommunicator und schicken Server Datei
									//if nickname exists already,return false
									
									}
								    else
								    {
									     JOptionPane.showMessageDialog(null,
							             "Sie haben zwei verschieden Passwoerter angegeben!",
								         "Fehlermeldung",
									      JOptionPane.ERROR_MESSAGE);
								     }
							     }
							    else
							    {
							       JOptionPane.showMessageDialog(null,
								   "Bitte geben Sie Password an",
								   "Fehlermeldung",
								   JOptionPane.ERROR_MESSAGE);
							    }
							}
							else
							{
							  JOptionPane.showMessageDialog(null,
							  "Bitte geben Sie Nickname an",
							  "Fehlermeldung",
							   JOptionPane.ERROR_MESSAGE);
							}
							
						}
						else
							{
								JOptionPane.showMessageDialog(null,
								"Bitte geben Sie richtige Email-Addresse an",
								"Fehlermeldung",
								JOptionPane.ERROR_MESSAGE);
							}
					     }
					     else
					     {
							JOptionPane.showMessageDialog(null,
							"Bitte geben Sie Email an",
							"Fehlermeldung",
							JOptionPane.ERROR_MESSAGE);
					     }
					}
					else
					{
					    JOptionPane.showMessageDialog(null,
						"Bitte geben Sie Nachname an",
						"Fehlermeldung",
						JOptionPane.ERROR_MESSAGE);
					}
                }
                else
                {
                	    JOptionPane.showMessageDialog(null,
                        "Bitte geben Sie Vorname an",
                        "Fehlermeldung",
                        JOptionPane.ERROR_MESSAGE);
                }
              }
      }
}
