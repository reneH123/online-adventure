package adventure.client;
import javax.swing.table.AbstractTableModel;
/**
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class LoadGameTableModel extends AbstractTableModel{
	

	private String[] columnNames = {"Speicher"};
	private String[] data = {"Speicher1","Speicher2","Speicher3"};
	 
	public int getColumnCount() {

		return columnNames.length;
	}

	public int getRowCount() {
	
		return data.length;
	}
	
	public String getColumnName(int col) {
		return columnNames[col];
	
	}
	public Object getValueAt(int row,int col) {
		
		return data[row];
	}
	/*
		 * JTable uses this method to determine the default renderer/
		 * editor for each cell.  If we didn't implement this method,
		 * then the last column would contain text ("true"/"false"),
		 * rather than a check box.
		 */
	public Class getColumnClass(int c) {
			return getValueAt(0,c).getClass();
	}
}
