package adventure.client;
import java.awt.Color;
import java.awt.Container;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

/**
 * Folgendes ausw�hlen, um die Schablone f�r den erstellten Typenkommentar zu �ndern:
 * Fenster&gt;Benutzervorgaben&gt;Java&gt;Codegenerierung&gt;Code und Kommentare
 */
public class AdminGUI extends JFrame
{	
	//Reiter
	private JTabbedPane tp;
	
	//Panel f�r verschieden Verwaltungsfunktionen
	private JPanel userDataPanel;
	private JPanel rulesPanel;
	private JPanel usersPanel;
	
	//Label entsprechend der Panel
	private JLabel rulesLabel, rulesNewLabel, existRulesLabel;
	private JLabel usersLabel, usersMaxOnlineLabel, usersMaxRegLabel;
	private JLabel usersCurrentOnlineLabel, usersCurrentRegLabel;

	//TextFields entsprechend der Panel
	private JTextField usersMaxOnlineTextField, usersMaxRegTextField;
	
	//Button entwprechend der Panel
	private JButton rulesNewButton, rulesLockButton, rulesDeleteButton;
	private JButton rulesActivateButton, rulesStartButton;
	private JButton usersDeleteButton, usersInfoButton, usersMaxSaveButton;

	//TableModel f�r Nutzer- + Regelwerkverwaltung
	private AdminRulesTableModel rulesTableModel;
	private AdminUsersTableModel usersTableModel;
	
	//Tabellen f�r Nutzer- & Regelwerkverwaltung
	private JTable rulesTable;
	private JTable usersTable;
	
	//zum Sortieren der Nutzertabelle
	private TableSorter sorter = new TableSorter();
	
	//ScrollLeisten f�r Tabellen
	private JScrollPane rulesTableScrollPane;
	private JScrollPane usersTableScrollPane;
	
/*
 * Konstruktor
 * @param Container ContentPane
 */
	public void init(Container cp)
	{			
		//keine LayoutManager
		cp.setLayout(null);
		//Hintergrundfarbe w�hlen
		cp.setBackground(ClientApplication.COMP_BACKGROUND_COLOR);
		//create JTabbedPane
		tp = new JTabbedPane();
		//Ausrichten + Gr��e setzen
		tp.setBounds(0,0,800,600);
		//Hintergrundfarbe setzen
		tp.setBackground(ClientApplication.BACKGROUND_COLOR);
		//Fordergrundfarbe (Schrift)
		tp.setForeground(ClientApplication.FOREGROUND_COLOR);
		String rules = new String(" Regelwerkverwaltung ");
		//(Font)rules.
		//add panels to TabbedPane
		tp.addTab(rules,createRulesManagePanel(cp));
		tp.addTab("  Nutzerverwaltung  ",createUsersManagePanel(cp));
		tp.addTab("   Nutzerprofil   ",createUserDataPanel(cp));
		//add TabbedPane to Container					
		cp.add(tp);
	}
	
	/*
	 * Methode zum Aufbau der Oberfl�che "Nutzerprofil"
	 * @param Container ContentPane
	 * @return JPanel mit Oberfl�che f�r Nutzerprofil
	 */
	private JPanel createUserDataPanel(Container cp)
	{
		userDataPanel = new JPanel(null);
		userDataPanel.setBounds(0,0,800,30);
		userDataPanel.setBackground(ClientApplication.BACKGROUND_COLOR);
		return userDataPanel;		
	}//Ende createUserDataPanel()
	
	/*
	 * Methode zum Aufbau der Oberfl�che "Regelwerkverwaltung"
	 * @param Container ContentPane
	 * @return JPanel mit Oberfl�che f�r Regelwerkverwaltung
	 */
	private JPanel createRulesManagePanel(Container cp)
	{
		//create panel
		rulesPanel = new JPanel(null);
		//set bounds
		//rulesPanel.setBounds(100,0,800,200);
		//set background-color
		rulesPanel.setBackground(ClientApplication.BACKGROUND_COLOR);
		//create highlight-label	
		rulesLabel = new JLabel("Regelwerke verwalten");
		rulesLabel.setBounds(10,20,770,20);
		//create border (underline) for highlight-label
		rulesLabel.setBorder(BorderFactory.createMatteBorder(0,0,1,0,ClientApplication.FOREGROUND_COLOR));
		//set new fontsize/ fontstyle for label
		rulesLabel.setFont(new Font("Arial",Font.BOLD,14));
		//set new fontcolor
		rulesLabel.setForeground(ClientApplication.FOREGROUND_COLOR);

		rulesNewLabel = new JLabel("Regelwerke zuf�gen");
		rulesNewLabel.setBounds(10,250,770,20);
		//create border (underline) for highlight-label
		rulesNewLabel.setBorder(BorderFactory.createMatteBorder(0,0,1,0,ClientApplication.FOREGROUND_COLOR));
		//set new fontsize/ fontstyle for label
		rulesNewLabel.setFont(new Font("Arial",Font.BOLD,14));
		//set new fontcolor
		rulesNewLabel.setForeground(ClientApplication.FOREGROUND_COLOR);

		//create tableModel	
		rulesTableModel = new AdminRulesTableModel();
		//create tabel an add tableModel
		rulesTable = new JTable(rulesTableModel);
		
		rulesTable = new JTable(rulesTableModel);
		//only one row selectable
		rulesTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	//	rulesTable.setBackground(ClientApplication.COMP_BACKGROUND_COLOR);
		//design TableHead
		rulesTable.getTableHeader().setFont(new Font("Arial",Font.BOLD,10));
		rulesTable.getTableHeader().setBackground(ClientApplication.TABLE_COLOR);
		rulesTable.getTableHeader().setForeground(ClientApplication.FOREGROUND_COLOR);
		rulesTable.getTableHeader().setBorder(ClientApplication.TABLE_BORDER);
				
		//Create the scroll pane and add the table to it.
		rulesTableScrollPane = new JScrollPane(rulesTable,
												JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
												JScrollPane.HORIZONTAL_SCROLLBAR_NEVER
												);
		//Positionierung + Gr��e der Tabele inklusive ScrollBars
		rulesTableScrollPane.setBounds(345,60,300,150);

		//design ScrollPane
		rulesTableScrollPane.setBorder(BorderFactory.createLineBorder(Color.BLACK,0));
	//	rulesTableScrollPane.setBackground(ClientApplication.COMP_BACKGROUND_COLOR);
		
		//create label for current active rules
		existRulesLabel = new JLabel("Zur Zeit existieren "+rulesTable.getRowCount()+" Regelwerke");
		existRulesLabel.setBounds(20,50,200,30);
		existRulesLabel.setForeground(ClientApplication.FOREGROUND_COLOR);

				
		//create buttons 
		rulesNewButton = new JButton ("neu");
		rulesLockButton = new JButton ("sperren");
		rulesDeleteButton = new JButton ("l�schen");
		rulesActivateButton = new JButton ("aktivieren");
		rulesStartButton = new JButton ("spielen");
		
		//Buttons Postionieren
		rulesActivateButton.setBounds(670, 60 ,ClientApplication.BUTTON_WIDTH , ClientApplication.BUTTON_HEIGHT );
		rulesLockButton.setBounds(670,90 , ClientApplication.BUTTON_WIDTH,ClientApplication.BUTTON_HEIGHT  );
		rulesDeleteButton.setBounds(670, 120 ,ClientApplication.BUTTON_WIDTH ,ClientApplication.BUTTON_HEIGHT  );
		rulesStartButton.setBounds(670, 190 ,ClientApplication.BUTTON_WIDTH , ClientApplication.BUTTON_HEIGHT  );
		
		rulesNewButton.setBounds(150,300,ClientApplication.BUTTON_WIDTH, ClientApplication.BUTTON_HEIGHT );
		
		//Border
		rulesActivateButton.setBorder(ClientApplication.BUTTON_BORDER);
		rulesLockButton.setBorder(ClientApplication.BUTTON_BORDER);
		rulesDeleteButton.setBorder(ClientApplication.BUTTON_BORDER);
		rulesStartButton.setBorder(ClientApplication.BUTTON_BORDER);
		rulesNewButton.setBorder(ClientApplication.BUTTON_BORDER);
		
		//add tooltip to buttons
		rulesNewButton.setToolTipText("neues Regelwerk anlegen");
		rulesLockButton.setToolTipText("Regelwerk f�r Nutzer sperren");
		rulesDeleteButton.setToolTipText("Regelwerk l�schen");
		rulesActivateButton.setToolTipText("Regelwerk aktivieren");
		rulesStartButton.setToolTipText("Regelwerk starten");
		
		//Farbgebung der Buttons
		rulesNewButton.setBackground(ClientApplication.BUTTON_COLOR);
		rulesNewButton.setForeground(ClientApplication.FOREGROUND_COLOR);
		rulesLockButton.setBackground(ClientApplication.BUTTON_COLOR);
		rulesLockButton.setForeground(ClientApplication.FOREGROUND_COLOR);
		rulesDeleteButton.setBackground(ClientApplication.BUTTON_COLOR);
		rulesDeleteButton.setForeground(ClientApplication.FOREGROUND_COLOR);
		rulesActivateButton.setBackground(ClientApplication.BUTTON_COLOR);
		rulesActivateButton.setForeground(ClientApplication.FOREGROUND_COLOR);
		rulesStartButton.setBackground(ClientApplication.BUTTON_COLOR);
		rulesStartButton.setForeground(ClientApplication.FOREGROUND_COLOR);

		
		//add ActionListener to rulesButtons
		rulesNewButton.addActionListener(new AdminRulesActionListenerImpl(this));
		rulesLockButton.addActionListener(new AdminRulesActionListenerImpl(cp,this,rulesTable));
		rulesDeleteButton.addActionListener(new AdminRulesActionListenerImpl(cp,this, rulesTable));
		rulesActivateButton.addActionListener(new AdminRulesActionListenerImpl(cp,this, rulesTable));
		rulesStartButton.addActionListener(new AdminRulesActionListenerImpl(cp,this,rulesTable));
		
		//add components to panel
		rulesPanel.add(rulesLabel);
		rulesPanel.add(rulesNewLabel);
		rulesPanel.add(rulesTableScrollPane);
		rulesPanel.add(rulesNewButton);
		rulesPanel.add(rulesLockButton);
		rulesPanel.add(rulesDeleteButton);
		rulesPanel.add(rulesActivateButton);
		rulesPanel.add(rulesStartButton);
		rulesPanel.add(existRulesLabel);

		return rulesPanel;
	}//Ende createRulesManagePanel()
	
	/*
	 * Methode zum Aufbau der Oberfl�che "Nutzerverwaltung
	 * @param Container ContentPane
	 * @return JPanel mit Oberfl�che f�r Nutzerverwaltung
	 */
	private JPanel createUsersManagePanel(Container cp)
	{
		/*panel for usermanagement*/
		usersPanel = new JPanel(null);
		usersPanel.setBounds(0,0,800,350);
		usersPanel.setBackground(ClientApplication.BACKGROUND_COLOR);
		usersPanel.setForeground(ClientApplication.FOREGROUND_COLOR);
		
		//create Label
		usersLabel = new JLabel(" ");
		//set position of label
		usersLabel.setBounds(10,0,770,20);
		//create border (underline) for this label
		usersLabel.setBorder(BorderFactory.createMatteBorder(0,0,1,0,ClientApplication.FOREGROUND_COLOR));
		//set new font for label
		usersLabel.setFont(new Font("Arial",Font.BOLD,14));
		//set new color for font
		usersLabel.setForeground(ClientApplication.FOREGROUND_COLOR);

		//table for usermanagement	
		//create the tableModel
		usersTableModel = new AdminUsersTableModel();
		//create the table an add the tableModel to it
		sorter.setModel(usersTableModel); 
		usersTable = new JTable(sorter);
	//	usersTable = new JTable(usersTableModel);
	
		sorter.addMouseListenerToHeaderInTable(usersTable); 	
		usersTable.getTableHeader().setToolTipText(
					   "Klick zum sortieren; Shift-Klick um absteigend zu sortieren");
		//desing Table					   
		usersTable.getTableHeader().setForeground(ClientApplication.FOREGROUND_COLOR);
		usersTable.getTableHeader().setFont(new Font("Arial",Font.BOLD,10));
		usersTable.getTableHeader().setBackground(ClientApplication.TABLE_COLOR);
		usersTable.getTableHeader().setForeground(ClientApplication.FOREGROUND_COLOR);
	
		//only one row selectable
		usersTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);	
		//Create the scroll pane and add the table to it.
		usersTableScrollPane = new JScrollPane(usersTable,
												JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
												JScrollPane.HORIZONTAL_SCROLLBAR_NEVER
												);
		//Positionierung + Gr��e der Tabele inklusive ScrollBars
		usersTableScrollPane.setBounds(345,40,300,400);
		//Label for current server-state	
		usersCurrentRegLabel = new JLabel("Zur Zeit sind "+usersTable.getRowCount()+" Benutzer registriert,");
		usersCurrentOnlineLabel = new JLabel("davon sind "+usersTableModel.getRowCount("online")+" Nutzer online.");
		
		usersCurrentRegLabel.setBounds(20,50,300,20);
		usersCurrentOnlineLabel.setBounds(20,70,300,20);
		
		usersCurrentRegLabel.setForeground(ClientApplication.FOREGROUND_COLOR);
		usersCurrentOnlineLabel.setForeground(ClientApplication.FOREGROUND_COLOR);

		//label for max server-state
		usersMaxOnlineLabel = new JLabel("maximale Anzahl eingeloggter Nutzer ");
		usersMaxRegLabel = new JLabel("maximale Anzahl registrierter Benutzer ");

		usersMaxOnlineLabel.setBounds(20,150,240,20);
		usersMaxRegLabel.setBounds(20,190,240,20);
	
		usersMaxOnlineLabel.setForeground(ClientApplication.FOREGROUND_COLOR);
		usersMaxRegLabel.setForeground(ClientApplication.FOREGROUND_COLOR);
		//TextField for max server-state	
		usersMaxOnlineTextField = new JTextField();
		usersMaxRegTextField = new JTextField();
		
		usersMaxOnlineTextField.setBounds(260,150,50,20);
		usersMaxRegTextField.setBounds(260,190,50,20);
		
		usersMaxOnlineTextField.setBackground(ClientApplication.COMP_BACKGROUND_COLOR);
		usersMaxOnlineTextField.setForeground(ClientApplication.FOREGROUND_COLOR);

		usersMaxRegTextField.setBackground(ClientApplication.COMP_BACKGROUND_COLOR);
		usersMaxRegTextField.setForeground(ClientApplication.FOREGROUND_COLOR);
		
		//create user-buttons
		usersInfoButton = new JButton("Info");
		usersDeleteButton = new JButton("l�schen");
		usersMaxSaveButton = new JButton("speichern");
		
		//Positionierung + Ausrichtung der Buttons
		usersInfoButton.setBounds(670,390,ClientApplication.BUTTON_WIDTH,ClientApplication.BUTTON_HEIGHT);
		usersDeleteButton.setBounds(670,420,ClientApplication.BUTTON_WIDTH,ClientApplication.BUTTON_HEIGHT);
		usersMaxSaveButton.setBounds(20,230,ClientApplication.BUTTON_WIDTH,ClientApplication.BUTTON_HEIGHT);
		
		//add tooltip to buttons
		usersInfoButton.setToolTipText("Informationen zu Nutzer");
		usersDeleteButton.setToolTipText("Nutzer l�schen");
		usersMaxSaveButton.setToolTipText("�nderungen speichern");
		
		//add ActionListener to buttons
		usersInfoButton.addActionListener(new AdminUsersActionListenerImpl(this,usersTable,cp));
		usersDeleteButton.addActionListener(new AdminUsersActionListenerImpl(this,usersTable));
		usersMaxSaveButton.addActionListener(new AdminUsersActionListenerImpl(this,usersMaxOnlineTextField, usersMaxRegTextField));
		
		//Border
		usersMaxSaveButton.setBorder(ClientApplication.BUTTON_BORDER);
		usersInfoButton.setBorder(ClientApplication.BUTTON_BORDER);
		usersDeleteButton.setBorder(ClientApplication.BUTTON_BORDER);
		
		//Farbgebung der Buttons
		usersMaxSaveButton.setBackground(ClientApplication.BUTTON_COLOR);
		usersMaxSaveButton.setForeground(ClientApplication.FOREGROUND_COLOR);
		usersInfoButton.setBackground(ClientApplication.BUTTON_COLOR);
		usersInfoButton.setForeground(ClientApplication.FOREGROUND_COLOR);
		usersDeleteButton.setBackground(ClientApplication.BUTTON_COLOR);
		usersDeleteButton.setForeground(ClientApplication.FOREGROUND_COLOR);

		
		//add components to panel
		usersPanel.add(usersLabel);
		usersPanel.add(usersTableScrollPane);
		usersPanel.add(usersInfoButton);
		usersPanel.add(usersDeleteButton);
		usersPanel.add(usersMaxSaveButton);
		usersPanel.add(usersCurrentOnlineLabel);
		usersPanel.add(usersCurrentRegLabel);
		usersPanel.add(usersMaxOnlineLabel);
		usersPanel.add(usersMaxRegLabel);
		usersPanel.add(usersMaxOnlineTextField);
		usersPanel.add(usersMaxRegTextField);

		return usersPanel;
	}//Ende createUsersMangePanel()
}
