package adventure.client;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

/**
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */

public class GUI extends JFrame{

  private JPanel imagePanel;
  private JPanel controlPanel;
  public static JTextField descriptionText;
  public JPanel dialogPanel;
  public static JTextField dialogText;
  public JDesktopPane desktop = new JDesktopPane();
  private static boolean optionFrameOpen = false;
  private JInternalFrame menu;

  /**/
  private String[] buttons = {"adventure/client/images/use1.jpg",
  								"adventure/client/images/use2.jpg", 
								"adventure/client/images/take1.jpg", 
								"adventure/client/images/take2.jpg", 
								"adventure/client/images/lookat1.jpg",
								"adventure/client/images/lookat2.jpg",
							  	"adventure/client/images/talkto1.jpg",
							  	"adventure/client/images/talkto2.jpg",
							  	"adventure/client/images/opt1.jpg",
							  	"adventure/client/images/opt2.jpg"};

  private String[] images = {"adventure/client/images/scene01.jpg","adventure/client/images/test.gif"};

 /* public GUI() {
    super("Online-Adventure");
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setResizable(false);
    getContentPane().setLayout(null);
    initComponents();
    pack();
    setSize(new Dimension(800,600));
    setVisible(true);
  }*/

  public void init(Container cp) {
  	
  	//cp.setContentPane(desktop);
  	cp.setLayout(null);
  	//imagePanel contains the background and all visible items
    imagePanel = new JPanel();
    imagePanel.setLayout(null);
    imagePanel.setBounds(0,0,800,400);
    imagePanel.setBackground(Color.blue);
    //controlPanel contains all buttons, the status textfield and the inventory
    controlPanel = new JPanel();
    controlPanel.setLayout(null);
    controlPanel.setBounds(0,400,800,200);
    controlPanel.setBackground(Color.black);
    //descriptionText is a textfield that shows messages to the user
    descriptionText = new JTextField("Hallo Spieler!");
    descriptionText.setBounds(20,10,300,20);
    descriptionText.setForeground(Color.white);
    descriptionText.setOpaque(true);
    descriptionText.setBackground(Color.black);
    descriptionText.setCaretColor(Color.lightGray);
    descriptionText.setEditable(false);
    //the inventory is a JTable placed into a ScrollPane
    JTableHeader inventoryHeader = new JTableHeader();
    inventoryHeader.setReorderingAllowed(false);
    inventoryHeader.setResizingAllowed(false);
    JTable inventoryTable = new JTable(3,4);
    inventoryTable.setRowHeight(46);
    inventoryTable.setCellSelectionEnabled(true);
    inventoryTable.setTableHeader(inventoryHeader);
    inventoryTable.setBackground(Color.black);
    inventoryTable.setSelectionForeground(Color.lightGray);
    inventoryTable.setSelectionBackground(Color.black);
    JScrollPane tableScrollPane = new JScrollPane(inventoryTable);
    tableScrollPane.setBounds(540,5,220,164);
    tableScrollPane.setBorder(new TitledBorder("Inventar"));
    tableScrollPane.getViewport().setBackground(Color.black);
    //create the dialogPanel and its TextField
    dialogPanel = new JPanel();
    dialogPanel.setLayout(null);
    dialogPanel.setBounds(20,370,760,20);
    dialogPanel.setBackground(Color.black);
    dialogText = new JTextField();
    dialogText.setBounds(0,0,760,20);
    dialogText.setForeground(Color.white);
    dialogText.setOpaque(true);
    dialogText.setBackground(Color.black);
    dialogText.setCaretColor(Color.lightGray);
    dialogText.setEditable(false);
    dialogText.setText("abcdefghijklmnopqrstuvwxyz");
    dialogPanel.add(dialogText);
    imagePanel.add(dialogPanel);
    //add the inventory and the status textfield to the controlPanel
    controlPanel.add(tableScrollPane);
    controlPanel.add(descriptionText);
    //call placeImages() to arrange all images and buttons
    placeImages();
    //add both the controlPanel and imagePanel to the GUI
    cp.add(controlPanel);
    cp.add(imagePanel);
  }

  public void placeImages() {
    //insert background image
    JLabel backgroundImageLabel = new JLabel(new ImageIcon((String)images[0]));
    backgroundImageLabel.setBounds(0,0,800,400);

//--> Test !! (Einf�gen eines Icons in die Szene)
    ImageIcon testIcon = new ImageIcon((String)images[1]);
    JLabel testLabel = new JLabel(testIcon);
    testLabel.setBounds(200,150,testIcon.getIconWidth(),testIcon.getIconHeight());
    testLabel.addMouseListener(new MyMouseListener(testLabel,"Apfel",testIcon,
        testIcon,true));
    imagePanel.add(testLabel);
//<-- Test !!

    imagePanel.add(backgroundImageLabel);
    //create JLabels for use as Buttons with own images
    JLabel useButton = new JLabel(new ImageIcon((buttons[0])));
    useButton.setBounds(50,45,110,50);
    useButton.addMouseListener(new MyMouseListener(useButton, "Benutze",
        new ImageIcon(buttons[0]),new ImageIcon(buttons[1]),false));
    JLabel takeButton = new JLabel(new ImageIcon((buttons[2])));
    takeButton.setBounds(170,45,110,50);
    takeButton.addMouseListener(new MyMouseListener(takeButton, "Nimm",
        new ImageIcon(buttons[2]),new ImageIcon(buttons[3]),false));
    JLabel lookatButton = new JLabel(new ImageIcon((buttons[4])));
    lookatButton.setBounds(190,110,110,50);
    lookatButton.addMouseListener(new MyMouseListener(lookatButton, "Schau an",
        new ImageIcon(buttons[4]),new ImageIcon(buttons[5]),false));
    JLabel talktoButton = new JLabel(new ImageIcon((buttons[6])));
    talktoButton.setBounds(70,110,110,50);
    talktoButton.addMouseListener(new MyMouseListener(talktoButton, "Rede mit",
        new ImageIcon(buttons[6]),new ImageIcon(buttons[7]),false));
    //the optionButton opens the menu
    final JLabel optionButton = new JLabel(new ImageIcon((buttons[8])));
    optionButton.setBounds(380,110,110,50);
    optionButton.addMouseListener(new MouseListener(){
      public void mouseClicked(MouseEvent e) {
      }
      public void mousePressed(MouseEvent e) {
        if(optionFrameOpen) {
          optionFrameOpen = false;
          desktop.getSelectedFrame().dispose();
        }
        else {
          optionFrameOpen = true;
          optionFrame();
        }
      }
      public void mouseReleased(MouseEvent e) {
      }
      public void mouseEntered(MouseEvent e) {
        optionButton.setIcon(new ImageIcon(buttons[9]));
      }
      public void mouseExited(MouseEvent e) {
        optionButton.setIcon(new ImageIcon(buttons[8]));
      }
    });
    //add buttons to controlPanel
    controlPanel.add(useButton);
    controlPanel.add(takeButton);
    controlPanel.add(lookatButton);
    controlPanel.add(talktoButton);
    controlPanel.add(optionButton);
  }

  //this method opens an internal frame in the imagePanel
  public void optionFrame() {
    //create buttons for internal frame
    JButton saveGame = new JButton("Speichern");
    saveGame.setBounds(10,10,170,40);
    JButton loadGame = new JButton("Laden");
    loadGame.setBounds(10,60,170,40);
    JButton newGame = new JButton("Neues Spiel");
    newGame.setBounds(10,110,170,40);
    JButton endGame = new JButton("Spiel Beenden");
    endGame.setBounds(10,160,170,40);
    endGame.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        System.exit(0);
      }
    });
    //create non-resizable internal frame itself
    menu = new JInternalFrame("Optionen",false);
    menu.getContentPane().setLayout(null);
    menu.setSize(200,250);
    menu.setLocation(400,40);
    menu.setVisible(true);
    //add buttons to internal frame
    menu.getContentPane().add(saveGame);
    menu.getContentPane().add(loadGame);
    menu.getContentPane().add(newGame);
    menu.getContentPane().add(endGame);
    //add internal frame to desktoppane (for dragging) and set its state
    //to selected
    desktop.add(menu);
    try {
      menu.setSelected(true);
    }
    catch (java.beans.PropertyVetoException pve){}
  }

  public static void main(String[] args) {
    try {
        UIManager.setLookAndFeel(
            UIManager.getCrossPlatformLookAndFeelClassName());
    } catch (Exception e) { }
    GUI GUI1 = new GUI();
  }
}

class MyMouseListener implements MouseListener {

  private ImageIcon normal;
  private ImageIcon over;
  private JLabel label;
  private String description;
  private boolean isIcon;

  public MyMouseListener(JLabel label, String description ,
                         ImageIcon normal, ImageIcon over, boolean isIcon) {
    this.label=label;
    this.description=description;
    this.normal=normal;
    this.over=over;
    this.isIcon = isIcon;
  }

  public void mouseClicked(MouseEvent e) {
  }

  public void mousePressed(MouseEvent e) {
    if(isIcon) {
      GUI.descriptionText.setText(GUI.descriptionText.getText() + " " + description);
    }
    else {
      GUI.descriptionText.setText(description);
    }
  }

  public void mouseReleased(MouseEvent e) {
  }

  public void mouseEntered(MouseEvent e) {
    if(isIcon) {
      GUI.dialogText.setText(description);
    }
    else {
      label.setIcon(over);
    }
  }

  public void mouseExited(MouseEvent e) {
    if(isIcon) {
      GUI.dialogText.setText("");
    }
    else {
      label.setIcon(normal);
    }
  }
}

class MyTableModel extends DefaultTableModel {

  public MyTableModel(Object[][] daten, Object[] headers) {
    super(daten,headers);
  }

  public Class getColumnClass(int col) {
    return getValueAt(0,col).getClass();
  }

  public boolean isCellEditable(int row, int col) {
    Class columnClass = getColumnClass(col);
    return columnClass != ImageIcon.class;
  }
}
