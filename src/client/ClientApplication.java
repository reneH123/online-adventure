/*
 * Created on Dec 15, 2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package adventure.client;
import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;

/**
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class ClientApplication extends JFrame
 {
 	//Farbgebung f�r Client 
	public static final Color BACKGROUND_COLOR = new Color(81,101,105);
	public static final Color COMP_BACKGROUND_COLOR = new Color(134,148,150);
	public static final Color FOREGROUND_COLOR = Color.WHITE;
	public static final Color BUTTON_COLOR = new Color(138,138,138);
	public static final Color TABLE_COLOR = new Color(134,148,150);
	public static final BevelBorder BUTTON_BORDER = new BevelBorder(BevelBorder.LOWERED,Color.DARK_GRAY,Color.GRAY);
	public static final BevelBorder TABLE_BORDER = new BevelBorder(BevelBorder.LOWERED,Color.DARK_GRAY,Color.GRAY);

	/*Gr��e der Buttons festlgen*/  
	public static final int BUTTON_WIDTH = 100;
	public static final int BUTTON_HEIGHT = 20;

	//verschieden GUI's
	private AdminGUI adminGUI;
	private LoginGUI loginGUI;
	private RegisterGUI registerGUI;
	private UserGUI userGUI;
	private GUI gui;
	private static ClientApplication client;

		
	public ClientApplication() {
		//Titel f�r Fenster
		super(" DOLGAR'S WORLD  ");
		try 
		{
				UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch(Exception e){
				;
			}
		setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE  );
						
		//erste GUI initialisieren
		
		/*
		LoginGUI login = new LoginGUI();
		login.init(getContentPane());
		*/
		
		
		//nur zum testen -> Administrationsbereich
		/*adminGUI = new AdminGUI();
		adminGUI.init(getContentPane());*/
		
		
		//nur zum testen -> Spieloberfl�che
		
		/*UserGUI user = new UserGUI();
		user.init(getContentPane());*/
		
		//nur zum testen -> Nutzerdaten
		
		GUI gui = new GUI();
		gui.init(getContentPane());
		/**/

		//nur zum testen -> Nutzerdaten
		//bitte klicken Registieren
		/*RegisterGUI reg = new RegisterGUI();
		reg.init(getContentPane());*/
		/**/
	}	
	public static void main(String[] args) {
		client = new ClientApplication();
		client.setSize(800,600);
		client.setResizable(false);
		client.setLocation(100,100);
		//frame.pack();
		client.setVisible(true);
	}
}
