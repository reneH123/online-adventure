/*
 * Created on Dec 15, 2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package adventure.client;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.List;

import adventure.gameobjects.*;


import adventure.server.Server;

/**
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class ClientCommunicator {
	private int port;
	private String serverName;
	private final String SERVICE_NAME = "RMI_ServerCommunicator";
	private Registry registry;
	private Server server;
	
	public ClientCommunicator(String serverName, int port) throws Exception
	{
		this.port = port;
		this.serverName = serverName;
		
		//Let's achieve servers register ...
		try
		{
			registry = LocateRegistry.getRegistry(serverName, port);
		}
		catch (RemoteException ex)
		{
			throw new Exception("Error: Can't achieve servers registry.");
		}
		
		try
		{
			server = (Server) registry.lookup(SERVICE_NAME);
		}
		catch (NotBoundException ex1)
		{
			throw new Exception("Error: There isn't any registry");
		}
		catch (RemoteException ex1)
		{
			throw new Exception("Error: Connection was broken");
		}
	}
	
	/**
	 * Liefert die aktuelle Szene f?r das angegebene Spiel
	 * @param gameID ID des aktuellen Spiels
	 * @return Scene Die im Moment aktuelle Szene
	 * @throws Exception
	 */
	public Scene getCurrentScene(int gameID) throws Exception
	{
		try
		{
			return server.getCurrentScene(gameID);
		}catch (RemoteException ex)
		{
			throw new Exception(ex.getMessage());
		}
	}
	
	/**
	 * Sendet eine Aktion des Benutzers an den Server. Diese wird dort ausgewertet
	 * und der Client
	 * erh?lt eine aktualisierte Szene zum Anzeigen
	 * @param gameID
	 * @param userAction
	 * @return Scene
	 * @throws Exception
	 */
	public Scene sendAction(int gameID, UserAction userAction) throws Exception
	{
		try
		{
			return server.sendAction(gameID, userAction);
		}catch (RemoteException ex)
		{
			throw new Exception(ex.getMessage());
		}
	}
	
	/**
	 * L?dt das Spiel mit dem angegebenen Namen und liefert die aktuelle Szene des
	 * geladenen Spiels zur?ck.
	 * @param gameID Die ID des Gespeicherten Spieles
	 * @param saveGameName Name des Gespeicherten Spieles
	 * @return Die Startszene des Spieles
	 * @throws Exception
	 */
	public Scene loadGame(int gameID, String saveGameName) throws Exception
	{
		try
		{
			return server.loadGame(gameID, saveGameName);
		}catch (RemoteException ex)
		{
			throw new Exception(ex.getMessage());
		}
	}
	/**
	 * Speichert das aktuelle Spiel unter dem angegebenen Namen ab und liefert eine
	 * Erfolgsmeldung zur?ck.
	 * @param gameID
	 * @param saveGameName
	 * @return boolean 
	 * @throws Exception
	 */
	public void saveGame(int gameID, String saveGameName) throws Exception
	{
		try
		{
			 server.saveGame(gameID, saveGameName);
		}catch (RemoteException ex)
		{
			throw new Exception(ex.getMessage());
		}
	}
	
	/**
	 * Versucht den Benutzer mithilfe der angegebenen Login / Passwort Kombination
	 * einzuloggen. Liefert die Spiel Id zurueck.
	 * @param gameID
	 * @param userName
	 * @param password
	 * @return GameId
	 * @throws Exception
	 */
	public int login(String userName, String password) throws Exception
	{
		try
		{
			return server.login(userName, password);
		}catch (RemoteException ex)
		{
			throw new Exception(ex.getMessage());
		}
	}
	
	/**
	 * Beendet das angegebene Spiel. Der Client muss nicht zwangsl?ufig beendet
	 * werden. Es kann ein neues Spiel begonnen werden oder ein Spielstand geladen
	 * werden.
	 * @param gameID
	 * @throws Exception
	 */
	public void quit(int gameID) throws Exception
	{
		try
		{
			server.quit(gameID);
		}catch (RemoteException ex)
		{
			throw new Exception(ex.getMessage());
		}
	}
	
	/**
	 * Loggt den Benutzer aus. Danach kann kein neues Spiel mehr gestartet werden.
	 * Der Client sollte sich nach Aufruf dieser Funktion ebenfalls beenden.
	 * @param gameID
	 * @throws Exception
	 */
	public void logout(int gameID) throws Exception
	{
		try
		{
			server.logout(gameID);
		}catch (RemoteException ex)
		{
			throw new Exception(ex.getMessage());
		}
	}

	/**
	 * Die Regeln.
	 * @return Object[][]
	 * @throws Exception
	 */
	public List getRules() throws Exception
	{
		try
		{
			return server.getRules();
		}catch (RemoteException ex)
		{
			throw new Exception(ex.getMessage());
		}
	}
	/**
	 * L�scht eine gegebene Regelbasis.
	 * @param ruleSet Ein String der den Namen repr�sentiert.
	 * @return boolean Des Gelingens Widerhall.
	 * @throws Exception
	 */
    public void deleteRules(RuleSet ruleSet) throws Exception
	{
		try
		{
			server.deleteRules(ruleSet);
		}catch (RemoteException ex)
		{
			throw new Exception(ex.getMessage());
		}
	}
	
	/**
	 * Setzt den Status einer gegebenen Regelbasis auf "aktiv"
	 * @param ruleSet Die Regelbasis.
	 * @return boolean Des Gelingens Widerhall.
	 * @throws Exception
	 */ 
    public void updateRuleState(RuleSet ruleSet) throws Exception
	{
		try
		{
			server.updateRuleState(ruleSet);
		}catch (RemoteException ex)
		{
			throw new Exception(ex.getMessage());
		}
	}

	
	/**
	 * Die Benutzer all als Doppelarray.
	 * @return 
	 * @throws Exception
	 */ 
    public List getUsers() throws Exception
	{
		try
		{
			return server.getUsers();
		}catch (RemoteException ex)
		{
			throw new Exception(ex.getMessage());
		}	
	}
	
	/**
	 * Die Benutzer einer bestimmten Regelbasis als Doppelarray.
	 * @param ruleSet Die Regelbasis.
	 * @return 
	 * @throws Exception
	 */ 
    public List getUsers(RuleSet ruleSet) throws Exception
	{
		try
		{
			return server.getUsers(ruleSet);
		}catch (RemoteException ex)
		{
			throw new Exception(ex.getMessage());
		}	
	}
	
	/**
	 * Ein Benutzer wird �ber den Namen ermittelt.
	 * @param userName Der Name des gew�nschten Nutzers.
	 * @return User Der Nutzer.
	 * @throws Exception
	 */
    public User getUserInfo(String userName) throws Exception
	{
		try
		{
			return server.getUserInfo(userName);
		}catch (RemoteException ex)
		{
			throw new Exception(ex.getMessage());
		}	
	}
	
	/**
	 * Ein Benutzer wird �ber den Namen ermittelt und gel�scht.
	 * @param userName Der Name des zu l�schenden Nutzers.
	 * @return boolean Des Gelingens Widerhall.
	 * @throws Exception
	 */
    public void deleteUser(User userName) throws Exception
	{
		try
		{
			server.deleteUser(userName);
		}catch (RemoteException ex)
		{
			throw new Exception(ex.getMessage());
		}	
	}
	
	/**
	 * Gibt die Maximalzahl m�glicher online Nutzer. 
	 * @return Die Zahl.
	 * @throws Exception
	 */
    public int getMaxOnlineUsers() throws Exception
	{
		try
		{
			return server.getMaxOnlineUsers();
		}catch (RemoteException ex)
		{
			throw new Exception(ex.getMessage());
		}	
	}
	
	/**
	 * Setzt die Maximalzahl m�glicher online Nutzer. 
	 * @param Die Zahl.
	 * @throws Exception
	 */
    public  void setMaxOnlineUsers(int maxOnlineUsers) throws Exception
	{
		try
		{
			server.setMaxOnlineUsers(maxOnlineUsers);
		}catch (RemoteException ex)
		{
			throw new Exception(ex.getMessage());
		}	
	}
	
	/**
	 * Gibt die Maximalzahl m�glicher registrierten Nutzer. 
	 * @return Die Zahl.
	 * @throws Exception
	 */
    public int getMaxRegUsers() throws Exception
	{
		try
		{
			return server.getMaxRegUsers();
		}catch (RemoteException ex)
		{
			throw new Exception(ex.getMessage());
		}
	}
	
	/**
	 * Setzt die Maximalzahl m�glicher registrierten Nutzer. 
	 * @param Die Zahl.
	 * @throws Exception
	 */
    public void setMaxRegUsers(int maxRegUsers) throws Exception
	{
		try
		{
			server.setMaxRegUsers(maxRegUsers);
		}catch (RemoteException ex)
		{
			throw new Exception(ex.getMessage());
		}	
	}
	
	/**
	 * Registriert einen neuen Nutzer.
	 * @param user Der Neue.
	 * @throws Exception
	 */
    public void registerUser(User user) throws Exception
	{
		try
		{
			server.registerUser(user);
		}catch (RemoteException ex)
		{
			throw new Exception(ex.getMessage());
		}	
	}
	
	/**
	 *	Aktualisiert die Nutzerdaten.
	 * @param user Der Nutzer.
	 * @throws Exception
	 */
    public void updateUser(User user) throws Exception
	{
		try
		{
			server.updateUser(user);
		}catch (RemoteException ex)
		{
			throw new Exception(ex.getMessage());
		}	
	}

    public void shutdownServer() throws RemoteException {
        server.shutdownServer();
    }

}

