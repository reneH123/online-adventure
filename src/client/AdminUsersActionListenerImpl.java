package adventure.client;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;


/**
 * Folgendes ausw�hlen, um die Schablone f�r den erstellten Typenkommentar zu �ndern:
 * Fenster&gt;Benutzervorgaben&gt;Java&gt;Codegenerierung&gt;Code und Kommentare
 */
public class AdminUsersActionListenerImpl implements ActionListener 
{
	private JTextField maxRegTextField;
	private JTextField maxOnlineTextField;
	private JTable table;
	private Component component;
	private Container container;
	private int message;
	private JPanel userInfoPanel;
	private JLabel userFirstNameLabel, userLastNameLabel, userNickNameLabel;
	private JLabel userMailLabel, userStateLabel, userOnlineTimeLabel;
	
	/*
	 * Konstruktor
	 * @param Component Elter-Component
	 * @param JTable Tabelle mit Nutzern
	 */
	public AdminUsersActionListenerImpl(Component c, JTable t)
	{
		this.component = c;
		this.table = t;
	}
	

	/*
	 * Konstruktor
	 * @param Component Elter-Component
	 * @param JTable Tabelle mit Nutzern
	 * @param Container ContentPane
	 */
	public AdminUsersActionListenerImpl(Component c, JTable t, Container cp)
	{
		this.component = c;
		this.table = t;
		this.container = cp;
	}
	
	/*
	 * Konstruktor
	 * @param Component Elter-Component
	 * @param JTextField # max Online-Nutzer
	 * @param JTextField # max registrierter Nutzer
	 */
	public AdminUsersActionListenerImpl(Component c, JTextField maxOnline, JTextField maxReg)
	{
		this.component = c;
		this.maxOnlineTextField = maxOnline;
		this.maxRegTextField = maxReg;
	}
	
	/* (Kein Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent event) 
	{

		Object[] object = {"OK","Chancel"};
		String cmd = event.getActionCommand();
		System.out.println(cmd);
		if(!cmd.equals("speichern"))
		{
			try {
				if(cmd.equals("l�schen"))
				{
					message=JOptionPane.showConfirmDialog(component,
									"Wollen Sie den Nutzer "+ table.getValueAt(table.getSelectedRow(),0) +" wirklich "+cmd+"?",
									"Sicherheitsdialog",
									JOptionPane.OK_CANCEL_OPTION,
									JOptionPane.WARNING_MESSAGE
									);
				}
				else 
				{
					System.out.println("Nutzerinformationen f�r "+table.getValueAt(table.getSelectedRow(),0));					
				}					
			} 
			catch(Exception e) 
			{
				JOptionPane.showConfirmDialog(component,
												"Keine Daten ausgew�hlt!",
												"Fehler",
												JOptionPane.CLOSED_OPTION,
												JOptionPane.ERROR_MESSAGE);
			}
		} else {
			if(maxOnlineTextField.getText().matches("[1-9]+[0-9]*") && maxRegTextField.getText().matches("[1-9]+[0-9]*"))
				System.out.println(maxOnlineTextField.getText()+" und "+maxRegTextField.getText()+" speichern");
			else
				JOptionPane.showConfirmDialog(component,
											"Bitte korrekte Werte angeben",
											"Fehler",
											JOptionPane.CLOSED_OPTION,
											JOptionPane.ERROR_MESSAGE);
 
								
		}
		if (message==JOptionPane.YES_OPTION && cmd.equals("l�schen"))
			System.out.println("l�sche...");

	}
}
