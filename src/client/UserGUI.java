package adventure.client;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
/**
 * Folgendes ausw�hlen, um die Schablone f�r den erstellten Typenkommentar zu �ndern:
 * Fenster&gt;Benutzervorgaben&gt;Java&gt;Codegenerierung&gt;Code und Kommentare
 */
public class UserGUI extends JFrame implements ActionListener
{
	private JPanel banner,userPanel;
	private JButton logout,emailChange,passwordChange,accountErase,playing,loading;
	private JLabel email,imageLabel,label,myProfil,playOption;
	private JTextField emailname;
	
	private UserRulesTableModel userRulesTableModel;
	private LoadGameTableModel loadGameTableModel;
	private JTable rulesTable;
	private JTable loadGameTable;
	
	private JScrollPane rulesTableScrollPane;
	private JScrollPane loadGameTableScrollPane;
    	/*TESTSTRING*/
	private String image = "adventure/client/images/banner.gif";
	public void init(Container cp)
	{
		cp.setLayout(null);
		banner = new JPanel(null);
		banner.setBounds(0,0,800,150);
		banner.setBackground(ClientApplication.BACKGROUND_COLOR);
		banner.setForeground(ClientApplication.FOREGROUND_COLOR);
		label = new JLabel("hallo!!willkommen in das Spiel");
		label.setBounds(10,130,150,30);
		label.setForeground(ClientApplication.FOREGROUND_COLOR);	
		logout = new JButton("Logout");
		logout.setBounds(600,130,ClientApplication.BUTTON_WIDTH,ClientApplication.BUTTON_HEIGHT);
		logout.setBorder(ClientApplication.BUTTON_BORDER);
		logout.setBackground(ClientApplication.BUTTON_COLOR);
		logout.setToolTipText("Wollen Sie Logout?");
		logout.addActionListener(this);
		//banner
		ImageIcon testIcon = new ImageIcon(image);
		JLabel backgroundImageLabel = new JLabel(testIcon);
		backgroundImageLabel.setBounds(0,0,testIcon.getIconWidth(),testIcon.getIconHeight());
		banner.add(backgroundImageLabel);
		banner.add(label);
		banner.add(logout);
		cp.add(banner);	 
		
		userPanel = new JPanel(null);
		userPanel.setBounds(0,0,800,600);
		userPanel.setBackground(ClientApplication.BACKGROUND_COLOR);
		userPanel.setForeground(ClientApplication.FOREGROUND_COLOR);
		
		myProfil = new JLabel("Mein Profil");
		myProfil.setBounds(10,170,770,20);
		myProfil.setBorder(BorderFactory.createMatteBorder(0,0,1,0,ClientApplication.FOREGROUND_COLOR));
		myProfil.setFont(new Font("Arial",Font.BOLD,14));
		
		playOption = new JLabel("SpielOption");
		playOption.setBounds(10,300,770,20);
		playOption.setBorder(BorderFactory.createMatteBorder(0,0,1,0,ClientApplication.FOREGROUND_COLOR));
		playOption.setFont(new Font("Arial",Font.BOLD,14));
		
		email = new JLabel("email");
		email.setBounds(10,205,100,50);
		email.setFont(new Font("Arial",Font.BOLD,14));
		
		emailname = new JTextField("");
		
		
		
		emailChange = new JButton("Aendern");
		passwordChange = new JButton("PasswortAenderung");
		accountErase = new JButton("Accountloeschen");
		playing = new JButton("spielen");
		loading = new JButton("laden");
		
		//Button und TextField positionieren
		emailname.setBounds(50,220,190,ClientApplication.BUTTON_HEIGHT);
		emailChange.setBounds(250,220,ClientApplication.BUTTON_WIDTH , ClientApplication.BUTTON_HEIGHT );
		passwordChange.setBounds(600,220,120, ClientApplication.BUTTON_HEIGHT );   
        accountErase.setBounds(600,270,ClientApplication.BUTTON_WIDTH,ClientApplication.BUTTON_HEIGHT);
        playing.setBounds(250,530,ClientApplication.BUTTON_WIDTH,ClientApplication.BUTTON_HEIGHT);
        loading.setBounds(640,530,ClientApplication.BUTTON_WIDTH,ClientApplication.BUTTON_HEIGHT);
        
        //Border
        emailChange.setBorder(ClientApplication.BUTTON_BORDER);
        passwordChange.setBorder(ClientApplication.BUTTON_BORDER);
        accountErase.setBorder(ClientApplication.BUTTON_BORDER);
        playing.setBorder(ClientApplication.BUTTON_BORDER);
        loading.setBorder(ClientApplication.BUTTON_BORDER);
        
        //Farbegebung der Buttons
        emailChange.setBackground(ClientApplication.BUTTON_COLOR);
        passwordChange.setBackground(ClientApplication.BUTTON_COLOR);
        accountErase.setBackground(ClientApplication.BUTTON_COLOR);
        playing.setBackground(ClientApplication.BUTTON_COLOR);
        loading.setBackground(ClientApplication.BUTTON_COLOR);
        
        //add tooltip to buttons
        emailChange.setToolTipText("Wollen Sie Email-adresse aendern?");
        passwordChange.setToolTipText("Wollen Sie Ihre Password neu definieren?");
        accountErase.setToolTipText("Wollen Sie diese Nickname nicht mehr?");
        playing.setToolTipText("Wollen Sie jetzt spielen?");
        loading.setToolTipText("Welches gespeicherte Spiel wollen sie jetzt laden?");
        
        //add ActionListener to buttons
        emailChange.addActionListener(this);
        passwordChange.addActionListener(this);
        accountErase.addActionListener(this);
        playing.addActionListener(this);
        loading.addActionListener(this);
        
        //create Table
		userRulesTableModel = new UserRulesTableModel();
        rulesTable = new JTable(userRulesTableModel);
		rulesTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		rulesTable.getTableHeader().setFont(new Font("Arial",Font.BOLD,10));
		rulesTable.getTableHeader().setBackground(ClientApplication.TABLE_COLOR);
		rulesTable.getTableHeader().setForeground(ClientApplication.FOREGROUND_COLOR);
		rulesTable.getTableHeader().setBorder(ClientApplication.TABLE_BORDER);
		rulesTableScrollPane = new JScrollPane(rulesTable,
														JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
														JScrollPane.HORIZONTAL_SCROLLBAR_NEVER
														);
		rulesTableScrollPane.setBounds(10,400,240,150);
		rulesTableScrollPane.setBorder(BorderFactory.createLineBorder(Color.BLACK,0));
		
		loadGameTableModel = new LoadGameTableModel();
		loadGameTable = new JTable(loadGameTableModel);
		loadGameTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		loadGameTable.getTableHeader().setFont(new Font("Arial",Font.BOLD,10));
		loadGameTable.getTableHeader().setBackground(ClientApplication.TABLE_COLOR);
		loadGameTable.getTableHeader().setForeground(ClientApplication.FOREGROUND_COLOR);
		loadGameTable.getTableHeader().setBorder(ClientApplication.TABLE_BORDER);
		loadGameTableScrollPane = new JScrollPane(loadGameTable,
														JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
														JScrollPane.HORIZONTAL_SCROLLBAR_NEVER
														);
		loadGameTableScrollPane.setBounds(400,400,240,150);	
		loadGameTableScrollPane.setBorder(BorderFactory.createLineBorder(Color.BLACK,0));											
		
		userPanel.add(loadGameTableScrollPane);												
		userPanel.add(rulesTableScrollPane);
		userPanel.add(myProfil);
		userPanel.add(playOption);
		userPanel.add(email);
		userPanel.add(emailname);
		userPanel.add(emailChange);
		userPanel.add(passwordChange);
		userPanel.add(accountErase);
		userPanel.add(playing);
		userPanel.add(loading);
        
		cp.add(userPanel);		
	}
        private void showUserData(){
            System.out.println("UserData");
        }
        private void userDatachange(){
            JPasswordField jPasswordField1 = new JPasswordField();
            JPasswordField jPasswordField2 = new JPasswordField(); 

            JPanel jNameQuestion1 = new JPanel();
            jNameQuestion1.setLayout(new BoxLayout(jNameQuestion1, BoxLayout.Y_AXIS));

            jNameQuestion1.add(new JLabel("neu Passwort:"));
            jNameQuestion1.add(jPasswordField1);
            jNameQuestion1.add(new JLabel("Bestaetigung:"));
            jNameQuestion1.add(jPasswordField2);

            JOptionPane.showMessageDialog (null,jNameQuestion1,"UserDataChange",JOptionPane.QUESTION_MESSAGE);
            if(jPasswordField1.getText().equals(jPasswordField2.getText())){
            	JOptionPane.showConfirmDialog(null, "Wollen Sie wirklich dein Passwort aendern?", "", JOptionPane.YES_NO_OPTION);
            }
            else{                    
              	JOptionPane.showMessageDialog(null, "Die Passwort stimmt nicht ueberein", "Error", JOptionPane.ERROR_MESSAGE);
            }    
        }
        

        public void actionPerformed(ActionEvent e) {
            String cmd = e.getActionCommand();
            System.out.println(cmd);
            if(cmd.equals("Aendern")){
				System.out.println("Aendern");
            }
            else if(cmd.equals("PasswortAenderung")){
                userDatachange();
            }
            else if(cmd.equals("Accountloeschen")){
                 System.out.println("Accountloeschen");
            }
            else if(cmd.equals("spielen")){
                 System.out.println("spielen");
            }
            else if(cmd.equals("Logout")){
            	System.out.println("Logout");
            	}
            else{
				System.out.println("laden");
            }
            
        }
        
}
