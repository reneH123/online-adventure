/*
 * Created on 12.01.2004
 */
package adventure.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Iterator;
import java.util.List;

import adventure.xalimporter.xalobjects.ActLook;
import adventure.xalimporter.xalobjects.ActTake;
import adventure.xalimporter.xalobjects.ActTalk;
import adventure.xalimporter.xalobjects.ActUse;
import adventure.xalimporter.xalobjects.DialogOption;
import adventure.xalimporter.xalobjects.Item;
import adventure.xalimporter.xalobjects.Scene;
import adventure.xalimporter.xalobjects.Xal;

public class XALImportHelper {

	//	Repr�sentiert das Regelwerk
	private Xal xal;
	private Statement stmt;
	private ResultSet rs;
	private Connection con;
	private ConnectionData condata;

	private boolean init() {
		try {
			Class.forName(condata.getDriverPath());
			con =
				DriverManager.getConnection(
					condata.getURL(),
					condata.getUser(),
					condata.getPassword());
			stmt = con.createStatement();
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * 
	 */
	public XALImportHelper(Xal xal) { //@Ulf: Constructor anpassen ConnectionData condata) {
		// @rene: w�rd ich ja gern machen, aber woher bekomme ich die ConnectionData???
		// @Ulf: ConnectionData ist eine Klasse von adventure.persistence, schau am besten mal in ServerApplication, dort steht wie man sie benutzt
		//this.condata = condata;
		try {
			if (init() == false)
				throw new Exception("Failure in connection to database in XALImportHelper");
			con.setAutoCommit(false);

			// autocommit ausschalten		
			this.xal = xal;

			/**
			 *   `rolesetID` int(10) unsigned NOT NULL auto_increment,
			`setname`  varchar(20),
			`locationid` varchar(20) NOT NULL,
			`state` char(1) binary NOT NULL default '0',
			 */
			int rolesetID = 0;
			stmt.executeUpdate(
				"INSERT INTO rolesets VALUES(null,'"
					+ xal.getRoleSetName()
					+ "',"
					+ ((Scene) xal.getSceneList().get(0)).getID()
					+ ",1);");
			rs = stmt.executeQuery("SELECT LAST_INSERT_ID() AS lastID;");
			rolesetID = rs.getInt("lastID");
			List list = xal.getDialogList();
			Iterator it, it2, it3;
			it = list.iterator();
			DialogOption dialogoption;
			while (it.hasNext()) {
				dialogoption = (DialogOption) it.next();
				stmt.executeUpdate(
					"INSERT INTO dialogoptions VALUES(null,"
						+ rolesetID
						+ ","
						+ dialogoption.getID()
						+ ",'"
						+ dialogoption.getText()
						+ "','"
						+ dialogoption.getActionScript()
						+ "');");
			}
			list = xal.getSceneList();
			it = list.iterator();
			Scene scene;
			Item item;
			int itemID;
			int locationID;
			ActLook actlook;
			ActTake acttake;
			ActTalk acttalk;
			ActUse actuse;
			while (it.hasNext()) {
				scene = (Scene) it.next();
				stmt.executeUpdate(
					"INSERT INTO locations VALUES(null,"
						+ scene.getID()
						+ ",'"
						+ xal.getRoleSetName()
						+ "',"
						+ rolesetID
						+ ");");
				rs = stmt.executeQuery("SELECT LAST_INSERT_ID() AS lastID;");
				locationID = rs.getInt("lastID");
				it2 = scene.getItemList().iterator();

				while (it2.hasNext()) {
					item = (Item) it2.next();
					stmt.executeUpdate(
						"INSERT INTO items VALUES (null,"
							+ item.getID()
							+ ","
							+ locationID
							+ ",'"
							+ item.getDescription()
							+ "','"
							+ item.getImagePath()
							+ "',"
							+ item.getPosX()
							+ ","
							+ item.getPosY()
							+ ","
							+ item.getVisible()
							+ ","
							+ item.getLevel()
							+ ");");
					rs =
						stmt.executeQuery("SELECT LAST_INSERT_ID() AS lastID;");
					itemID = rs.getInt("lastID");
					it3 = item.getActLookList().iterator();
					while (it3.hasNext()) {
						actlook = (ActLook) it3.next();
						stmt.executeUpdate(
							"INSERT INTO itemactions VALUES(null,"
								+ itemID
								+ ",null,'"
								+ actlook.getActionScript()
								+ "',"
								+ adventure.gameobjects.Item.ACTION_LOOK
								+ ");");
					}

					it3 = item.getActTakeList().iterator();
					while (it3.hasNext()) {
						acttake = (ActTake) it3.next();
						stmt.executeUpdate(
							"INSERT INTO itemactions VALUES(null,"
								+ itemID
								+ ",null,'"
								+ acttake.getActionScript()
								+ "',"
								+ adventure.gameobjects.Item.ACTION_TAKE
								+ ");");
					}
					it3 = item.getActTalkList().iterator();
					while (it3.hasNext()) {
						acttalk = (ActTalk) it3.next();
						stmt.executeUpdate(
							"INSERT INTO itemactions VALUES(null,"
								+ itemID
								+ ",null,'"
								+ acttalk.getActionScript()
								+ "',"
								+ adventure.gameobjects.Item.ACTION_TALK
								+ ");");
					}

					it3 = item.getActUseList().iterator();
					while (it3.hasNext()) {
						actuse = (ActUse) it3.next();
						stmt.executeUpdate(
							"INSERT INTO itemactions VALUES(null,"
								+ itemID
								+ ",'"
								+ actuse.getTargetItem()
								+ "','"
								+ actuse.getActionScript()
								+ "',"
								+ adventure.gameobjects.Item.ACTION_USE
								+ ");");
					}
				}
			}
			con.commit();
			con.setAutoCommit(true);
		} catch (Exception exception) {
			try {
				con.rollback();
				con.setAutoCommit(true);
			} catch (Exception exception2) {
				System.out.println(
					"Failure in setting autocommit in XALImportHelper");
			}
		}
	}
}

