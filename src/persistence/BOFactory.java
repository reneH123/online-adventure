package adventure.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;

import adventure.gameobjects.DialogOption;
import adventure.gameobjects.Item;
import adventure.gameobjects.Location;
import adventure.gameobjects.Scene;
import adventure.server.Game;

/**
 * @author René Haberland
 */
public class BOFactory {

	private ConnectionData conData;
	private Connection con;
	private Statement stmt;
	private ResultSet rs, rs2;

	/**
	 * Constructor esablishs Connection to Database
	 *
	 * @param driver
	 * @param database
	 * @param driverPath
	 * @param passwd
	 * @param port
	 * @param username
	 */
	public BOFactory(
	    String driverPath,
	    String host,
	    String port,
		String database,
		String username,
		String passwd
		)
		throws Exception {

		try {
			conData = new ConnectionData();
			conData.setDriverPath(driverPath);
			conData.setHost(host);
			conData.setPort(port);
			conData.setDatabaseName(database);
			conData.setUser(username);
			conData.setPassword(passwd);
			con = initConnection(conData);
		} catch (Exception e) {
			throw new Exception("Could not Connect to Database in SimpleQueryTool");
		}
	}

	public BOFactory(ConnectionData conData) throws Exception {
		this.conData = conData;
		try {
			con = initConnection(conData);
		} catch (Exception e) {
			throw new Exception("Could not Connect to Database");
		}
	}

	private Connection initConnection(ConnectionData cd) throws Exception {
		Connection c = null;
		try {
			Class.forName(cd.getDriverPath()).newInstance();
			c =
				DriverManager.getConnection(
					cd.getURL(),
					cd.getUser(),
					cd.getPassword());
			stmt = c.createStatement();
		} catch (Exception e) {
			throw new Exception("Could not get connection in initConnection");
		}
		return c;
	}

	protected void finalize() throws Throwable {
		try {
			con.close();
		} finally {
			super.finalize();
		}
	}

	/**
	 * Get the Dynamic Gamedata of a Savegame
	 *
	 * @param game the Game
	 * @param String the SaveGameName
	 * @return Users Gamedata
	 */

	public void loadGame(Game game, String saveGameName) throws Exception {
		int rulesetid;
		int currentlocationid;
		int savegameID;
		int userid;
		Hashtable dlgopt = new Hashtable();
		Hashtable flags = new Hashtable();
		LinkedList invent = new LinkedList();
		LinkedList loc = new LinkedList();
		LinkedList scenes = new LinkedList();
		try {

			userid = game.getUser().getId();
			//ruleset zum Savegamename und userid finden
			rs =
				stmt.executeQuery(
					"SELECT * FROM users u,savegames s,rolesets r "
						+ "WHERE (savegamename='"
						+ saveGameName
						+ "')AND(s.rolesetID=r.rolesetID)"
						+ "(AND u.userID="
						+ userid
						+ ");");
			if (rs.first()) {
				//rulesetname im game setzen
				game.setRuleSetName(rs.getString("setname"));
			} else {
				throw new Exception("Could not find SaveGame in loadGame");
			}
			rulesetid = rs.getInt("s.rolesetID");
			currentlocationid = rs.getInt("s.currentlocation");
			savegameID = rs.getInt("savegameID");

			//dialogoptions fuer ruleset laden
			rs =
				stmt.executeQuery(
					"SELECT * FROM dialogoptions WHERE rolesetID="
						+ rulesetid
						+ ";");
			while (rs.next()) {
				DialogOption dlg =
					new DialogOption(
						rs.getInt("dialogoptionID"),
						rs.getString("text"),
						rs.getString("actionscript").split("#"));
				dlgopt.put(new Integer(rs.getInt("dialogoptionID")), dlg);
			}

			//alle locations finden die zum ruleset gehoeren
			rs =
				stmt.executeQuery(
					"SELECT * FROM locations WHERE rolesetID="
						+ rulesetid
						+ ";");
			while (rs.next()) {
				loc.add(
					new Location(
						rs.getInt("locationID"),
						rs.getString("locationname")));
			}

			Iterator it = loc.iterator();
			while (it.hasNext()) {
				Location temp = (Location) it.next();

				//alle items die zu einer location des rulesets gehoeren finden
				rs =
					stmt.executeQuery(
						"SELECT * FROM items WHERE locationID="
							+ temp.getID()
							+ ";");
				LinkedList items = new LinkedList();
				while (rs.next()) {
					//alle itemactions fuer ein spezifisches item finden
					rs2 =
						stmt.executeQuery(
							"SELECT * FROM itemactions WHERE itemID="
								+ rs.getInt("itemID")
								+ ";");
					String[] at = null, al = null, ata = null;
					while (rs2.next()) {
						switch (rs2.getInt("actiontype")) {
							case Item.ACTION_LOOK :
								al = rs2.getString("actionscript").split("#");
								break;
							case Item.ACTION_TAKE :
								at = rs2.getString("actionscript").split("#");
								break;
							case Item.ACTION_TALK :
								ata = rs2.getString("actionscript").split("#");
								break;
						}

					}
					Item item =
						new Item(
							rs.getInt("itemID"),
							rs.getString("itemname"),
							rs.getString("description"),
							rs.getString("imagepath"),
							rs.getInt("positionx"),
							rs.getInt("positiony"),
							at,
							ata,
							al,
							rs.getInt("level"),
							false);
					items.add(item);
					rs2.beforeFirst();
					while (rs2.next()) {
						if (rs2.getInt("actiontype") == Item.ACTION_USE) {
							item.addUseAction(
								rs2.getString("itempartner"),
								rs2.getString("actionscript").split("#"));
						}

					}

				}
				temp.setListOfItems(items);
			}
			//alle Items des inventories
			rs =
				stmt.executeQuery(
					"SELECT * FROM inventory WHERE savegameID="
						+ savegameID
						+ ";");
			//alle items die zur Scene gehoeren und auf visible gesetzt werden muessen
			rs2 =
				stmt.executeQuery(
					"SELECT * FROM scenes WHERE savegameID="
						+ savegameID
						+ ";");
			Iterator it1 = loc.iterator();
			Iterator it2;
			while (it.hasNext()) {
				it2 = ((Location) it.next()).getListOfItems().iterator();
				while (it2.hasNext()) {
					Item item = (Item) it2.next();
					while (rs.next()) {
						if (rs.getInt("itemID") == item.getId()) {
							//sind inventory items auch sichtbar???
							invent.add(item);
						}
					}
					rs.beforeFirst();
					while (rs2.next()) {
						if (rs.getInt("itemID") == item.getId()) {
							item.setVisible(true);
						}
					}
					rs2.beforeFirst();
				}
			}

			//alle flags mit zugehoerigen werten holen
			rs =
				stmt.executeQuery(
					"SELECT * FROM flags WHERE savegameID=" + savegameID + ";");
			while (rs.next()) {
				flags.put(
					rs.getString("flagname"),
					new Boolean(rs.getBoolean("value")));
			}

			//erzeuge Scene Objekts und setze currentScene des game objekts
			it1 = loc.iterator();
			while (it1.hasNext()) {
				Location temp = (Location) it.next();
				Scene t2 = new Scene(temp);
				if (currentlocationid == temp.getID())
					game.setCurrentScene(t2);
				scenes.add(t2);
			}

			//setze alle restlichen daten des gameobjekts
			game.setInventory(invent);
			game.setScenes(scenes);
			game.setDialogOptions(dlgopt);
			game.setFlags(flags);
			game.setId(savegameID);
			//game.setUser(); //soll der User hier nochmal gesetzt werden?
			// Variablen statt Werteparameter game!	

		} catch (Exception e) {
			throw new Exception("Could not load SaveGame");
		}
	}
}

