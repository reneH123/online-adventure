package adventure.persistence;
/**
 * helper class for construction the right URL-path, user name and password
 * @author René Haberland
 * 
 * old definition of an URL:  DriverPath+":"+port+"/"+databaseName
 * new definition of an URL:  DriverPath+"://"+host+":"+port+"/"+databaseName
 * 
 * Example:
 * 
 * ConnectionData condata;
 * condata.setDriverPath("jdbc:mysql");
 * condata.setHost("localhost");
 * condata.setPort("3306");
 * condata.setDatabaseName("adventure");  
 * System.out.println("current URL is: "+condata.getURL());
 * 
 * Console: current URL is: jdbc:mysql://localhost:3306/adventure
 */

public class ConnectionData {
	private String user;
	private String databaseName;
	private String password;
	private String host;
	private String port;
	private String DriverPath;
	/**
	 * url is compounding string
	 */
	private String URL;
	/**
	 * @return whole URL including driverpath,host,port,databasename
	 * example: "jdbc:mysql://localhost:3306/adventure"
	 * */
	public String getURL() {
		return URL;
	}
	/**
	 * @return databaseName
	 * example: "adventure"
	 */
	public String getDatabaseName() {
		return databaseName;
	}
	/**
	 * @return DriverPath
	 * example: "jdbc:mysql"
	 */
	public String getDriverPath() {
		return DriverPath;
	}
	/**
	 * @return password
	 * example: "blah"
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @return port
	 * example: "3306"
	 */
	public String getPort() {
		return port;
	}
	/**
	 * @return user
	 * example: "user1"
	 */
	public String getUser() {
		return user;
	}
	/**
	 * @return host
	 * example: "localhost"
	 */
	public String getHost() {
		return host;
	}
	/**
	 * @param String databaseName
	 * example: DatabaseName="adventure";
	 *  => URL=="jdbc:mysql://localhost:3306/adventure"
	 */
	public void setDatabaseName(String DatabaseName) {
		this.databaseName = DatabaseName;
		URL = DriverPath + "://" + host + ":" + port + "/" + DatabaseName;
	}
	/**
	 * @param path of JDBC driver
	 * example: driverpath="jdbc:mysql";
	 * => URL=="jdbc:mysql";
	 */
	public void setDriverPath(String driverpath) {
		DriverPath = driverpath;
		URL = DriverPath;
	}
	/**
	 * @param users` password to enter database
	 * example: password="blah";
	 * => URL keeps like it is
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @param mySQL port number as String
	 * example: port="3306";
	 * => URL=="jdbc:mysql://localhost:3306"
	 */
	public void setPort(String port) {
		this.port = port;
		URL = DriverPath + "://" + host + ":" + port;
	}
	/**
	 * @param user of the database
	 * example: user="user1";
	 * => URL keeps like it is
	 */
	public void setUser(String user) {
		this.user = user;
	}
	/**
	 * @param name of host
	 *  example: host="localhost";
	 * =>URL=="jdbc:mysql://localhost"  
	 */
	public void setHost(String host) {
		this.host = host;
		URL = DriverPath + "://" + host;
	}
}

