package adventure.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import adventure.gameobjects.Admin;
import adventure.gameobjects.Player;
import adventure.gameobjects.RuleSet;
import adventure.gameobjects.User;

/**
 * Class to handle non Game specific Database Requests
 *
 * @author Ren� Haberland
 */
public class SimpleQueryTool {

	private ConnectionData conData;
	private Connection con;
	private Statement stmt;
	private ResultSet rs;

	/**
	 * Constructor establishes Database Connection
	 * @param driver
	 * @param database
	 * @param driverPath
	 * @param passwd
	 * @param port
	 * @param username
	 */
	public SimpleQueryTool(
	    String driverPath,
		String host,
		String port,
		String database,
		String username,
		String passwd)
		throws Exception {

		try {
			conData = new ConnectionData();
			conData.setDriverPath(driverPath);
			conData.setHost(host);
			conData.setPort(port);
			conData.setDatabaseName(database);
			conData.setUser(username);
			conData.setPassword(passwd);
			con = initConnection(conData);
		} catch (Exception e) {
			throw new Exception("Could not Connect to Database in SimpleQueryTool");
		}
	}

	public SimpleQueryTool(ConnectionData conData) throws Exception {
		this.conData = conData;
		try {
			con = initConnection(conData);
		} catch (Exception e) {
			throw new Exception("Could not Connect to Database");
		}
	}

	private Connection initConnection(ConnectionData cd) throws Exception {
		Connection c = null;
		try {
			Class.forName(cd.getDriverPath());
			c =
				DriverManager.getConnection(
					cd.getURL(),
					cd.getUser(),
					cd.getPassword());
			stmt = c.createStatement();
		} catch (Exception e) {
			throw new Exception("Could not get connection in initConnection");
		}
		return c;
	}

	protected void finalize() throws Throwable {
		try {
			con.close();
		} finally {
			super.finalize();
		}
	}

	/**
	 * Get a List of RuleSets from the DB
	 * @return Linked List of RuleSets
	 */
	public List getRules() throws Exception {
		LinkedList rules = new LinkedList();
		try {
			rs = stmt.executeQuery("SELECT * FROM RuleSet;");
			while (rs.next()) {
				rules.add(
					new RuleSet(
						rs.getInt("ruleSetID"),
						rs.getString("ruleSetName"),
						rs.getString("autor"),
						rs.getBoolean("state")));
			}
		} catch (Exception e) {
			throw new Exception("Could not load Rulesets from Database in getRules");
		}
		return rules;
	}

	/**
	 * Delete a RuleSet and all Savegames that belong to the RuleSet
	 * @param ruleSet Delete this RuleSet
	 * @return true if succsesfull, false else
	 */
	public void delRules(RuleSet ruleSet) throws Exception {
		HashSet locations = new HashSet();
		HashSet items = new HashSet();
		HashSet savegames = new HashSet();
		try {
			rs =
				stmt.executeQuery(
					"SELECT * FROM Locations WHERE ruleSetID="
						+ ruleSet.getRuleSetID()
						+ ";");
			while (rs.next()) {
				locations.add(new Integer(rs.getInt("locationID")));
			}
			Iterator it = locations.iterator();
			while (it.hasNext()) {
				rs =
					stmt.executeQuery(
						"SELECT * FROM Items WHERE locationID="
							+ (Integer) it.next()
							+ ";");
				while (rs.next()) {
					items.add(new Integer(rs.getInt("itemID")));
				}
			}
			it = items.iterator();
			while (it.hasNext()) {
				rs =
					stmt.executeQuery(
						"SELECT * FROM Scenes WHERE itemID="
							+ (Integer) it.next()
							+ ";");
				while (rs.next()) {
					savegames.add(new Integer(rs.getInt("saveGameID")));
				}
				rs =
					stmt.executeQuery(
						"SELECT * FROM Inventory WHERE itemID="
							+ (Integer) it.next()
							+ ";");
				while (rs.next()) {
					savegames.add(new Integer(rs.getInt("saveGameID")));
				}
			}

			it = savegames.iterator();
			while (it.hasNext()) {
				stmt.executeQuery(
					"DELETE FROM Flags WHERE saveGameID="
						+ (Integer) it.next()
						+ ";");
				stmt.executeQuery(
					"DELETE FROM SaveGames WHERE saveGameID="
						+ (Integer) it.next()
						+ ";");
				stmt.executeQuery(
					"DELETE FROM Inventory WHERE saveGameID="
						+ (Integer) it.next()
						+ ";");
				stmt.executeQuery(
					"DELETE FROM Scenes WHERE saveGameID="
						+ (Integer) it.next()
						+ ";");
			}
			it = items.iterator();
			while (it.hasNext()) {
				stmt.executeQuery(
					"DELETE FROM ItemActions WHERE itemID="
						+ (Integer) it.next()
						+ ";");
				stmt.executeQuery(
					"DELETE FROM Items WHERE itemID="
						+ (Integer) it.next()
						+ ";");
			}
			it = locations.iterator();
			while (it.hasNext()) {
				stmt.executeQuery(
					"DELETE FROM Locations WHERE locationID="
						+ (Integer) it.next()
						+ ";");
			}
			stmt.executeQuery(
				"DELETE FROM DialogOptions WHERE ruleSetID="
					+ ruleSet.getRuleSetID()
					+ ";");
			stmt.executeQuery(
				"DELETE FROM RuleSets WHERE ruleSetID="
					+ ruleSet.getRuleSetID()
					+ ";");
		} catch (Exception e) {
			throw new Exception("Could not Delete RuleSet from Database in delRules");
		}
	}

	/**
	 * update a RuleSet. Name or State.
	 * @param ruleSet RuleSet to update
	 * @return true if succsesfull, false else
	 */
	public void updateRuleState(RuleSet ruleSet) throws Exception {
		try {
			stmt.executeUpdate(
				"UPDATE RuleSets SET "
					+ "state="
					+ ruleSet.getState()
					+ ", ruleSetName='"
					+ ruleSet.getRuleSetName()
					+ "' WHERE ruleSetID="
					+ ruleSet.getRuleSetID()
					+ ";");
		} catch (Exception e) {
			throw new Exception("Could not update RuleSet in updateRuleState");
		}
	}

	/**
	 * Get a User from the Database by his Username and Password for Login Procedure
	 * Checks also for a Valid User
	 *
	 * @param Username Users username
	 * @param Password Users Password
	 * @return User or NULL if the given Username or Password was incorrect
	 */
	public User getUser(String Username, String Password) throws Exception {
		User user;
		try {
			rs =
				stmt.executeQuery(
					"SELECT * FROM Users WHERE (username='"
						+ Username
						+ "') AND (password='"
						+ Password
						+ "');");
			if (rs.first()) {
				if (rs.getBoolean("state")) {
					user =
						new Admin(
							rs.getString("username"),
							rs.getString("firstname"),
							rs.getString("nickname"),
							rs.getString("email"),
							rs.getString("password"),
							rs.getInt("userID"),
							User.USER_ONLINE);
				} else {
					user =
						new Player(
							rs.getString("username"),
							rs.getString("firstname"),
							rs.getString("nickname"),
							rs.getString("email"),
							rs.getString("password"),
							rs.getInt("userID"),
							User.USER_ONLINE);
				}
			} else {
				throw new Exception("Username or Password wrong");
			}
			return user;
		} catch (Exception e) {
			throw new Exception("Error connecting to Database in getUser");
		}

	}

	/**
	 * Get a List of all known Users
	 * @return A Linked List with all Users
	 */
	public List getUserList() throws Exception {
		LinkedList users = new LinkedList();
		try {
			rs = stmt.executeQuery("SELECT * FROM Users;");
			while (rs.next()) {
				if (rs.getBoolean("state")) {
					users.add(
						new Admin(
							rs.getString("username"),
							rs.getString("firstname"),
							rs.getString("nickname"),
							rs.getString("email"),
							rs.getString("password"),
							rs.getInt("userID"),
							User.USER_OFFLINE));
				} else {
					users.add(
						new Player(
							rs.getString("username"),
							rs.getString("firstname"),
							rs.getString("nickname"),
							rs.getString("email"),
							rs.getString("password"),
							rs.getInt("userID"),
							User.USER_OFFLINE));
				}
			}
		} catch (Exception e) {
			throw new Exception("Could not get UserList from Database in getUserList");
		}
		return users;
	}

	/**
	 * Delete a User from DB
	 * @param user the user to delete
	 * @return true if succsesfull, false else
	 */
	public void deleteUser(User user) throws Exception {
		LinkedList savegames = new LinkedList();
		try {
			rs =
				stmt.executeQuery(
					"SELECT * FROM SaveGames WHERE userID="
						+ user.getID()
						+ ";");

			while (rs.next()) {
				deleteSaveGame(rs.getInt("saveGameID"));
			}
			stmt.executeUpdate(
				"DELETE FROM Users WHERE userID=" + user.getID() + ";");
		} catch (Exception e) {
			throw new Exception("Could not delete User in deleteUser");
		}
	}

	/**
	 * Register a new User in the DB
	 * @param user the User to register
	 */
	public void registerUser(User user) throws Exception {
		try {
			int isAdmin = user.getIsAdmin()?1:0;
			stmt.executeUpdate(
				"INSERT INTO users VALUES (null,'"
					+ user.getUserName()
					+ "','"
					+ user.getUserFirstName()
					+ "','"
					+ user.getUserNickName()
					+ "','"
					+ user.getEmail()
					+ "','"
					+ user.getPassWD()
					+ "',"
					+ isAdmin
					+ ");");
			rs = stmt.executeQuery("SELECT LAST_INSERT_ID() AS userID;");
			// evtl noch user.setID(rs.getInt("userID")); zum setzen der User id des users (ansonsten user objekt wegwerfen und neu ueber getUser holen)
		} catch (Exception e) {
			throw new Exception("Could not register User in registerUser (maybe User exists)");
		}
	}

	/**
	 * Update a User
	 * @param user User with updated Properties
	 * @return true if succsesfull, false else
	 */
	public void updateUser(User user) throws Exception {
		try {
			int isAdmin = user.getIsAdmin()?1:0;
			stmt.executeUpdate(
				"UPDATE users SET "
					+ "username='"
					+ user.getUserName()
					+ "', firstname='"
					+ user.getUserFirstName()
					+ "', nickname='"
					+ user.getUserNickName()
					+ "', email='"
					+ user.getEmail()
					+ "', password='"
					+ user.getPassWD()
					+ "', state="
					+ isAdmin
					+ " WHERE userID="
					+ user.getID()
					+ ";");
		} catch (Exception e) {
			throw new Exception("Could not update User in Database in updateUser");
		}
	}

	/**
	 * Get a list of all Savegames of per user and ruleset
	 *
	 * @param User Users 
	 * @param RuleSet Ruleset of the List of SaveGames
	 * @return Map consisting of the savegameIDs as keys and Savegame Names as values
	 *
	 */
	public Map getSaveList(User User, RuleSet RuleSet) throws Exception {
		try {
			Map saveMap = new HashMap();
			rs =
				stmt.executeQuery(
					"SELECT savegameName FROM Savegames WHERE (userID="
						+ User.getID()
						+ ") AND (gameID="
						+ RuleSet.getRuleSetID()
						+ ");");
			Integer i;
			while (rs.next()) {
				i = new Integer(rs.getInt("savegameID"));
				saveMap.put(i, rs.getString("savegameName"));
			}
			return saveMap;
		} catch (Exception e) {
			throw new Exception("Could not get List of SaveGames from Database in getSaveList");
		}
	}

	/**
	 * Delete a SaveGame from DB
	 * @param saveGameID ID of the SaveGame
	 * @return true if succsesfull, false else
	 */
	public void deleteSaveGame(int saveGameID) throws Exception {
		try {
			stmt.executeQuery(
				"DELETE FROM Scenes WHERE saveGameID=" + saveGameID + ";");
			stmt.executeQuery(
				"DELETE FROM Inventory WHERE saveGameID=" + saveGameID + ";");
			stmt.executeQuery(
				"DELETE FROM Flags WHERE saveGameID=" + saveGameID + ";");
			stmt.executeQuery(
				"DELETE FROM SaveGames WHERE saveGameID=" + saveGameID + ";");
		} catch (Exception e) {
			throw new Exception("Could not delete SaveGame in deleteSaveGame");
		}
	}

	public User getUser(String Username) throws Exception {
		User user;
		try {
			rs =
				stmt.executeQuery(
					"SELECT * FROM users WHERE (username='" + Username + "');");
			if (rs.first()) {
				if (rs.getBoolean("state")) {
					user =
						new Admin(
							rs.getString("username"),
							rs.getString("firstname"),
							rs.getString("nickname"),
							rs.getString("email"),
							rs.getString("password"),
							rs.getInt("userID"),
							User.USER_ONLINE);
				} else {
					user =
						new Player(
							rs.getString("username"),
							rs.getString("firstname"),
							rs.getString("nickname"),
							rs.getString("email"),
							rs.getString("password"),
							rs.getInt("userID"),
							User.USER_ONLINE);
				}
			} else {
				throw new Exception("Username or Password wrong");
			}
			return user;
		} catch (Exception e) {
			throw new Exception("Error connecting to Database in getUser");
		}
	}
}

