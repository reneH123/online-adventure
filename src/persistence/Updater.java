package adventure.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import adventure.gameobjects.Item;
import adventure.gameobjects.Location;
import adventure.gameobjects.Scene;
import adventure.server.Game;

/**
 * @author Ren� Haberland
 * @since 22.12.2003
 * @version 0.1
 * 
 */

public class Updater {
	private ConnectionData Connection;
	private Connection con;
	private ResultSet rs;
	private Statement stmt;

	/**
	 * save current game to database to 'slot' slot if possible
	 * @param gamedata
	 * @param slot
	 * @return error code true success false failure
	 * @exception Exception
	 */
	public void saveGame(Game game, String saveGameName) throws Exception {
		int savegameID, usedslots;
		Iterator it, it2;
		Integer integer;
		Scene scene;
		Location location;
		List list;
		Item item;
		Set flagnameSet;
		String flagname, locationname, userName;
		int value;
		int userID, roleSetID;

		try {
			// save userID for current game
			userID = game.getUser().getID();
			// save rolesetID for current game
			rs =
				stmt.executeQuery(
					"SELECT rolesetID FROM rolesets WHERE setname='"
						+ game.getRuleSetName()
						+ "';");
			rs.next();
			roleSetID = rs.getInt("rolesetID");
			userName = game.getUser().getUserName();
			// save savegameID for current game named `saveGameName`
			rs =
				stmt.executeQuery(
					"SELECT savegameID FROM savegames WHERE (savegamename='"
						+ saveGameName
						+ "')AND(userID="
						+ userID
						+ ");");
			if (rs.next()){
				savegameID = rs.getInt("savegameID");
				stmt.executeUpdate("DELETE FROM savegames WHERE savgameID="+savegameID+";");
			}	
			stmt.executeUpdate(
					"INSERT INTO savegames VALUES(null,'"
						+ saveGameName
						+ "',"
						+ userID
						+ ","
						+ roleSetID
						+ ","
						+ game.getCurrentScene().getLocation().getID()
						+ ",null);");
						rs=stmt.executeQuery("SELECT LAST_INSERT_ID() AS savegameID;");
				savegameID=rs.getInt("savegameID");
			// check whether first 10 SaveGames are free
			rs =
				stmt.executeQuery(
					"SELECT COUNT(userID) AS usedslots FROM savegames WHERE userID="
						+ userID
						+ ";");
			rs.next();
			usedslots = rs.getInt("usedslots");
			if (usedslots > 10)
				throw new Exception("to much savegames on " + saveGameName);

			/**
			 * puts new entries to table scenes by itemID of each item 
			 */
			stmt.executeUpdate(
				"DELETE FROM scenes WHERE savegameID=" + savegameID + ";");

			// (a) save scene list 
			it = game.getScenes().iterator();
			// for each scene of game
			while (it.hasNext()) {
				scene = (Scene) it.next();
				location = scene.getLocation();
				/** determine which items of location are visible
				 * @see scene.getLocation().getVisibleItems()
				 * getVisibleItems returns List of item-objects
				 */
				list = location.getVisibleItems();
				it2 = list.iterator();
				while (it2.hasNext()) {
					item = (Item) it2.next();

					/**
					 *  append new entries into table scenes: [sceneID,savegameID,itemID]
					 *  - sceneID will be calculated each loop by incrementing by 1
					 *  - savegameID once determined
					 *  - itemID will be determined by each item
					 */
					stmt.executeUpdate(
						"INSERT INTO scenes VALUES(null,"
							+ ","+savegameID+","+item.getId()+");");
				}
			}
			// (b) save list of flags
			flagnameSet = game.getFlags().keySet();
			stmt.executeUpdate("DELETE FROM flags WHERE savegameID="+savegameID+";");
			it = flagnameSet.iterator();
			while (it.hasNext()) {
				flagname = (String) it.next();
				value = ((Boolean) game.getFlags().get(flagname)).booleanValue()?1:0;
				
				// fetch value from set
				stmt.executeUpdate(
					"INSERT INTO flags VALUES(null,"+savegameID+",'"+flagname+"',"+value+");");
			}
			// (c) save inventory of savegame 
			// inventory describes a list of item objects
			/**
			 * append new entries [savegameName,itemID]
			 * savegameName keeps fix
			 * itemID will be determined by each item of inventory list
			 * 
			 * entries will be put ascending order by inventoryID
			 */
			it = game.getInventory().iterator();
			stmt.executeUpdate("DELETE FROM inventory WHERE savegameID="+savegameID+";");
			while (it.hasNext()) {
				item = (Item) it.next();
				stmt.executeUpdate(
					"INSERT INTO inventory VALUES(null,"+savegameID+","+item.getId()+");");
			}
		} catch (SQLException exception) {
			throw new Exception("Failture while excecuting SQL-command in SaveGame");
		} catch (Exception exception) {
			throw new Exception("Failure while saving the game in SaveGame");
		}
	}
	/**
	 * destructor, closes database connection
	 * @exception Exception 
	 */
	protected void finalize() throws Throwable {
		try {
			con.close();
		} finally {
			super.finalize();
		}
	}
	/**
	 * standard constructor initializes SQL connection
	 * 
	 * @param driver
	 * @param database
	 * @param driverPath
	 * @param passwd
	 * @param port
	 * @param username
	 * 
	 */
	public Updater(
		String Database,
		String DriverPath,
		String Passwd,
		String Host,
		String Port,
		String Username) {
		Connection = new ConnectionData();
		Connection.setDriverPath(DriverPath);
		Connection.setHost(Host);
		Connection.setPort(Port);
		Connection.setDatabaseName(Database);
		Connection.setUser(Username);
		Connection.setPassword(Passwd);
		try {
			Class.forName(Connection.getDriverPath());
			con =
				DriverManager.getConnection(
					Connection.getURL(),
					Connection.getUser(),
					Connection.getPassword());
			stmt = con.createStatement();
		} catch (Exception ex) {
		}
	}
	public Updater(ConnectionData conData) {
		this.Connection = conData;
		if (init() == false) {
			System.out.println(
				"Failure while getting connection to "
					+ Connection.getDatabaseName());
			System.out.println("->Empty instance!");
		}
	}
	private boolean init() {
		try {
			Class.forName(Connection.getDriverPath());
			con =
				DriverManager.getConnection(
					Connection.getURL(),
					Connection.getUser(),
					Connection.getPassword());
			stmt = con.createStatement();
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
		return true;
	}
}

