package adventure.gameobjects;

import java.io.Serializable;
import java.util.*;

public class Location extends GameObject implements Serializable {

    /**
     * List of items in this location
     */
    private List items = new LinkedList();

    /**
     * name of the location
     */
    private String locationName;


    /**
     * Constructor of location
     * @param ID
     * @param name
     */
    public Location(int ID, String name){

        this.ID = ID;
        this.locationName=name;
    }

    /**
     * sets the name of the location
     * @param name
     */
    public void setLocationName(String name){
        this.locationName=name;

    }

    /**
     *
     * @return name of the location
     */
    public String getLocationName(){
        return this.locationName;
    }

    /**
     * adds an item to the list of items
     * @param item
     */
    public void addItem(Item item){
        items.add(item);
    }

    /**
     * removes an item from the list of items
     * @param name name of the item
     */
    public void removeItem(String name){

        Iterator it = items.iterator();
        while(it.hasNext()){
            Item i = (Item)it.next();
            if (i.getName().equals(name)){items.remove(i);}
        }

    }

    /**
     *
     * @return the list of items
     */
    public List getListOfItems(){

        return items;
    }

    /**
     * sets the list of items
     * @param items
     */
    public void setListOfItems(List items){

        this.items=items;
    }

    /**
     *
     * @param name the name of the item
     * @return a specific item
     */
    public Item getItem(String name){
        Iterator it = items.iterator();
        while(it.hasNext()){
            Item i = (Item)it.next();
            if (i.getName().equals(name)){
                return i;
            }
        }
        return null;
    }

    /**
     *
     * @return the location's ID
     */
    public int getID(){
        return ID;
    }

    /**
     *
     * @return a list of visible items
     */
    public List getVisibleItems(){
        List l = new LinkedList();
        Iterator it = items.iterator();
        while(it.hasNext()){
            Item i = (Item)it.next();
            if (i.getVisibility()){
                l.add(i);
            }
        }
        return l;

    }

    /**
     * sets the visibility of an item
     * @param itemName name of the item
     * @param visible true if item should be visible, otherwise false
     */
    public void setItemVisible(String itemName, boolean visible){

        getItem(itemName).setVisible(visible);

    }

    public void addItem(String s, String s1) {
    }
}
