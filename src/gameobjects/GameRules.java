package adventure.gameobjects;

import java.util.LinkedList;

public class GameRules {
	private LinkedList items;
	private LinkedList locations;
	private LinkedList dialogOptions;
	private String resourceLink;

	/**
	 * @return
	 */
	public LinkedList getDialogOptions() {
		return dialogOptions;
	}

	/**
	 * @return
	 */
	public LinkedList getItems() {
		return items;
	}

	/**
	 * @return
	 */
	public LinkedList getLocations() {
		return locations;
	}

	/**
	 * @return
	 */
	public String getResourceLink() {
		return resourceLink;
	}

	/**
	 * @param list
	 */
	public void setDialogOptions(LinkedList list) {
		dialogOptions = list;
	}

	/**
	 * @param list
	 */
	public void setItems(LinkedList list) {
		items = list;
	}

	/**
	 * @param list
	 */
	public void setLocations(LinkedList list) {
		locations = list;
	}

	/**
	 * @param string
	 */
	public void setResourceLink(String string) {
		resourceLink = string;
	}

}
