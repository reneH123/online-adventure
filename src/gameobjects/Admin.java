/*
 * Created on Dec 15, 2003
 */
package adventure.gameobjects;

import java.io.Serializable;

public class Admin extends User implements Serializable {

  /**
   * cvonstructor of Admin
   * @param name
   * @param eMail
   * @param passWD
   * @param ID
   */
  public Admin(String name, String firstname, String nickname, String eMail, String passWD, int ID, int state){

        super(name, firstname, nickname, eMail, passWD, ID, state);
        isAdmin(true);
  }
}

