/*
* Created on Dec 15, 2003
*/
package adventure.gameobjects;

import java.io.Serializable;

public class User extends GameObject implements Serializable {

    public static final int USER_ONLINE = 1;
    public static final int USER_OFFLINE = 2;

    /**
     * user's firstname
     */
    private String firstname;

    /**
     * user's name
     */
    private String name;

    /**
     * user's nickname
     */
    private String nickname;

    /**
     * users's email
     */
    private String eMail;

    /**
     * user's password
     */
    private String passWD;

    /**
     * flag if user is an Admin
     */
    private boolean isAdmin;

    /**
     * is User online?
     */

    private int state;

    /**
     * constructor of User
     * @param name
     * @param eMail
     * @param passWD
     * @param ID
     */

    public User(String name, String firstname, String nickname, String eMail, String passWD, int ID, int state){

        this.ID = ID;
        this.firstname=firstname;
        this.name=name;
        this.nickname=nickname;
        this.eMail=eMail;
        this.passWD=passWD;
        this.state=state;
    }

    /**
     *
     * @return user's firstname
     */

    public String getUserFirstName(){
        return firstname;
    }

    /**
     * sets the user's firstname
     * @param firstname
     */
    public void setUserFirstName(String firstname){

        this.firstname=firstname;
    }

    /**
     *
     * @return user's name
     */
    public String getUserName() {
        return name;
    }

    /**
     * sets the user's name
     * @param name
     */
    public void setUserName(String name){
        this.name = name;
    }

    /**
     *
     * @return  user's nickname
     */
    public String getUserNickName(){
        return nickname;
    }

    /**
     * sets the user's nickname
     * @param nickname
     */
    public void setUserNickName(String nickname){

        this.nickname=nickname;
    }

    /**
     *
     * @return users's email
     */
    public String getEmail(){
        return eMail;
    }

    /**
     * sets the user's email
     * @param email
     */
    public void setEmail(String email){
        this.eMail=email;
    }

    /**
     * sets user's password
     * @param passWD
     */
    public void setPassWD(String passWD){
        this.passWD = passWD;
    }

    /**
     *
     * @return user's password
     */
    public String getPassWD(){
        return passWD;
    }

    /**
     *
     * @return user's ID
     */
    public int getID(){
        return ID;
    }

    /**
     * sets the admin flag
     * @param isAdmin
     */
    public void isAdmin(boolean isAdmin){
        this.isAdmin = isAdmin;
    }

    /**
     * @return true if it is an admin, otherwise false
     */
    public boolean getIsAdmin(){
        return isAdmin;
    }

    /**
     * gets the Users State
     * @return Users state
     */
    public int getState() {
        return state;
    }

    /**
     * Sets the users State
     * @param i new State
     */
    public void setState(int i) {
        state = i;
    }

}

