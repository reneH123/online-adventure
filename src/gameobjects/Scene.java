/*
* Created on Dec 15, 2003
*/
package adventure.gameobjects;

import java.io.Serializable;
import java.util.*;

public class Scene implements Serializable {

    private Location location;
//private DialogOption dialogOption;
    private List dialogOptionsToShow = new LinkedList();
    private String message;
    private String statusBarMessage;
    private List inventory;

    public List getInventory() {
        return inventory;
    }

    /**
     * Constructor for Scene
     * @param loc Location of the Scene
     */
    public Scene(Location loc){
        this.location=loc;

    }

    /**
     * sets the message
     * @param message
     */
    public void showMessage(String message) {
        this.message=message;
    }

    /**
     *
     * @return the message
     */
    public String getMessage(){
        return message;
    }

    /**
     * sets the location of the scene
     * @param location
     */
    public void setLocation(Location location){
        this.location=location;
    }

    /**
     *
     * @return the location
     */
    public Location getLocation(){
        return location;
    }

    /**
     * sets the statusBarMessage
     * @param statusBarMessage
     */
    public void setStatusBarMessage(String statusBarMessage){
        this.statusBarMessage=statusBarMessage;
    }

    /**
     *
     * @return the statusBarMessage
     */
    public String getStatusBarMessage(){
        return statusBarMessage;
    }

    /**
     * sets the list of dialogOptionsToShow
     * @param dialogOptionsToShow
     */
    public void setDialogOptionsToShow(List dialogOptionsToShow){
        this.dialogOptionsToShow=dialogOptionsToShow;
    }

    /**
     *
     * @return the dialogOptionsToShow
     */
    public List getDialogOptionsToShow(){
        return dialogOptionsToShow;
    }

    public void addDialogOption(DialogOption dialogOption) {
        dialogOptionsToShow.add(dialogOption);
    }

    public void removeDialogOption(DialogOption dialogOption) {
        dialogOptionsToShow.remove(dialogOption);
    }

    public List getVisibleItems() {
        return location.getVisibleItems();
    }

    public void setInventory(List inventory) {
        this.inventory = inventory;
    }
}

