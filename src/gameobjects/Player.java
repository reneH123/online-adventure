package adventure.gameobjects;

import java.io.Serializable;

public class Player extends User implements Serializable {

  /**
   * constructor of Palyer
   * @param name
   * @param eMail
   * @param passWD
   * @param ID
   */
  public Player(String name, String firstname, String nickname, String eMail, String passWD, int ID, int state){

        super(name, firstname, nickname, eMail, passWD, ID, state);
        isAdmin(false);
  }

}
