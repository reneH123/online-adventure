package adventure.gameobjects;

import java.io.Serializable;

public class GameObject implements Serializable {

 private int ID;

 /**
  * constructor of GameObject
  * @param ID of the GaneObject
  */

  public GameObject(int ID) {

    this.ID=ID;
  }

    public GameObject() {
	this.ID = -1;
    }

    public int getId() {
        return ID;
    }
}
