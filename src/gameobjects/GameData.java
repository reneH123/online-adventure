/*
 * Created on Dec 15, 2003
 */
package adventure.gameobjects;

import java.util.*;

public class GameData {

	private String[] returnText;
	private HashMap locationData;
	private HashMap dialogData;
	private List Location;
	private Location act;
	private List inventory;

	public GameData() {

	}

	public HashMap getLocationData() {
		return null;
	}

	public String[] getReturnText() {
		return null;
	}

	public HashMap getDialogData() {
		return null;
	}

	public void setDialogData(Map data) {

	}

	public void setLocationData(Map data) {

	}

	public void setReturnText(String text) {

	}

	public void addItemToLocation(Item it) {

	}

	public boolean removeItemFromLocation(Item it) {
		return false;
	}

	public void addDialogOptionToItem(DialogOption d) {

	}

	public boolean removeDialogOptionFromItem(DialogOption d) {
		return false;

	}
}
