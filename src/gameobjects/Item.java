/*
* Created on Dec 15, 2003
*/
package adventure.gameobjects;

import java.io.Serializable;
import java.util.List;
import java.util.Hashtable;

public class Item extends GameObject implements Serializable {


	public static final int ACTION_USE = 0;
	public static final int ACTION_TAKE = 1;
	public static final int ACTION_TALK = 2;
	public static final int ACTION_LOOK = 3;

    /**
     * name of the item
     */
    private String name;

    /**
     * description of the item
     */
    private String description;

    /**
     * path to the image of the item
     */
    private String imgPath;


    /**
     * array of actions for take-command
     */
    private String[] actionTake;

    /**
     * array of actions for look-command
     */
    private String[] actionLook;

    /**
     * array of actions for talk-command
     */
    private String[] actionTalk;

    /**
     * x-coordinate of the image
     */
    private int positionX;

    /**
     * y-coordinate of the image
     */
    private int positionY;

    /**
     * visible flag
     */
    private boolean visible;

    /**
     * Liste der UseActions des Items
     */
    private Hashtable useActions;

    /**
     * Level, auf dem das Image angezeigt werden soll
     */
    private int level;

    /**
     * Contructor of Item
     * @param ID
     * @param name
     * @param description
     * @param imgPath
     * @param positionX
     * @param positionY
     * @param actionTake
     * @param actionTalk
     * @param actionLook
     * @param visible
     */

    public Item(int ID, String name, String description, String imgPath, int positionX, int positionY,
                String[] actionTake, String[] actionTalk, String[] actionLook, int level, boolean visible){

        this.ID = ID;
        this.name=name;
        this.description=description;
        this.imgPath=imgPath;
        this.positionX = positionX;
        this.positionY = positionY;
        this.actionLook=actionLook;
        this.actionTake=actionTake;
        this.actionTalk=actionTalk;
        this.visible = visible;
        this.level = level;
    }
    /**
     * @return array of actions for look-command
     */
    public String[] getActionLook() {
        return actionLook;
    }

    /**
     * @return array of actions for take-command
     */
    public String[] getActionTake() {
        return actionTake;
    }

    /**
     * @return array of actions for talk-command
     */
    public String[] getActionTalk() {
        return actionTalk;
    }

    /**
     * @return the description of the image
     */
    public String getDescription() {
        return description;
    }


    /**
     * @return path of the image
     */
    public String getImgPath() {
        return imgPath;
    }


    /**
     * @return name of the item
     */
    public String getName() {
        return name;
    }

    /**
     * sets the  array of actions for look-command
     * @param strings
     */
    public void setActionLook(String[] strings) {
        actionLook = strings;
    }

    /**
     * sets the  array of actions for take-command
     * @param strings
     */
    public void setActionTake(String[] strings) {
        actionTake = strings;
    }

    /**
     * sets the  array of actions for talk-command
     * @param strings
     */
    public void setActionTalk(String[] strings) {
        actionTalk = strings;
    }

    /**
     * sets the description of the item
     * @param string
     */
    public void setDescription(String string) {
        description = string;
    }


    /**
     * sets the path of the image of the item
     * @param string
     */
    public void setImgPath(String string) {
        imgPath = string;
    }


    /**
     * sets the name of the item
     * @param string
     */
    public void setName(String string) {
        name = string;
    }

    /**
     * @return the x-coordinate of the image
     */
    public int getPositionX() {
        return positionX;
    }

    /**
     * @return the y-coordinate of the image
     */
    public int getPositionY() {
        return positionY;
    }

    /**
     * sets the x-coordinate of the image
     * @param i
     */
    public void setPositionX(int i) {
        positionX = i;
    }

    /**
     * sets the y-coordinate of the image
     * @param i
     */
    public void setPositionY(int i) {
        positionY = i;
    }

    /**
     * sets the visibility of the item
     * @param visible
     */
    public void setVisible(boolean visible){
        this.visible = visible;
    }

    /**
     *
     * @return visibility of the item
     */
    public boolean getVisibility(){
        return visible;
    }

    public boolean isValidPartner(String item) {
        if(useActions.get(item) != null)
            return true;
        return false;
    }

    public String[] getActionUse(String key) {
        return (String[]) useActions.get(key);
    }

    public void addUseAction(String partnerItem, String[] actionScript) {
        useActions.put(partnerItem, actionScript);
    }
}

