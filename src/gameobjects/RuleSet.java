/*
 * Created on 09.01.2004
 *
 */
package adventure.gameobjects;

/**
 * Ruleset Class represents a Ruleset, its state and maybe some other usefull information.
 */
public class RuleSet {
	
	private int ruleSetID; //not changeable
	private String ruleSetName;
	private boolean state;
	private String Autor;
	
	/**
	 * Constructor for RuleSet
	 * @param RuleSetID ID of the RuleSet 
	 * @param RuleSetName the RuleSets Name
	 * @param State the RuleSets State
	 */
	public RuleSet(int RuleSetID, String RuleSetName, String Autor, boolean State)
	{
		this.ruleSetID = RuleSetID;
		this.ruleSetName = RuleSetName;
		this.Autor = Autor;
		this.state = State;
	}
	
	/**
	 * get the State
	 * @return the actuall State of the RuleSet
	 */
	public boolean getState()
	{
		return state;
	}
	
	/**
	 * Set the state
	 * @param State Set a new State for the RuleSet
	 */
	public void setState(boolean State)
	{
		this.state = State;
	}
	
	/**
	 * get the Name
	 * @return the Name of the RuleSet
	 */
	public String getRuleSetName()
	{
		return ruleSetName;
	}
	
	/**
	 * set the Name
	 * @param RuleSetName Set the Name of the RuleSet
	 */
	public void setRuleSetName(String RuleSetName)
	{
		this.ruleSetName = RuleSetName;
	}
	
	/**
	 * Get the RuleSets ID
	 * @return ID of the RuleSet
	 */
	public int getRuleSetID()
	{
		return ruleSetID;
	}
}
