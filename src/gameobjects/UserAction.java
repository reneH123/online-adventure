/*
* Created on Dec 15, 2003
*
* To change the template for this generated file go to
* Window - Preferences - Java - Code Generation - Code and Comments
*/
package adventure.gameobjects;

import java.io.Serializable;

/**
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class UserAction implements Serializable
{
    public static final int ACTION_USE = 0;
    public static final int ACTION_TAKE = 1;
    public static final int ACTION_TALK = 2;
    public static final int ACTION_LOOK = 3;


    int kindOfAction;
    String item1;
    String item2;

    public UserAction(int kindOfAction, String item1, String item2){

        this.kindOfAction = kindOfAction;
        this.item1 = item1;
        this.item2 = item2;
    }

    public UserAction(int kindOfAction, String item1){

        this.kindOfAction = kindOfAction;
        this.item1 = item1;
    }


    public int getKindOfAktion(){
        return kindOfAction;
    }

    public String getItem1(){
        return item1;
    }

    public String getIem2(){
        return item2;
    }

    public void setKindOfAction(int kindOfAction){
        this.kindOfAction = kindOfAction;
    }

    public void setItem1(String item1){
        this.item1 = item1;
    }

    public void setItem2(String item2){
        this.item2 = item2;
    }

}
