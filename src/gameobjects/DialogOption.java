/*
 * Created on Dec 15, 2003
 */
package adventure.gameobjects;

import java.io.Serializable;
import java.util.Arrays;

public class DialogOption extends GameObject implements Serializable {

	private String text;
	private String[] action;

        /**
         * Constructor of DialogOption
         * @param ID
         * @param text
         * @param action
         */

	public DialogOption(int ID, String text, String[] action) {

		this.ID = ID;
		this.action = action;
		this.text = text;
	}

	/**
	 * @return
	 */
	public String[] getAction() {
		return action;
	}

	/**
	 * @return
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param strings
	 */
	public void setAction(String[] strings) {
		action = strings;
	}

	/**
	 * @param string
	 */
	public void setText(String string) {
		text = string;
	}

	public boolean equals(Object o) {
	        if (this == o) return true;
	        if (!(o instanceof DialogOption)) return false;
	
	        final DialogOption dialogOption = (DialogOption) o;

	        if(dialogOption.getId() != this.getId()) return false;
	        return true;
	}

 	public int hashCode() {
        	return text.hashCode();
	}
}
