#
# table structure for table `users`
#

CREATE TABLE `users` (
  `userID` int(10) unsigned NOT NULL auto_increment,
  `username` varchar(20) NOT NULL default '',
  `email` varchar(30) NOT NULL default '',
  `password` varchar(32) NOT NULL default '',
  `icq` varchar(10) default NULL,
  `yahoo` varchar(10) default NULL,
  `state` enum('player','admin') NOT NULL default 'player',
  PRIMARY KEY  (`userID`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
) TYPE=InnoDB;

CREATE TABLE `games` (
  `gameID` int(10) unsigned NOT NULL auto_increment,
  `locationID` int(10) unsigned NULL,
  `gamename` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`gameID`),
  UNIQUE KEY `gamename` (`gamename`)
) TYPE=InnoDB;

CREATE TABLE `savegames` (
  `savegameID` int(10) unsigned NOT NULL auto_increment,
  `userID` int(10) unsigned NOT NULL,
  `gameID` int(10) unsigned NOT NULL,
  `savegameName` varchar(40) default NULL,
  PRIMARY KEY  (`savegameID`),
  UNIQUE KEY `userID` (`userID`,`gameID`,`savegameName`)
) TYPE=InnoDB;

CREATE TABLE `locations` (
  `locationID` int(10) unsigned NOT NULL auto_increment,
  `gameID` int(10) unsigned NOT NULL default '0',
  `locationName` varchar(50) NOT NULL default '',
  `imagePath` varchar(100) default NULL,
  PRIMARY KEY  (`locationID`),
  KEY `gameID` (`gameID`,`locationName`)
) TYPE=InnoDB;

CREATE TABLE `items` (
  `itemID` int(10) unsigned NOT NULL auto_increment,
  `locationID` int(10) unsigned NOT NULL default '0',
  `itemName` varchar(50) NOT NULL default '',
  `description` text,
  `imagePath` varchar(100) default NULL,
  `positionX` int(11) default NULL,
  `positionY` int(11) default NULL,
  `visibleAtStart` char(1) binary NOT NULL default '0',
  PRIMARY KEY  (`itemID`)
) TYPE=InnoDB;

CREATE TABLE `inventory` (
  `savegameID` int(10) unsigned NOT NULL default '0',
  `itemID` int(10) unsigned NOT NULL default '0',
  UNIQUE KEY `savegameID` (`savegameID`,`itemID`)
) TYPE=InnoDB;

CREATE TABLE `score` (
  `savegameID` int(10) unsigned NOT NULL default '0',
  `itemID` int(10) unsigned NOT NULL default '0',
  UNIQUE KEY `savegameID` (`savegameID`,`itemID`)
) TYPE=InnoDB;

CREATE TABLE `actiontypes` (
  `actionTypeID` int(10) unsigned NOT NULL auto_increment,
  `actionName` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`actionTypeID`)
) TYPE=InnoDB;

CREATE TABLE `itemactions` (
  `itemID1` int(10) unsigned NOT NULL default '0',
  `itemID2` int(10) unsigned default NULL,
  `actionTypeID` int(10) unsigned NOT NULL default '0',
  `actionScript` text NOT NULL,
  UNIQUE KEY `itemID1` (`itemID1`,`itemID2`,`actionTypeID`)
) TYPE=InnoDB;

CREATE TABLE `dialogoptions` (
  `dialogOptionID` int(10) unsigned NOT NULL auto_increment,
  `text` text NOT NULL,
  `actionScript` text NOT NULL,
  PRIMARY KEY  (`dialogOptionID`)
) TYPE=InnoDB;

