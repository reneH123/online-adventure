# some test samples:
INSERT INTO items VALUES(1,0,'scissors','One of the best I ever had; it is 3/4 damaged','Schere.jpg',10,10,1);
INSERT INTO items VALUES(2,0,'Apple_cut','A cut apple','Apfel_geschnitten.jpg',20,30,1);

INSERT INTO actiontypes VALUES(1,"actTake");
INSERT INTO actiontypes VALUES(2,"actTalk");
INSERT INTO actiontypes VALUES(3,"actLook");
INSERT INTO actiontypes VALUES(4,"actUse");
