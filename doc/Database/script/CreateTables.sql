USE adventure;

CREATE TABLE `users` (
  `userID` int(10) unsigned NOT NULL auto_increment,
  `username` varchar(20) NOT NULL default '',
  `firstname` varchar(20) NOT NULL default '',
  `nickname` varchar(20) NOT NULL default '',
  `email` varchar(50) NOT NULL default '',
  `password` varchar(10) NOT NULL default '',
  `state` char(1) binary NOT NULL default '0',
  PRIMARY KEY  (`userID`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) TYPE=InnoDB;

CREATE TABLE `savegames` (
  `savegameID` int(10) unsigned NOT NULL auto_increment,
  `savegamename` varchar(20) NOT NULL,
  `userID` int(10) unsigned NOT NULL,
  `rolesetid` int(10) unsigned NOT NULL,
  `currentLocation` int(10) unsigned NOT NULL,
  `currentMessage` blob,
  PRIMARY KEY (`savegameID`),
  UNIQUE KEY `savegamename` (`savegamename`)
) TYPE=InnoDB;

CREATE TABLE `flags` (
  `flagID` int(10) unsigned NOT NULL auto_increment,
  `savegameID` int(10) unsigned NOT NULL,
  `flagname` varchar(20),
  `value` char(1) binary NOT NULL default '0',
  PRIMARY KEY (`flagID`)
) TYPE=InnoDB;

CREATE TABLE `rolesets` (
  `rolesetID` int(10) unsigned NOT NULL auto_increment,
  `setname`  varchar(20),
  `locationid` varchar(20) NOT NULL,
  `state` char(1) binary NOT NULL default '0',
  PRIMARY KEY (`rolesetID`),
  UNIQUE KEY `setname` (`setname`)
) TYPE=InnoDB;

CREATE TABLE `inventory` (
  `inventoryID` int(10) unsigned NOT NULL auto_increment,
  `savegameID` int(10) unsigned NOT NULL default '0',
  `itemID` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY (`inventoryID`)
) TYPE=InnoDB;

CREATE TABLE `scenes` (
  `sceneID` int(10) unsigned NOT NULL auto_increment,
  `savegameID` int(10) unsigned NOT NULL default '0',
  `itemID` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY (`sceneID`)
) TYPE=InnoDB;

CREATE TABLE `items` (
  `itemID` int(10) unsigned NOT NULL auto_increment,
  `itemName` varchar(20) NOT NULL,
  `locationID` int(10) unsigned NOT NULL default '0',
  `description` text,
  `imagePath` varchar(100) default NULL,
  `positionX` int(11) default NULL,
  `positionY` int(11) default NULL,
  `visibleAtStart` char(1) binary NOT NULL default '0',
  `level` int(4) default NULL,
  PRIMARY KEY  (`itemID`)
) TYPE=InnoDB;

CREATE TABLE `locations` (
  `locationID` int(10) unsigned NOT NULL auto_increment,
  `locationName` varchar(20) NOT NULL default '',
  `setname` varchar(100) NOT NULL default '',
  `rolesetID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`locationID`)
) TYPE=InnoDB;

CREATE TABLE `itemactions` (
  `itemactionID` int(10) unsigned NOT NULL auto_increment,
  `itemID` int(10) unsigned NOT NULL default '0',
  `itempartner` varchar(20) NOT NULL,
  `actionScript` text NOT NULL,
  `actionType` int(1) unsigned default '0',
  PRIMARY KEY (`itemactionID`)
) TYPE=InnoDB;

CREATE TABLE `dialogoptions` (
  `dialogoptionID` int(10) unsigned NOT NULL auto_increment,
  `rolesetID` int(10) unsigned NOT NULL,
  `doname` varchar(20),
  `text` text NOT NULL,
  `actionScript` text NOT NULL,
  PRIMARY KEY (`dialogoptionID`)
) TYPE=InnoDB;

