USE adventure;

# Savegames.userID, rolesetID
ALTER TABLE Savegames ADD INDEX userIDindex (userID);
ALTER TABLE Savegames ADD INDEX rolesetIDindex (rolesetID);
ALTER TABLE Savegames ADD FOREIGN KEY userIDfkey (userID) REFERENCES Users(userID) ON DELETE NO ACTION;
ALTER TABLE Savegames ADD FOREIGN KEY rolesetIDfkey (rolesetID) REFERENCES Rolesets(rolesetID) ON DELETE NO ACTION;

# Roleset.locationID
ALTER TABLE Rolesets ADD INDEX locationIDindex (locationID);
ALTER TABLE Rolesets ADD FOREIGN KEY rolesetIDfkey (rolesetID) REFERENCES Locations(locationID) ON DELETE CASCADE;

# Inventory.savegameID, itemID
ALTER TABLE Inventory ADD INDEX savegameIDindex (savegameID);
ALTER TABLE Inventory ADD INDEX itemIDindex (itemID);
ALTER TABLE Inventory ADD FOREIGN KEY savegameIDfkey (savegameID) REFERENCES Savegames(savegameID) ON DELETE NO ACTION;
ALTER TABLE Inventory ADD FOREIGN KEY itemIDfkey (itemID) REFERENCES Items(itemID) ON DELETE NO ACTION;

# Scenes.savegameID, itemID
ALTER TABLE Scenes ADD INDEX savegameIDindex (savegameID);
ALTER TABLE Scenes ADD INDEX itemIDindex (itemID);
ALTER TABLE Scenes ADD FOREIGN KEY savegameIDfkey (savegameID) REFERENCES Savegames(savegameID) ON DELETE NO ACTION;
ALTER TABLE Scenes ADD FOREIGN KEY itemIDfkey (itemID) REFERENCES Items(itemID) ON DELETE NO ACTION;

# Items.locationID
ALTER TABLE Items ADD INDEX locationIDindex (locationID);
ALTER TABLE Items ADD FOREIGN KEY loactionIDfkey (locationID) REFERENCES Locations(locationID) ON DELETE NO ACTION;

# Locations.gameID
ALTER TABLE Locations ADD INDEX rolesetIDindex (rolesetID);
ALTER TABLE Locations ADD FOREIGN KEY rolesetIDfkey (rolesetID) REFERENCES Rolesets(rolesetID) ON DELETE NO ACTION;

# DialogOptions.rolesetID
ALTER TABLE Dialogoptions ADD INDEX rolesetIDindex (rolesetID);
ALTER TABLE Dialogoptions ADD FOREIGN KEY rolesetIDfkey (rolesetID) REFERENCES Rolesets(rolesetID) ON DELETE NO ACTION;

# Itemactions.itemID, itempartnerID
ALTER TABLE Itemactions ADD INDEX itemIDindex (itemID);
ALTER TABLE Itemactions ADD INDEX itempartnerIDindex (itempartnerID);
ALTER TABLE Itemactions ADD FOREIGN KEY itemIDfkey1 (itemID) REFERENCES Items(itemID) ON DELETE NO ACTION;
ALTER TABLE Itemactions ADD FOREIGN KEY itemIDfkey2 (itempartnerID) REFERENCES Items(itemID) ON DELETE NO ACTION;

# Flags.savegameID
ALTER TABLE Flags ADD INDEX savegameIDindex (savegameID);
ALTER TABLE Flags ADD FOREIGN KEY savegameIDfkey (savegameID) REFERENCES Savegames(savegameID) ON DELETE NO ACTION;