#
# Savegames.userID
#

ALTER TABLE Savegames ADD INDEX userIDindex (userID);
ALTER TABLE Savegames ADD FOREIGN KEY userIDfkey (userID) REFERENCES Users(userID) ON DELETE NO ACTION;

#
# Savegames.gameID
#

ALTER TABLE Savegames ADD INDEX gameIDindex (gameID);
ALTER TABLE Savegames ADD FOREIGN KEY gameIDfkey (gameID) REFERENCES Games(gameID) ON DELETE NO ACTION;

#
# Games.locationID
#

ALTER TABLE Games ADD INDEX locationIDindex (locationID);
ALTER TABLE Games ADD FOREIGN KEY locationIDfkey (locationID) REFERENCES Locations(locationID) ON DELETE SET NULL;

#
# Locations.gameID
#

ALTER TABLE Locations ADD INDEX gameIDindex (gameID);
ALTER TABLE Locations ADD FOREIGN KEY gameIDfkey (gameID) REFERENCES Games(gameID) ON DELETE NO ACTION;

#
# Score.savegameID
#

ALTER TABLE Score ADD INDEX savegameIDindex (savegameID);
ALTER TABLE Score ADD FOREIGN KEY savegameIDfkey (savegameID) REFERENCES Savegames(savegameID) ON DELETE NO ACTION;

#
# Score.itemID
#

ALTER TABLE Score ADD INDEX itemIDindex (itemID);
ALTER TABLE Score ADD FOREIGN KEY itemIDfkey (itemID) REFERENCES Items(itemID) ON DELETE NO ACTION;

#
# Inventory.savegameID
#

ALTER TABLE Inventory ADD INDEX savegameIDindex (savegameID);
ALTER TABLE Inventory ADD FOREIGN KEY savegameIDfkey (savegameID) REFERENCES Savegames(savegameID) ON DELETE NO ACTION;

#
# Inventory.itemID
#

ALTER TABLE Inventory ADD INDEX itemIDindex (itemID);
ALTER TABLE Inventory ADD FOREIGN KEY itemIDfkey (itemID) REFERENCES Items(itemID) ON DELETE NO ACTION;

#
# ItemActions.itemID1
#

ALTER TABLE ItemActions ADD INDEX itemID1index (itemID1);
ALTER TABLE ItemActions ADD FOREIGN KEY itemID1fkey (itemID1) REFERENCES Items(itemID) ON DELETE NO ACTION;

#
# ItemActions.itemID2
#

ALTER TABLE ItemActions ADD INDEX itemID2index (itemID2);
ALTER TABLE ItemActions ADD FOREIGN KEY itemID2fkey (itemID2) REFERENCES Items(itemID) ON DELETE NO ACTION;

#
# ItemActions.actionTypeID
#

ALTER TABLE ItemActions ADD INDEX actionTypeIDindex (actionTypeID);
ALTER TABLE ItemActions ADD FOREIGN KEY actionTypeIDfkey (actionTypeID) REFERENCES ActionTypes(actionTypeID) ON DELETE NO ACTION;


